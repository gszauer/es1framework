#ifndef _H_ES1_KEYBOARD_
#define _H_ES1_KEYBOARD_

#include <ES1/Framework/Keydefs.h>

namespace ES1 {
    class Keyboard {
    private:
        Keyboard(const Keyboard&);
        Keyboard& operator=(const Keyboard&);
    protected:
        bool m_pKeyState[156];
        bool m_pPrevKeyState[156];
    public:
        Keyboard();
        ~Keyboard();
        
        bool KeyDown(Keydef eKey);
        bool KeyUp(Keydef eKey);

        bool KeyPressed(Keydef eKey);
        bool KeyReleased(Keydef eKey);
        
        void SetKeyPressed(Keydef eKey);
        void SetKeyReleased(Keydef eKey);
        
        void Update();
    };
}

#endif