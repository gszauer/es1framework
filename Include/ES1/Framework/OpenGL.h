#ifndef _H_ES1_OPENGL_
#define _H_ES1_OPENGL_

    #ifdef _WIN32
        //define something for Windows (32-bit and 64-bit, this part is common)
        #ifdef _WIN64
            //define something for Windows (64-bit only)
        #else
            //define for 32 bit only
        #endif
    #endif

    #ifdef __APPLE__
        #include <OpenGL/gl.h>
        #include <OpenGL/OpenGL.h>
        // Define for all apples
        #if TARGET_IPHONE_SIMULATOR
            // iOS Simulator
        #elif TARGET_OS_IPHONE
            // iOS device
        #elif TARGET_OS_MAC
            // Other kinds of Mac OS
        #else
            // Unsupported platform
        #endif
    #endif

    #ifdef __linux
        // linux
    #elif __unix // all unices not caught above
        // Unix
    #elif __posix
        // POSIX
    #endif

#endif