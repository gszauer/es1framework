#ifndef _H_ES1_STREAMREADER_
#define _H_ES1_STREAMREADER_

namespace ES1 {
    class DataStream;

    class StreamReader {
    private:
        StreamReader(const StreamReader&);
        StreamReader& operator=(const StreamReader&);
    public:
        StreamReader();
        virtual ~StreamReader();
        virtual DataStream* Load(const char* path) = 0;
    };
}

#endif