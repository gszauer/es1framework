#ifndef _H_ES1_MESH_
#define _H_ES1_MESH_

#include "Vector.h"

namespace ES1 {
    class Mesh {
    friend class MeshBuilder;
    private:
        float* m_pSoup;
        
        // All actual mesh data is contained as contigous chunks in m_pSoup,
        // We simply point into that section of memory with these
        float* m_pVertices;
        float* m_pNormals;
        float* m_pTexCoords;
        float* m_pColors;
        unsigned int* m_pIndices;
        
        // Every mesh has a bounding AABB and a bounding Sphere
        vec m_vecMin;
        vec m_vecMax;
        float m_nRadius;
        
        int m_nVertexCount;
        // It's possible to have a mesh with vertex count > 0 but index count == 0
        // Not every mesh is going to be indexed. DrawArrays and DrawElements should
        // both be respected by the renderer
        int m_nIndexCount;
    private:
        Mesh(const Mesh&);
        Mesh& operator=(const Mesh&);
        
        static Mesh* Construct(float* vertices, int numVertices, float* normals, int numNormals, float* texCoords, int numTexCoords, float* colors, int numColors, unsigned int* indices, int numIndices);
    public:
        Mesh();
        ~Mesh();
        
        static Mesh* MakeCube(bool useNormals = true, bool useUvs = false, bool useColors = false);
        static Mesh* MakeSphere(bool useNormals = true, bool useTexCoords = false);
        static Mesh* MakePlane(bool normals = true, bool texCoords = true, bool colors = true);
        
        // TODO: Generate Taurus
        // TODO: Generate Cone
        // TODO: Generate Cylinder
        // TODO: Loab OBJ
        
        float* GetSoup();
        float* GetVertices(); // This should suffice for animations
        float* GetNormals();  // Can modify result of returned pointer
        float* GetTexCoords();
        float* GetColors();
        unsigned int* GetIndices();
        
        bool IsValid();
        bool HasVertices();
        bool HasNormals();
        bool HasTexCoords();
        bool HasColors();
        bool HasIndices();
        
        int GetIndexCount();
        int GetVertexCount();
        
        vec GetMin();
        vec GetMax();
        float GetRadius();
    };
}

#endif