#ifndef _H_ES1_STDLOGGER_
#define _H_ES1_STDLOGGER_

#include <ES1/Framework/Logger.h>

namespace ES1 {
    class StdLogger : public Logger {
    public:
        StdLogger();
        ~StdLogger();
        
        void Log(const char* szString, ...);
    };
}

#endif