#ifndef _H_ES1_RENDERER_
#define _H_ES1_RENDERER_

// Based on OpenGLES 1.0: https://www.khronos.org/registry/gles/api/1.0/gl.h
// Lighting: http://www.sjbaker.org/steve/omniv/opengl_lighting.html
// Materials: http://jerome.jouvie.free.fr/opengl-tutorials/Tutorial14.php

#include "Material.h"
#include "Color.h"
#include "Mesh.h"
#include "Texture.h"
#include "Matrix.h"
#include "Light.h"

#include <vector>

namespace ES1 {
    class Renderer {
    private:
        // Bloom? Bloom material?
        // Other screen space effects?
        Matrix view;
        Matrix projection;
        
        unsigned int clearFlags;
        unsigned int matrixMode;
        bool lightingEnabled;
        bool depthTestEnabled;
        bool smoothShadingEnabled;
        bool lightEnabled;
        bool depthWriteEnabled;
        float ambientIntensity;
        bool colorBlendingEnabled;
        bool blendingEnabled;
        bool wireOutlineEnabled;
        
        Color materialAmbient;
        Color materialDiffuse;
        Color materialSpecular;
        float materialShine;
        
        Texture* boundDiffuse;
        bool textureEnabled = false;
        
        std::vector<PointLight*> pointLights;
        std::vector<DirectionalLight*> directionalLights;
        
        int objectsDrawn;
        int numRenderCalls;
    private:
        Renderer(const Renderer&);
        Renderer& operator=(const Renderer&);
        void EnableColorBlending();
        void DisableColorBlending();
        void BindLight(PointLight* light);
        void BindLight(DirectionalLight* light);
        void BindMaterial(Material& material);
        void SetMatrixMode(unsigned int mode);
    public:
        Renderer();
        ~Renderer();
        
        void SetClearColor(Color color);
        void SetClearDepth(float depth);
        void SetAmbientIntensity(float ambient);
        // Stencyl?
        
        void ConfigClearColor(bool enable);
        void ConfigClearDepth(bool enable);
        void ConfigDepthTest(bool enable);
        void ConfigSmoothShading(bool enable);
        void ConfigBlending(bool enable);
        void ConfigDepthWrite(bool enable);
        void ConfigWireframe(bool enable);
        void ConfigTexture(bool enable);
        // Stencyl?
        void ConfigLighting(bool enable);
    
        void Render(Mesh& mesh, Material& material, Matrix& transform);
        
        void Line(Vector& p1,Vector& p2, Color color, Matrix& transform);
        void Line(float* p1,float* p2, Color color, Matrix& transform);
        
        void BeginLines(Color color, Matrix& transform);
        void Vertex(float x, float y, float z);
        void Vertex(float* xyz);
        void EndLines();
        
        //void Render(Model& model); TODO: Is Model a core function of the engine?
        
        void SetView(Matrix& v);
        void SetProjection(Matrix& p);
        Matrix GetView();
        Matrix GetProjection();
        
        void Ortho(float left, float right, float bottom, float top, float _near, float _far);
        void Perspective(float fov, float aspect, float _near, float _far);
        void LookAt(const Vector& position, const Vector& target);
        
        void Clear();
        
        int GetNumObjectsRendered();
        int GetNumDrawCalls();
        
        void PushLight(PointLight* light);
        void PushLight(DirectionalLight* light);
        void ClearLights();
    };
}

#endif