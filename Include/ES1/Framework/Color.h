#ifndef _H_ES1_COLOR_
#define _H_ES1_COLOR_

namespace ES1 {
    struct Color4f {
        union {
            struct {
                float r;
                float g;
                float b;
                float a;
            };
            float rgba[4];
        };
        
        Color4f();
        Color4f(float r, float g, float b);
        Color4f(float r, float g, float b, float a);
        //Color4f(int r, int g, int b);
        //Color4f(int r, int g, int b, int a);
        Color4f(const Color4f& other);
        Color4f& operator=(const Color4f& other);
    };
    
    bool operator==(const Color4f& c1, const Color4f& c2);
    bool operator!=(const Color4f& c1, const Color4f& c2);
    
    typedef Color4f Color;
    
    extern Color Black;
    extern Color Yellow;
    extern Color Ambient;
    extern Color White;
    extern Color Red;
    extern Color Green;
    extern Color Blue;
    extern Color Magenta;
    extern Color Background;
}

#endif