#ifndef _H_ES1_CLIPBOARD_
#define _H_ES1_CLIPBOARD_

#include <string>

namespace ES1 {
    class Platform;
    
class Clipboard {
protected:
    Platform* m_pPlatform;
protected:
    Clipboard();
    Clipboard(const Clipboard&);
    Clipboard& operator=(const Clipboard&);
public:
    Clipboard(Platform& platform);
    ~Clipboard();
    
    std::string GetText();
    void SetText(const std::string& text);
};
}

#endif