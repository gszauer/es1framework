#ifndef _H_ES1_VECTOR_
#define _H_ES1_VECTOR_

namespace ES1 {
    struct Vector {
        union {
            float xyz[3];
            struct {
                float x, y, z;
            };
        };
        
        Vector();
        Vector(float _x, float _y, float _z);
        Vector(float* v);
        Vector(const Vector& v);
        Vector& operator=(const Vector& v);
        float operator [](int i);
    };
    
    Vector Cross(const Vector& v1, const Vector& v2);
    float Dot(const Vector& v1, const Vector& v2);
    float Distance(const Vector& v1, const Vector& v2);
    float DistanceSq(const Vector& v1, const Vector& v2);
    float Length(const Vector& v);
    float LengthSq(const Vector& v);
    Vector Normalize(const Vector& v);
    Vector Lerp(const Vector& v1, const Vector& v2, float delta);
    
    bool operator==(const Vector& lhs, const Vector& rhs);
    bool operator< (const Vector& lhs, const Vector& rhs);
    bool operator!=(const Vector& lhs, const Vector& rhs);
    bool operator> (const Vector& lhs, const Vector& rhs);
    bool operator<=(const Vector& lhs, const Vector& rhs);
    bool operator>=(const Vector& lhs, const Vector& rhs);
    
    Vector operator*(const Vector& v, float f);
    Vector operator*(float f, const Vector& v);
    Vector operator*(const Vector& v1, const Vector& v2);
    Vector operator+(const Vector& v1, const Vector& v2);
    Vector operator-(const Vector& v1, const Vector& v2);
    Vector operator-(const Vector& v);
    
    Vector& operator*=(Vector& v, float f);
    Vector& operator*=(Vector& v, const Vector& v2);
    Vector& operator+=(Vector& v, const Vector& v2);
    Vector& operator-=(Vector& v, const Vector& v2);

    float Dot4fv(float* v1, float* v2);
    void Normalize4fv(float* v);
    void Cross4fv(float* result, float* v1, float* v2);
    
    float Dot3fv(float* v1, float* v2);
    void Normalize3fv(float* v);
    void Cross3fv(float* result, float* v1, float* v2);
    
    // Short types
    typedef Vector vec;
}

#endif