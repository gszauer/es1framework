#ifndef _H_ES1_MOUSEDEFS_
#define _H_ES1_MOUSEDEFS_

namespace ES1 {
    enum Mousedef {
        MOUSE_NONE    = 0,
        MOUSE_LEFT,
        MOUSE_MIDDLE,
        MOUSE_RIGHT
    };

    int MouseIndex(Mousedef m);
}

#endif