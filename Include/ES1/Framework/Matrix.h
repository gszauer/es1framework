#ifndef _H_ES1_MATRIX_
#define _H_ES1_MATRIX_

#include <ES1/Framework/Vector.h>

/*
 Matrices are COLUMN major
 They use POST multiplication (matrix * vector)
 They also use column vectors
 Right handed coordinate system
 +Z is out of the monitor, -Z is into the monitor
 
 Logical Matrix layout
    Xx  Yx  Zx  Tx
    Xy  Yy  Zy  Ty
    Xz  Yz  Zz  Tz
    0   0   0   1
 
 Memory layout
    Matrices are stored in a column major layout.
    That is, the above matrix will be stored as:
    Xx Xy Xz 0 Yx Yy Yx 0 Zx Zy Zz 0 Tx Ty Tz 1
 
 Memory layout
    The above memory layout does mean that physically
    laid out, the memory for this matrix is transposed.
    Xx  Xy  Xz  0
    Yx  Yy  Yz  0
    Zx  Zy  Zz  0
    Tx  Ty  Tz  1
 
 Indexed column first
    matrix[column][row]
 
 Transformation are applied right to left
 Proper transform pipe:
    Scale First, Rotate second, Translate last
    Translate * Rotate * Scale * Vertex
 Proper Modev View Projection pipe (MVP)
    Projection * View * Model * Vertex
*/

// #define INDEX(r,c) (r * 4 + c)

namespace ES1 {    
    struct Matrix {
        union {
            float m[16];
            struct {
                float   _11, _12, _13, _14, // Column 1
                        _21, _22, _23, _24, // Column 2
                        _31, _32, _33, _34, // Column 3
                        _41, _42, _43, _44; // Column 4
            };
        };
        
        Matrix();
        Matrix(float* v);
        // The blow constructor takes verbatim column order
        Matrix(float m11, float m21, float m31, float m41,
               float m12, float m22, float m32, float m42,
               float m13, float m23, float m33, float m43,
               float m14, float m24, float m34, float m44);
        Matrix(const Matrix& m);
        Matrix& operator=(const Matrix& other);
        
        void GetRow(int i, float* outData);
        void GetColumn(int i, float* outData);
        
        // Returns index into column i
        float* operator [](int i);
    };
    
    // Generic matrix operations
    Matrix operator*(const Matrix& v, float f);
    Matrix operator*(const Matrix& m1, const Matrix& m2);
    Matrix& operator*=(Matrix& m, float f);
    Matrix& operator*=(Matrix& m, const Matrix& m2);
    bool operator==(const Matrix& lhs, const Matrix& rhs);
    bool operator!=(const Matrix& lhs, const Matrix& rhs);
    float Determinant(const Matrix& m);
    Matrix Transpose(const Matrix& m);
    Matrix Minor(const Matrix& m);
    Matrix Cofactor(const Matrix& m);
    Matrix Adjugate(const Matrix& m);
    Matrix Inverse(const Matrix& m);
    Vector MultiplyPoint(const Matrix& m, const Vector& vector);
    Vector MultiplyVector(const Matrix& m, const Vector& vector);
    
    // Transform matrix operations
    Matrix Translate(float x, float y, float z);
    Matrix Translate(const Vector& v);
    Matrix Scale(float uniform);
    Matrix Scale(float x, float y, float z);
    Matrix Scale(const Vector& v);
    Matrix Rotation(float eulerAngle, const Vector& axis);
    Matrix Rotation_LeftHanded(float eulerAngle, const Vector& axis);
    Matrix RotationX(float euler);
    Matrix RotationY(float euler);
    Matrix RotationZ(float euler);
    Matrix RotationX_LeftHanded(float euler);
    Matrix RotationY_LeftHanded(float euler);
    Matrix RotationZ_LeftHanded(float euler);
    
    // Graphics matrix operations
    Matrix Frustum(float left, float right, float bottom, float top, float _near, float _far);
    Matrix Ortho(float left, float right, float bottom, float top, float _near, float _far);
    Matrix Ortho(float left, float right, float bottom, float top);
    Matrix Ortho();
    Matrix Perspective(float fov, float aspect, float _near, float _far);
    
    // The Look functions already return a view matrix
    Matrix LookAt(const Vector& position, const Vector& target, const Vector& up);
    Matrix LookAt(const Vector& position, const Vector& target);
    Matrix LookTowards(const Vector& position, const Vector& forward, const Vector& up);
    Matrix LookTowards(const Vector& position, const Vector& forward);
    
    // Ortho-Normalize the 3x3 part of the matris
    Matrix Normalize(const Matrix& mat);
    
    Matrix TRS(const Vector& translation, const Vector& eulerRotation, const Vector& scale);
    Matrix TRS(const Vector& translation, const Vector& eulerRotation, float uniformScale);
    Matrix TRS(const Vector& transform, const Vector& eulerRotation);
    
    typedef Matrix mat4;
}

#endif