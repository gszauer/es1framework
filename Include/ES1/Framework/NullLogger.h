#ifndef _H_ES1_NULLLOGGER_
#define _H_ES1_NULLLOGGER_

#include <ES1/Framework/Logger.h>

namespace ES1 {
    class NullLogger : public Logger {
    public:
        NullLogger();
        ~NullLogger();
        
        void Log(const char* szString, ...);
    };
}

#endif