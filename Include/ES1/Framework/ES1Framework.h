#ifndef _H_ES1_ES1FRAMEWORK_
#define _H_ES1_ES1FRAMEWORK_

#include <ES1/Framework/Config.h>
#include <ES1/Framework/Platform.h>
#include <ES1/Framework/Application.h>
#include <ES1/Framework/DataStream.h>
#include <ES1/Framework/StreamReader.h>
#include <ES1/Framework/StreamWriter.h>
#include <ES1/Framework/FileSystem.h>
#include <ES1/Framework/Math.h>
#include <ES1/Framework/Keyboard.h>
#include <ES1/Framework/Mouse.h>
#include <ES1/Framework/Logger.h>
#include <ES1/Framework/NullLogger.h>
#include <ES1/Framework/FileLogger.h>
#include <ES1/Framework/StdLogger.h>
#include <ES1/Framework/DialogWindows.h>
#include <Es1/Framework/Clipboard.h>
#include <Es1/Framework/Renderer.h>

// TODO: At end go trough and make sure all headers are included!

#endif