#ifndef _H_ES1_MOUSE_
#define _H_ES1_MOUSE_

#include <ES1/Framework/Mousedefs.h>

#define MOUSE_SCROLL_DELTA 0.0005f

namespace ES1 {
    class Mouse {
    protected:
        bool        m_pButtonState[4];
        bool        m_pLastButtonState[4];
        int         m_nPosition[2];
        int         m_nScrollDirection;
        bool        m_bInWindiw;
        bool        m_bLastInWindow;
    public:
        Mouse();
        ~Mouse();
        
        void SetPosition(int nX, int nY);
        int GetX();
        int GetY();
        int* GetPosition();
        
        void SetMouseButton(Mousedef eButton);
        void ClearMouseButton(Mousedef eButton);
        void SetScrollDirection(int direction);
        
        int GetScrollDirection();
        
        bool ButtonDown(Mousedef eButton);
        bool ButtonUp(Mousedef eButton);
        bool MousePressed(Mousedef eButton);
        bool MouseReleased(Mousedef eButton);
        
        void SetInWidow(bool inWindow);
        bool InWindow();
        bool LeftWindow();
        bool EnteredWindow();
        
        void Update();
    };
}

#endif