#ifndef _H_ES1_APPLICATION_
#define _H_ES1_APPLICATION_

#include <string>

namespace ES1 {
    class FileSystem;
    class Keyboard;
    class Mouse;
    class Logger;
    class DialogWindows;
    class Clipboard;
    class Renderer;

    class Application {
    protected:
        static Application* g_pLastInstance;
        FileSystem*         m_pFileSystem;
        Keyboard*           m_pKeyboard;
        Mouse*              m_pMouse;
        Logger*             m_pDebugLog;
        Logger*             m_pWarningLog;
        Logger*             m_pErrorLog;
        DialogWindows*      m_pWindows;
        Clipboard*          m_pClipboard;
        std::string         m_strAppName;
        int                 m_nWindowWidth;
        int                 m_nWindowHeight;
    private:
        Application();
        Application(const Application&);
        Application& operator=(const Application&);
    public:
        Application(const char* name);
        virtual ~Application();
        static Application* GetInstance();
        
        void SetFileSystem(FileSystem* pFS);
        FileSystem* GetFileSystem();
        
        void SetKeyboard(Keyboard* pKb);
        Keyboard* GetKeyboard();
        
        void SetMouse(Mouse* pMouse);
        Mouse* GetMouse();
        
        void SetLoggers(Logger* debug, Logger* warning, Logger* error);
        Logger* GetDebugLog();
        Logger* GetWarningLog();
        Logger* GetErrorLog();
        
        void SetDialogWindows(DialogWindows* pWindows);
        DialogWindows* GetDialog();
        
        void SetClipboard(Clipboard* pClipboard);
        Clipboard* GetClipboard();
        
        // TODO: Hook these functions up in framework window!
        virtual void OnInitialize(Renderer* renderer, int winWidth, int winHeight) { }
        virtual void OnUpdate(float deltaTime) { }
        virtual void OnShutdown() { }
        virtual void OnRender(Renderer* renderer) { }
        virtual void OnResize(int w, int h) { /*TODO: Resize renderer? */ }
        
        std::string GetName();
    };
}

#endif