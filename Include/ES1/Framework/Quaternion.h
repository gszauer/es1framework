#ifndef _H_ES1_QUATERNION_
#define _H_ES1_QUATERNION_

#include <ES1/Framework/Matrix.h>

// All the quaternion constructors create a normalized quaternion.
// To make a non-normalized copy, use the "Clone" function

namespace ES1 {
    struct Quaternion {
        union {
            float wxyz[4];
            struct {
                float w, x, y, z;
            };
        };
        
        Quaternion();
        Quaternion(float _w, float _x, float _y, float _z);
        Quaternion(float* v);
        Quaternion(const Quaternion& v);
        Quaternion(const Vector& eulers);
        Quaternion(float eulerX, float eulerY, float eulerZ);
        Quaternion& operator=(const Quaternion& v);
        float operator [](int i);
        void ComputeW();
        void Normalize();
    };
    
    Quaternion Clone(const Quaternion& quat);
    Quaternion EulerAngles(float x, float y, float z);
    Quaternion EulerAngles(const Vector& v);
    Vector ToEulerAngles(const Quaternion& q);
    Quaternion AngleAxis(float angle, const Vector& axis);
    void ToAngleAxis(const Quaternion& q, float* outAngle, Vector* outAxis);
    
    Quaternion Normalize(const Quaternion& q);
    Quaternion Inverse(const Quaternion& q);
    float Dot(const Quaternion& q1, const Quaternion& q2);
    
    Quaternion operator*(const Quaternion& q1, const Quaternion& q2);
    Quaternion operator*(const Quaternion& q1, float f);
    Quaternion& operator*=(Quaternion& q1, const Quaternion& q2);
    Quaternion& operator*=(Quaternion& q1, float f);
    Vector operator*(const Quaternion& q, const Vector& v);
    bool operator==(const Quaternion& lhs, const Quaternion& rhs);
    bool operator!=(const Quaternion& lhs, const Quaternion& rhs);
    
    Quaternion Lerp(const Quaternion& lhs, const Quaternion& rhs, float delta);
    Quaternion Slerp(const Quaternion& lhs, const Quaternion& rhs, float delta);
    float Magnitude(const Quaternion& q);
    float SqrMagnitude(const Quaternion& q);
    Quaternion FromTo(const Vector& from, const Vector& to);
    
    Quaternion AsQuaternion(const Matrix& m);
    Matrix AsMatrix(const Quaternion& q);
    
    typedef Quaternion quat;
}

#endif