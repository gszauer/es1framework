#ifndef _H_ES1_MATH_
#define _H_ES1_MATH_

#define DEG2RAD  0.0174532f
#define RAD2DEG 57.2957795f
#define PI       3.14159265359f
#define PI2      6.2831853f
#define EPSILON	 0.00000005f
               //0.99999994

namespace ES1 {
    float Lerp(float from, float to, float delta);
    float Clamp(float input, float min, float max);
    float Clamp01(float input);
    bool Equals(float f1, float f2);
    float Abs(float value);
    float Sqrt(float number);
    float Pow(float base, float exponent);
    
    float CosEuler(float euler);
    float SinEuler(float euler);
    float TanEuler(float euler);
    
    float CosRadian(float rad);
    float SinRadian(float rad);
    float TanRadian(float rad);
    
    float AsinEuler(float euler);
    float AcosEuler(float euler);
    float AtanEuler(float euler);
    float Atan2Euler(float eulerX, float eulerY);
    
    float AsinRadian(float rad);
    float AcosRadian(float rad);
    float AtanRadian(float rad);
    float Atan2Radian(float radX, float radY);
    
    float Min(float x, float y);
    float Max(float x, float y);
}

#include <ES1/Framework/Vector.h>
#include <ES1/Framework/Matrix.h>
#include <ES1/Framework/Quaternion.h>

#endif