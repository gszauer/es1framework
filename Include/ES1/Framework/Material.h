#ifndef _H_ES1_MATERIAL_
#define _H_ES1_MATERIAL_

#include "Color.h"

namespace ES1 {
    class Texture;

    struct Material {
    protected:
        Material(const Material& other);
        Material& operator=(const Material& other);
    public:
        // Diffuse and ambient should always be the same!
        Color ambient;
        Color diffuse;
        // Specular and shininess make up the highlight
        Color specular;
        float shininess;
        
        Texture* diffuseTexture;
        Texture* normalTexture;
        
        bool lit;
        bool wireframe;
        bool flatShaded;    // WORKS
        
        bool castShadow;
        bool recieveShadow;
        bool renderOcclusion; // Behind wall effect
        // Bloom effect? bool renderBloom;
        
        Material();
        ~Material();
        
        // Sets both diffuse and ambient to the same color.
        void SetColor(Color amb);
        // Sets both the specular and shininess
        void SetHighlight(Color color, float intensity);
        
        void ParseMtl(const char* path);
    };
}

#endif