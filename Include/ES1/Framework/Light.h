#ifndef _H_ES1_LIGHT_
#define _H_ES1_LIGHT_

#include <ES1/Framework/Vector.h>
#include <ES1/Framework/Color.h>

namespace ES1 {
    struct PointLight {
        Vector lightPosition; // World position
        float lightRadius;
        Color lightColor;
        
        PointLight();
        PointLight(const PointLight&);
        PointLight& operator=(const PointLight&);
        ~PointLight();
    };

    struct DirectionalLight {
        Vector lightDirection;
        Color lightColor;
        
        DirectionalLight();
        DirectionalLight(const DirectionalLight&);
        DirectionalLight& operator=(const DirectionalLight&);
        ~DirectionalLight();
    };
}

#endif