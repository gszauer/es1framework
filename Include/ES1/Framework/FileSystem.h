#ifndef _H_ES1_FILESYSTEM_
#define _H_ES1_FILESYSTEM_

#include <string>
#include <vector>
#include <map>

namespace ES1 {
    class Platform;
    class DataStream;
    class StreamReader;
    class StreamWriter;

    typedef std::map<std::string, std::pair<int, DataStream*>> RefCount;

    class FileSystem {
    private:
        FileSystem();
        FileSystem(const FileSystem&);
        FileSystem& operator=(const FileSystem&);
    private:
        Platform* m_pPlatformInstance; // Application will own file system, but platform will create it!
    protected:
        std::map<std::string, StreamReader*>    m_mapReaders;
        std::map<std::string, StreamWriter*>    m_mapWriters;
        RefCount                                m_mapRefCount;
        std::string                             m_strWorkingDirectory;
        std::string                             m_strAssetsDirectory;
        std::string                             m_strPresistentDirectory;
    public:
        FileSystem(Platform& platform, const char* working, const char* assets, const char* presistent);
        ~FileSystem();
        
        void RegisterReader(const char* szType, StreamReader* rProcessor); // Will take ownership & free in destructor
        void RegisterWriter(const char* szType, StreamWriter* rProcessor); // Will take ownership & free in destructor
        
        DataStream* Load(const char* szDataPath, const char* szForceType = 0); // Does reference counting
        void Free(DataStream* pStream);
        void Save(const DataStream& rTarget, const char* szDataPath, const char* szForceType);
        
        const std::string& GetAssetDirectory();
        const std::string& GetWorkingDirectory();
        const std::string& GetPresistentDirectory();
        
        bool FileExists(const char* szFilePath);
        bool DirectoryExists(const char* szDirectoryPath);
        
        bool CreateFile(const char* szFilePath);
        bool CreateDirectory(const char* szDirectoryPath);
        
        std::vector<std::string> List(const char* szDirectoryPath);
        bool IsFile(const char* szFilePath);
        bool IsDirectory(const char* szFilePath);
        
        std::string ReadAllText(const char* szFilePath);
        std::string GetExtension(const char* szFilePath);
    };
}

/* Sample Usage:
void LoadModel(const char* path, const FileSystem& fileSystem) {
    MD5ModelStream* model = (MD5ModelStream*)fileSystem.Load(path, "MD5Model");
    // TOOD: Process data from model!
    fileSystem.Free(model);
}
*/

#endif