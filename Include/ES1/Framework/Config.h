#ifndef _H_ES1_CONFIG_
#define _H_ES1_CONFIG_

#define ES1_LOG_CONSTRUCTOR     1
#define ES1_LOG_DESTRUCTOR      1
#define ES1_LOG_INIT            1
#define ES1_LOG_SHUTDOWN        1
#define ES1_REFCOUNT            1

#define ES1_LOG(x) std::cout << x << "\n";

#endif