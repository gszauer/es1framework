#ifndef _H_ES1_LOG_
#define _H_ES1_LOG_

#include <iostream>

namespace ES1 {
    class Logger {
    private:
        Logger(const Logger&);
        Logger& operator=(const Logger&);
    public:
        inline Logger() { }
        inline virtual ~Logger() { }
        
        virtual void Log(const char* szString, ...) = 0;
    };
}

#endif