#ifndef _H_ES1_TEXTURE_
#define _H_ES1_TEXTURE_

namespace ES1 {
    class Texture {
    protected:
        unsigned int m_nGLHandle;
        unsigned int m_nWidth;
        unsigned int m_nHeight;
        unsigned int m_nBitDepth;
        bool m_bLinearFilter;
    protected:
        Texture();
        Texture(const Texture&);
        Texture& operator=(const Texture&);
    public:
        static Texture* Load(const char* path);
        static void Unbind();
        
        void Bind();
        ~Texture();

        unsigned int GetGLHandle();
        
        int GetWidth();
        int GetHeight();
        
        bool IsUsingLinearFilter();
        bool IsUsingNearestFilter();
        
        void UseLinearFilter();
        void UseNearestFilter();
    };
}

#endif