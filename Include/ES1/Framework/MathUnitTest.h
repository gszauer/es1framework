#ifndef _H_ES1_MATHUNITTEST_
#define _H_ES1_MATHUNITTEST_

#include "Math.h"

//#define TEST_GL_VIEW 1

namespace UnitTests {
    void MathUnits();
    void VectorUnits();
    void MatrixUnits();
    void QuaternionUnits();
    
    void PrintQuaternion(const ES1::Quaternion& q);
    void PrintQuaternion(const char* c, const ES1::Quaternion& q);
    void PrintVector(const ES1::Vector& v);
    void PrintVector(const char* c, const ES1::Vector& v);
    void PrintMatrix(const ES1::Matrix& m);
    void PrintMatrix(const char* c, const ES1::Matrix& m);
    
    void DoGLRender();
}


#endif