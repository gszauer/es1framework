#ifndef _H_ES1_DATASTREAM_
#define _H_ES1_DATASTREAM_

#include <string>

namespace ES1 {
    class DataStream {
    private:
        DataStream();
        DataStream(const DataStream&);
        DataStream& operator=(const DataStream&);
    protected:
        std::string m_strType;
        void*       m_pRawData;
    public:
        DataStream(const char* szType);
        virtual ~DataStream();
        
        const std::string& GetType();
        void* GetRawData();
    };
}

#endif