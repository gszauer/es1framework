#ifndef _H_ES1_DIALOGWINDOWS_
#define _H_ES1_DIALOGWINDOWS_

#include <string>
#include <cstdio>
#include <vector>

namespace ES1 {
    class Platform;
    
    
class DialogWindows {
protected:
    Platform* m_pPlatform;
protected:
    DialogWindows();
    DialogWindows(const DialogWindows&);
    DialogWindows& operator=(const DialogWindows&);
public:
    DialogWindows(Platform& platform);
    ~DialogWindows();
    
    FILE* OpenFile(const char* szMode = "w+", const std::vector<std::string>& filter = std::vector<std::string>());
    FILE* SaveFile(const char* szMode = "w+", const std::vector<std::string>& filter = std::vector<std::string>());
    std::string SelectFolder();
    
    void Alert(const char* szTitle, const char* szMessage);
};
}

#endif