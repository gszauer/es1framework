#ifndef _H_ES1_FILELOGGER_
#define _H_ES1_FILELOGGER_

#include <ES1/Framework/Logger.h>
#include <cstdio>

namespace ES1 {
    class FileLogger : public Logger {
    protected:
        FILE* pTarget;
    public:
        FileLogger();
        ~FileLogger();
        
        void SetFile(const char* szFilePath);
        
        void Log(const char* szString, ...);
    };
}

#endif