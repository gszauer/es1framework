#ifndef _H_ES1_STREAMWRITER_
#define _H_ES1_STREAMWRITER_

namespace ES1 {
    class DataStream;

    class StreamWriter {
    private:
        StreamWriter(const StreamWriter&);
        StreamWriter& operator=(const StreamWriter&);
    public:
        StreamWriter();
        virtual ~StreamWriter();
        
        virtual void Save(const DataStream& rTarget, const char* szDataPath) = 0;
    };
}

#endif