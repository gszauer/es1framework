#ifndef _H_ES1_PLATFORM_
#define _H_ES1_PLATFORM_

#include <string>
#include <cstdio>
#include <vector>

#include <ES1/Framework/Keydefs.h>
#include <ES1/Framework/Mousedefs.h>

/* IDEA:
 C-API for platform specific things. No windowing, just things like
 getting milliseconds, creating files, triggering an open file window, etc */

namespace ES1 {
    class Application;
    class DialogWindows;
    class FileSystem;
    class Clipboard;
    class Mouse;
    class Keyboard;
    class Renderer;
    
    class Platform {
    private:
        DialogWindows*  m_pWindows;
        FileSystem*     m_pFileSystem;
        Clipboard*      m_pClipboard;
        Mouse*          m_pMouse;
        Keyboard*       m_pKeyboard;
    private:
        Platform();
        Platform(const Platform&);
        Platform& operator=(const Platform&);
    protected:
        Renderer*          m_pRenderer;
        Application*    m_pApplication;
        bool            m_bAlive;
        std::string     m_strWorkingDir;
        std::string     m_strAssetsDir;
        double          m_dLastTime;
        std::vector<std::string> m_vArgs;
    public: // Platform specific
        Platform(Application* pApplication);
        ~Platform();                     
        
        int MessagePump(int argc, const char** argv);
    public: // Generic
        void OnKeyDown(Keydef eKey);
        void OnKeyUp(Keydef eKey);
        
        void SetMousePosition(int x, int y);
        void SetScrollWheel(float delta);
        void OnMouseDown(Mousedef button);
        void OnMouseUp(Mousedef button);
        void MouseEnteredWindow();
        void MouseLeftWindow();
        
        void SetWorkingDir(const char* szDir);
        void SetAssetsDir(const char* szDir);
        std::string GetWorkingDir();
        std::string GetAssetsDir();
        std::string GetExecutableDir();
        std::string GetPersistentDir();
        
        Application* GetApplication();
        std::string GetArgv(int index);
        int GetArgc();
        
        double GetMilliseconds();
        void Initialize(int w, int h);
        void Frame();
        void Shutdown();
        void Resize(int w, int h);
        
        std::string GetClipboardText();
        void SetClipboardText(const char* szText);
        
        // Exposed
        FILE* OpenFileDialog(const char* szMode = "w+", const std::vector<std::string>& filter = std::vector<std::string>());
        FILE* SaveFileDialog(const char* szMode = "w+", const std::vector<std::string>& filter = std::vector<std::string>());
        std::string SelectFolderDialog();
        void ShowDialog(const char* szTitle, const char* szMessage);
        
        // Exposed
        bool CreateDirectory(const char* szDirectoryPath);
        bool DirectoryExists(const char* szDirectoryPath);
        bool FileExists(const char* szFilePath);
        std::vector<std::string> ListDirectory(const char* szDirectoryPath);
        bool IsFile(const char* szFilePath);
        bool IsDirectory(const char* szFilePath);
    };
}

#endif