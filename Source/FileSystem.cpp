#include <ES1/Framework/Platform.h>
#include <ES1/Framework/FileSystem.h>
#include <ES1/Framework/DataStream.h>
#include <ES1/Framework/StreamReader.h>
#include <ES1/Framework/StreamWriter.h>

#include <fstream>
#include <istream>
#include <iostream>
#include <algorithm>

ES1::FileSystem::FileSystem(Platform& platform, const char* working, const char* assets, const char* presistent) {
    m_pPlatformInstance = &platform;
    
    if (!DirectoryExists(working)) {
        CreateDirectory(working);
    }
    
    if (!DirectoryExists(assets)) {
        CreateDirectory(assets);
    }
    
    if (!DirectoryExists(presistent)) {
        CreateDirectory(presistent);
    }
}

ES1::FileSystem::~FileSystem() {
    for(std::map<std::string, StreamReader*>::iterator it = m_mapReaders.begin(); it != m_mapReaders.end(); it++) {
        delete it->second;
    }
    for(std::map<std::string, StreamWriter*>::iterator it = m_mapWriters.begin(); it != m_mapWriters.end(); it++) {
        delete it->second;
    }
    for (RefCount::iterator it = m_mapRefCount.begin(); it != m_mapRefCount.end(); it++) {
        delete it->second.second;
    }
    
    m_mapReaders.clear();
    m_mapWriters.clear();
    m_mapRefCount.clear();
}

void ES1::FileSystem::RegisterReader(const char* szType, StreamReader* rProcessor) {
    std::map<std::string, StreamReader*>::iterator it = m_mapReaders.find(szType);
    if(it != m_mapReaders.end()) {
        std::cout << "Trying to insert duplicate reader: " << szType << "\n";
        return;
    }
    
    m_mapReaders.insert(std::pair<std::string, StreamReader*>(szType, rProcessor));
}

void ES1::FileSystem::RegisterWriter(const char* szType, StreamWriter* rProcessor) {
    std::map<std::string, StreamWriter*>::iterator it = m_mapWriters.find(szType);
    if(it != m_mapWriters.end()) {
        std::cout << "Trying to insert duplicate writer: " << szType << "\n";
        return;
    }
    
    m_mapWriters.insert(std::pair<std::string, StreamWriter*>(szType, rProcessor));
}

ES1::DataStream* ES1::FileSystem::Load(const char* szDataPath, const char* szForceType) {
    RefCount::iterator it = m_mapRefCount.find(szDataPath);
    if(it != m_mapRefCount.end()) {
        it->second.first += 1;
        return it->second.second;
    }
    
    std::string type = (szForceType != 0)? szForceType : GetExtension(szDataPath);
    DataStream* newData = m_mapReaders[type]->Load(szDataPath);
    
    std::pair<int, DataStream*> refCount = std::pair<int, DataStream*>(1, newData);
    m_mapRefCount.insert(std::pair<std::string, std::pair<int, DataStream*>>(szDataPath, refCount));
    
    return newData;
}

void ES1::FileSystem::Free(DataStream* pStream) {
    for (RefCount::iterator it = m_mapRefCount.begin(); it != m_mapRefCount.end(); it++) {
        if (it->second.second == pStream) {
            it->second.first -= 1;
            if (it->second.first > 0) {
                return;
            }
            else {
                m_mapRefCount.erase(it);
                break;
            }
        }
    }
    
    delete pStream;
}

void ES1::FileSystem::Save(const DataStream& rTarget, const char* szDataPath, const char* szForceType) {
    std::string type = (szForceType != 0)? szForceType : GetExtension(szDataPath);
    m_mapWriters[type]->Save(rTarget, szDataPath);
}

const std::string& ES1::FileSystem::GetAssetDirectory() {
    return m_strAssetsDirectory;
}

const std::string& ES1::FileSystem::GetWorkingDirectory() {
    return m_strWorkingDirectory;
}

const std::string& ES1::FileSystem::GetPresistentDirectory() {
    return m_strPresistentDirectory;
}

bool ES1::FileSystem::FileExists(const char* szFilePath) {
    std::ifstream testStream(szFilePath);
    bool good = testStream.good();
    testStream.close();
    return good;
}

bool ES1::FileSystem::DirectoryExists(const char* szDirectoryPath) {
    return m_pPlatformInstance->DirectoryExists(szDirectoryPath);
}

bool ES1::FileSystem::CreateFile(const char* szFilePath) {
    std::fstream f;
    f.open(szFilePath, std::ios::out);
    f << std::flush;
    f.close();
    
    return m_pPlatformInstance->FileExists(szFilePath);
}

bool ES1::FileSystem::CreateDirectory(const char* szDirectoryPath) {
    return m_pPlatformInstance->CreateDirectory(szDirectoryPath);
}

std::vector<std::string> ES1::FileSystem::List(const char* szDirectoryPath) {
    return m_pPlatformInstance->ListDirectory(szDirectoryPath);
}

bool ES1::FileSystem::IsFile(const char* szFilePath) {
    return m_pPlatformInstance->IsFile(szFilePath);
}

bool ES1::FileSystem::IsDirectory(const char* szFilePath) {
    return m_pPlatformInstance->IsDirectory(szFilePath);
}

std::string ES1::FileSystem::ReadAllText(const char* szFilePath) {
    std::ifstream f(szFilePath, std::ifstream::in);
    if (!f) {
        std::cout << "Trying to read non existent file: " << szFilePath << "\n";
        return "";
    }
    
    std::string allText;
    
    f.seekg(0, std::ios::end);
    allText.reserve(f.tellg());
    f.seekg(0, std::ios::beg);
    allText.assign((std::istreambuf_iterator<char>(f)), std::istreambuf_iterator<char>());
    f.close();
    
    return allText;
}

std::string ES1::FileSystem::GetExtension(const char* szFilePath) {
    const char *dot = strrchr(szFilePath, '.');
    if(!dot || dot == szFilePath) return "";
    
    std::string extension(dot + 1);
    std::transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
    return extension;
}