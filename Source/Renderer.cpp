#include <ES1/Framework/Renderer.h>
#include <ES1/Framework/OpenGL.h>
#include <ES1/Framework/Math.h>
#include <assert.h>

/* OpenGL Lighting
 if (lightingEnabled) { // glEnable(GL_LIGHTING)
    if (colorDrivenMaterials) { // glEnable(GL_COLOR_MATERIAL)
        // Final polygon color for components specified by glColorMaterial
        // are set by glColor. The rest are set by glMaterial
    }
    else { // glDisable(GL_COLOR_MATERIAL)
        // Final colors for ambient, diffuse, spec and emmision
        // are determined by glMaterial
    }
 }
 else { // glDisable(GL_LIGHTING)
    // Color determined by glColor
 }
*/

// TODO: AFTER THE RENDERER IS FEATURE COMPLETE! Should optimize lights so 8 batch together.
// This is going to be a large undertaking, but should optimize draw calls quiet a bit

void ES1::Renderer::SetMatrixMode(unsigned int mode) {
    if (matrixMode != mode) {
        glMatrixMode(mode);
        matrixMode = mode;
    }
}

ES1::Renderer::Renderer() {
    glClearColor(ES1::Background.r, ES1::Background.g, ES1::Background.b, ES1::Background.a);
    glClearDepth(1.0f);
    
    clearFlags = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT;
    
    matrixMode = GL_MODELVIEW;
    glLoadIdentity();
    
    lightingEnabled = false;
    glDisable(GL_LIGHTING);
    
    depthTestEnabled = true;
    glEnable(GL_DEPTH_TEST);
    
    glDisable(GL_COLOR_MATERIAL);
    
    SetMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    SetMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    
    float emission[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    materialAmbient = Color4f(0.2f, 0.2f, 0.2f, 1.0f);
    materialDiffuse = Color4f(0.8f, 0.8f, 0.8f, 1.0f);
    materialSpecular = Color4f(0.0f, 0.0f, 0.0f, 1.0f);
    materialShine = 0.0f;
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, materialAmbient.rgba);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, materialDiffuse.rgba);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, materialSpecular.rgba);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emission);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, materialShine);
    glColor4fv(materialDiffuse.rgba);
    
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);
    
    GLfloat AmbientColor[] = {0.2f, 0.2f, 0.2f,1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, AmbientColor);
    ambientIntensity = 0.2f;
    
    glDisable(GL_LIGHT0);
    lightEnabled = false;
    
    glDisable(GL_BLEND);
    blendingEnabled = false;
    
    glDepthMask(GL_TRUE);
    depthWriteEnabled = true;
    
    glShadeModel(GL_SMOOTH);
    smoothShadingEnabled = true;
    boundDiffuse = 0;
    colorBlendingEnabled = false;
    
    wireOutlineEnabled = false;
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    
    DisableColorBlending();
    
    glDisable(GL_TEXTURE_2D);
    textureEnabled = false;
}

ES1::Renderer::~Renderer() {
    if (boundDiffuse != 0) {
        Texture::Unbind();
        boundDiffuse = 0;
    }
}

void ES1::Renderer::SetClearColor(ES1::Color color) {
    glClearColor(color.r, color.g, color.b, color.a);
}

void ES1::Renderer::SetAmbientIntensity(float ambient) {
    if (ambient < 0.0f) {
        ambient = 0.0f;
    }
    else if (ambient > 1.0f) {
        ambient =1.0f;
    }
    
    if (Equals(ambient, ambientIntensity)) {
        return;
    }
    
    GLfloat AmbientColor[] = {ambient, ambient, ambient,1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, AmbientColor);
    ambientIntensity = ambient;
}

void ES1::Renderer::SetClearDepth(float depth) {
    glClearDepth(depth);
}

void ES1::Renderer::ConfigClearColor(bool enable) {
    if (enable) {
        clearFlags |= GL_COLOR_BUFFER_BIT;
    }
    else {
        clearFlags &= ~GL_COLOR_BUFFER_BIT;
    }
}

void ES1::Renderer::ConfigClearDepth(bool enable) {
    if (enable) {
        clearFlags |= GL_DEPTH_BUFFER_BIT;
    }
    else {
        clearFlags &= ~GL_DEPTH_BUFFER_BIT;
    }
}

void ES1::Renderer::ConfigDepthWrite(bool enable) {
    if (enable) {
        if (!depthWriteEnabled) {
            glDepthMask(GL_TRUE);
        }
    }
    else {
        if (depthWriteEnabled) {
            glDepthMask(GL_FALSE);
        }
    }
    depthWriteEnabled = enable;
}

void ES1::Renderer::ConfigBlending(bool enable) {
    if (enable) {
        if (!blendingEnabled) {
            glEnable(GL_BLEND);
        }
    }
    else {
        if (blendingEnabled) {
            glDisable(GL_BLEND);
        }
    }
    blendingEnabled = enable;
}

void ES1::Renderer::ConfigTexture(bool enable) {
    if (enable) {
        if (!textureEnabled) {
            glEnable(GL_TEXTURE_2D);
        }
    }
    else {
        if (textureEnabled) {
            glDisable(GL_TEXTURE_2D);
        }
    }
    
    textureEnabled = enable;
}

void ES1::Renderer::ConfigWireframe(bool enable) {
    if (enable) {
        if (!wireOutlineEnabled) {
            glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        }
    }
    else {
        if (wireOutlineEnabled) {
            glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
        }
    }
    
    wireOutlineEnabled = enable;
}

void ES1::Renderer::ConfigSmoothShading(bool enable) {
    if (enable) {
        if (!smoothShadingEnabled) {
            glShadeModel(GL_SMOOTH);
        }
    }
    else {
        if (smoothShadingEnabled) {
            glShadeModel(GL_FLAT);
        }
    }
    smoothShadingEnabled = enable;
}

void ES1::Renderer::ConfigDepthTest(bool enable) {
    if (enable) {
        if (!depthTestEnabled) {
            glEnable(GL_DEPTH_TEST);
        }
    }
    else {
        if (depthTestEnabled) {
            glDisable(GL_DEPTH_TEST);
        }
    }
    depthTestEnabled = enable;
}

void ES1::Renderer::ConfigLighting(bool enable) {
    if (enable) {
        if (!lightingEnabled) {
            glEnable(GL_LIGHTING);
        }
    }
    else {
        if (lightingEnabled) {
            glDisable(GL_LIGHTING);
        }
    }
    lightingEnabled = enable;
}

void ES1::Renderer::Clear() {
    glClear(clearFlags);
    ClearLights();
    
    objectsDrawn = 0;
    numRenderCalls = 0;
}

void ES1::Renderer::SetView(Matrix& v) {
    view = v;
    glLoadMatrixf(view.m);
}

void ES1::Renderer::SetProjection(Matrix& p) {
    assert(matrixMode == GL_MODELVIEW);
    projection = p;
    SetMatrixMode(GL_PROJECTION);
    glLoadMatrixf(projection.m);
    SetMatrixMode(GL_MODELVIEW);
}

ES1::Matrix ES1::Renderer::GetView() {
    return view;
}

ES1::Matrix ES1::Renderer::GetProjection() {
    return projection;
}

void ES1::Renderer::Ortho(float left, float right, float bottom, float top, float _near, float _far) {
    assert(matrixMode == GL_MODELVIEW);
    
    SetMatrixMode(GL_PROJECTION);
    projection = ES1::Ortho(left, right, bottom, top, _near, _far);
    glLoadMatrixf(projection.m);
    SetMatrixMode(GL_MODELVIEW);
}

void ES1::Renderer::Perspective(float fov, float aspect, float _near, float _far) {
    assert(matrixMode == GL_MODELVIEW);
    
    SetMatrixMode(GL_PROJECTION);
    projection = ES1::Perspective(fov, aspect, _near, _far);
    glLoadMatrixf(projection.m);
    SetMatrixMode(GL_MODELVIEW);
}

void ES1::Renderer::LookAt(const ES1::Vector& position, const ES1::Vector& direction) {
    assert(matrixMode == GL_MODELVIEW);
    
    view = ES1::LookAt(position, direction);
    glLoadMatrixf(view.m);
}

void ES1::Renderer::BindMaterial(ES1::Material& material) {
    if (material.lit && !material.wireframe) {
        if (!lightingEnabled) {
            glEnable(GL_LIGHTING);
            lightingEnabled = true;
        }
        
        if (materialAmbient != material.ambient) {
            glMaterialfv(GL_FRONT, GL_AMBIENT, material.ambient.rgba);
            materialAmbient = material.ambient;
        }
        
        if (materialDiffuse != material.diffuse) {
            glColor4f(material.diffuse.r, material.diffuse.g, material.diffuse.b, material.diffuse.a);
            glMaterialfv(GL_FRONT, GL_DIFFUSE, material.diffuse.rgba);
            materialDiffuse = material.diffuse;
        }
        
        if (materialSpecular != material.specular) {
            glMaterialfv(GL_FRONT, GL_SPECULAR, material.specular.rgba);
            materialSpecular = material.specular;
        }
        
        if (!Equals(materialShine, material.shininess)) {
            // Shininess is between 0 and 128, not 0 and 1. Let's fix that!
            glMaterialf(GL_FRONT, GL_SHININESS, material.shininess * 128.0f);
            materialShine = material.shininess;
        }
    }
    else {
        if (lightingEnabled) {
            glDisable(GL_LIGHTING);
            lightingEnabled = false;
        }
        
        if (materialDiffuse != material.diffuse) {
            glColor4fv(material.diffuse.rgba);
            glMaterialfv(GL_FRONT, GL_DIFFUSE, material.diffuse.rgba);
            materialDiffuse = material.diffuse;
        }
    }
    
    ConfigSmoothShading(!material.flatShaded);
    ConfigWireframe(material.wireframe);
    
    if (material.diffuseTexture != 0) {
        ConfigTexture(true);
        
        if (boundDiffuse != 0 && boundDiffuse != material.diffuseTexture) {
            Texture::Unbind();
            boundDiffuse = 0;
        }
        
        material.diffuseTexture->Bind();
        boundDiffuse = material.diffuseTexture;
    }
    else {
        ConfigTexture(false);
        Texture::Unbind();
    }
    
    /*
     Texture* normalTexture;
    */
    
    /* TODO
     bool castShadow;
     bool recieveShadow;
     // Bloom?
     bool renderOcclusion; // Behind wall effect
    */
}

void ES1::Renderer::BeginLines(ES1::Color color, ES1::Matrix& transform) {
    glDisable(GL_LIGHTING);
    ConfigTexture(false);
    
    
    glPushMatrix();
    glMultMatrixf(transform.m);
    
    glBegin(GL_LINES);
    glColor3f(color.r, color.g, color.b);
}

void ES1::Renderer::EndLines() {
    glEnd();
    glColor4fv(materialDiffuse.rgba);
    glPopMatrix();
    if (lightingEnabled) {
        glEnable(GL_LIGHTING);
    }
    ConfigTexture(true);
    
    numRenderCalls += 1;
    objectsDrawn += 1;
}

void ES1::Renderer::Vertex(float x, float y, float z) {
    glVertex3f(x, y, z);
}

void ES1::Renderer::Vertex(float* xyz) {
    glVertex3fv(xyz);
}

void ES1::Renderer::Line(ES1::Vector& p1, ES1::Vector& p2, ES1::Color color, ES1::Matrix& transform) {
    Line(p1.xyz, p2.xyz, color, transform);
}

void ES1::Renderer::Line(float* p1,float* p2, ES1::Color color, ES1::Matrix& transform) {
    glDisable(GL_LIGHTING);
    ConfigTexture(false);
    
    glPushMatrix();
    glMultMatrixf(transform.m);
    
    glBegin(GL_LINES);
    glColor3f(color.r, color.g, color.b);
    glVertex3fv(p1);
    glVertex3fv(p2);
    glEnd();
    glColor4fv(materialDiffuse.rgba);
    glPopMatrix();
    if (lightingEnabled) {
        glEnable(GL_LIGHTING);
    }
    ConfigTexture(true);
    
    numRenderCalls += 1;
    objectsDrawn += 1;
}

// Lighting Pass 2 and so forth
void ES1::Renderer::EnableColorBlending() {
    if (colorBlendingEnabled) {
        return;
    }
    
    ConfigDepthTest(true);
    ConfigBlending(true);
    ConfigDepthWrite(false);
    glDepthFunc(GL_EQUAL);
    
    GLfloat AmbientColor[] = {0.0f, 0.0f, 0.0f,1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, AmbientColor);
    colorBlendingEnabled = true;
}

// Lighting Pass 1
void ES1::Renderer::DisableColorBlending() {
    if (!colorBlendingEnabled) {
        return;
    }
    
    ConfigDepthTest(true);
    ConfigBlending(false);
    ConfigDepthWrite(true);
    glDepthFunc(GL_LEQUAL);
    
    GLfloat AmbientColor[] = {ambientIntensity, ambientIntensity, ambientIntensity,1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, AmbientColor);
    colorBlendingEnabled = false;
}

// http://gamedev.stackexchange.com/questions/56897/glsl-light-attenuation-color-and-intensity-formula
void ES1::Renderer::BindLight(PointLight* light) {
    const float noAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
    float position[] = { light->lightPosition.x, light->lightPosition.y, light->lightPosition.z, 1.0f };
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, noAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light->lightColor.rgba);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light->lightColor.rgba);
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.0f);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 1.0f / Sqrt(light->lightRadius));
    glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.0f);
}

void ES1::Renderer::BindLight(DirectionalLight* light) {
    const float noAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
    float position[] = { light->lightDirection.x, light->lightDirection.y, light->lightDirection.z, 0.0f };
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, noAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light->lightColor.rgba);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light->lightColor.rgba);
    glLightfv(GL_LIGHT0, GL_POSITION, position);    
}

void ES1::Renderer::PushLight(ES1::PointLight* light) {
    pointLights.push_back(light);
}

void ES1::Renderer::PushLight(ES1::DirectionalLight* light) {
    directionalLights.push_back(light);
}

void ES1::Renderer::ClearLights() {
    pointLights.clear();
    directionalLights.clear();
}

int ES1::Renderer::GetNumObjectsRendered() {
    return numRenderCalls;
}

int ES1::Renderer::GetNumDrawCalls() {
    return objectsDrawn;
}

void ES1::Renderer::Render(ES1::Mesh& mesh, ES1::Material& material, ES1::Matrix& transform) {
    if (!mesh.HasVertices()) {
        return;
    }
    assert(mesh.HasVertices());
    assert(matrixMode == GL_MODELVIEW);
    
    BindMaterial(material);
    DisableColorBlending();
    
    unsigned int numLights = (unsigned int)pointLights.size() + (unsigned int)directionalLights.size();
    if ((material.lit && numLights > 0) && !lightEnabled) {
        glEnable(GL_LIGHT0);
        lightEnabled = true;
    }
    else if ((!material.lit || numLights <= 0) && lightEnabled) {
        glDisable(GL_LIGHT0);
        lightEnabled = false;
    }
    
    glEnableClientState(GL_VERTEX_ARRAY);
    if (mesh.HasColors()) {
        glEnableClientState(GL_COLOR_ARRAY);
    }
    if (mesh.HasTexCoords()) {
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    }
    if (mesh.HasNormals()) {
        glEnableClientState(GL_NORMAL_ARRAY);
    }
    
    glVertexPointer(3, GL_FLOAT, 0, mesh.GetVertices());
    if (mesh.HasColors()) {
        glColorPointer(3, GL_FLOAT, 0, mesh.GetColors());
    }
    if (mesh.HasTexCoords()) {
        glTexCoordPointer(2, GL_FLOAT, 0, mesh.GetTexCoords());
    }
    if (mesh.HasNormals()) {
        glNormalPointer(GL_FLOAT, 0, mesh.GetNormals());
    }
    
    if (lightingEnabled && material.lit && numLights > 0) {
        // Got a bunch of lights. Need to render the first as solid, with ambient enabled.
        // then, disable global ambient and render each other light additiveley!
        for (unsigned int i = 0, size = (unsigned int)directionalLights.size(); i < size; ++i) {
            BindLight(directionalLights[i]);
            
            glPushMatrix();
            glMultMatrixf(transform.m);
            if (mesh.HasIndices()) {
                glDrawElements(GL_TRIANGLES, mesh.GetIndexCount(), GL_UNSIGNED_INT, mesh.GetIndices());
                numRenderCalls += 1;
            }
            else {
                glDrawArrays(GL_TRIANGLES, 0, mesh.GetVertexCount());
                numRenderCalls += 1;
            }
            glPopMatrix();
            
            EnableColorBlending();
        }
        
        for (unsigned int i = 0, size = (unsigned int)pointLights.size(); i < size; ++i) {
            BindLight(pointLights[i]);
            
            glPushMatrix();
            glMultMatrixf(transform.m);
            if (mesh.HasIndices()) {
                glDrawElements(GL_TRIANGLES, mesh.GetIndexCount(), GL_UNSIGNED_INT, mesh.GetIndices());
                numRenderCalls += 1;
            }
            else {
                glDrawArrays(GL_TRIANGLES, 0, mesh.GetVertexCount());
                numRenderCalls += 1;
            }
            glPopMatrix();
            
            EnableColorBlending();
        }
    }
    else { // Unlit, no fancy blending. Just draw the model the way it is
        glPushMatrix();
        glMultMatrixf(transform.m);
        if (mesh.HasIndices()) {
            glDrawElements(GL_TRIANGLES, mesh.GetIndexCount(), GL_UNSIGNED_INT, mesh.GetIndices());
            numRenderCalls += 1;
        }
        else {
            glDrawArrays(GL_TRIANGLES, 0, mesh.GetVertexCount());
            numRenderCalls += 1;
        }
        glPopMatrix();
    }
    
    objectsDrawn += 1;
    
    
    // TODO: Maybe this only needs to happen at the top???
    // Depends on how the normal mapping will work i guess
    DisableColorBlending();
    
    if (mesh.HasNormals()) {
        glDisableClientState(GL_NORMAL_ARRAY);
    }
    if (mesh.HasTexCoords()) {
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    }
    if (mesh.HasColors()) {
        glDisableClientState(GL_COLOR_ARRAY);
    }
    glDisableClientState(GL_VERTEX_ARRAY);
}
