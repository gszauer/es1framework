#include <ES1/Framework/Color.h>
#include <ES1/Framework/Math.h>

namespace ES1 {
    Color Black(0.0f, 0.0f, 0.0f);
    Color Yellow(1.0f, 1.0f, 0.0f);
    Color Ambient(0.2f, 0.2f, 0.2f);
    Color White(1.0f, 1.0f, 1.0f);
    Color Red(1.0f, 0.0f, 0.0f);
    Color Green(0.0f, 1.0f, 0.0f);
    Color Blue(0.0f, 0.0f, 1.0f);
    Color Magenta(1.0f, 0.0f, 1.0f);
    Color Background(0.5f, 0.6f, 0.7f);
}


ES1::Color4f::Color4f() {
    r = g = b = a = 1.0f;
}

ES1::Color4f::Color4f(float _r, float _g, float _b) {
    r = _r;
    g = _g;
    b = _b;
    a = 1.0f;
}

ES1::Color4f::Color4f(float _r, float _g, float _b, float _a) {
    r = _r;
    g = _g;
    b = _b;
    a = _a;
}

/*ES1::Color4f::Color4f(int _r, int _g, int _b) {
    r = ((float)_r) / 255.0f;
    g = ((float)_g) / 255.0f;
    b = ((float)_b) / 255.0f;
    a = 1.0f;
}

ES1::Color4f::Color4f(int _r, int _g, int _b, int _a) {
    r = ((float)_r) / 255.0f;
    g = ((float)_g) / 255.0f;
    b = ((float)_b) / 255.0f;
    a = ((float)_a) / 255.0f;
}*/

ES1::Color4f::Color4f(const ES1::Color4f& other) {
    r = other.r;
    g = other.g;
    b = other.b;
    a = other.a;
}

ES1::Color4f& ES1::Color4f::operator=(const ES1::Color4f& other) {
    r = other.r;
    g = other.g;
    b = other.b;
    a = other.a;
    
    return *this;
}

bool ES1::operator==(const ES1::Color4f& c1, const ES1::Color4f& c2) {
    return Equals(c1.a, c2.a) && Equals(c1.r, c2.r) && Equals(c1.g, c2.g) && Equals(c1.b, c2.b);
}

bool ES1::operator!=(const ES1::Color4f& c1, const ES1::Color4f& c2) {
    return !(c1 == c2);
}