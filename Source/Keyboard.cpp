#include <ES1/Framework/Keyboard.h>

ES1::Keyboard::Keyboard() {
    for (int i = 0; i < 156; ++i) {
        m_pKeyState[i] = false;
        m_pPrevKeyState[i] = false;
    }
}

ES1::Keyboard::~Keyboard() { }

bool ES1::Keyboard::KeyDown(Keydef eKey) {
    return m_pKeyState[KeyIndex(eKey)];
}

bool ES1::Keyboard::KeyUp(Keydef eKey) {
    return !m_pKeyState[KeyIndex(eKey)];
}

bool ES1::Keyboard::KeyPressed(Keydef eKey) {
    int index = KeyIndex(eKey);
    return (!m_pPrevKeyState[index]) && m_pKeyState[index];
}
bool ES1::Keyboard::KeyReleased(Keydef eKey) {
    int index = KeyIndex(eKey);
    return m_pPrevKeyState[index] && (!m_pKeyState[index]);
}

void ES1::Keyboard::SetKeyPressed(Keydef eKey) {
    m_pKeyState[KeyIndex(eKey)] = true;
}

void ES1::Keyboard::SetKeyReleased(Keydef eKey) {
    m_pKeyState[KeyIndex(eKey)] = false;
}

void ES1::Keyboard::Update() {
    for (int i = 0; i < 156; ++i) {
        m_pPrevKeyState[i] = m_pKeyState[i];
    }
}