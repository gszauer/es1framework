#include <ES1/Framework/Application.h>
#include <ES1/Framework/NullLogger.h>

ES1::Application* ES1::Application::g_pLastInstance = 0;
ES1::NullLogger g_NullLogger;

ES1::Application::Application() {
    g_pLastInstance = this;
    m_strAppName = "ES1Framework";
    SetLoggers(&g_NullLogger, &g_NullLogger, &g_NullLogger);
}

ES1::Application::Application(const char* name) {
    g_pLastInstance = this;
    m_strAppName = name;
    SetLoggers(&g_NullLogger, &g_NullLogger, &g_NullLogger);
}

ES1::Application::~Application() { }

ES1::Application* ES1::Application::GetInstance() {
    return g_pLastInstance;
}

void ES1::Application::SetFileSystem(ES1::FileSystem* pFS) {
    m_pFileSystem = pFS;
}

ES1::FileSystem* ES1::Application::GetFileSystem() {
    return m_pFileSystem;
}

void ES1::Application::SetKeyboard(ES1::Keyboard* pKb) {
    m_pKeyboard = pKb;
}

ES1::Keyboard* ES1::Application::GetKeyboard() {
    return m_pKeyboard;
}

void ES1::Application::SetMouse(ES1::Mouse* pMouse) {
    m_pMouse = pMouse;
}

ES1::Mouse* ES1::Application::GetMouse() {
    return m_pMouse;
}

void ES1::Application::SetLoggers(ES1::Logger* debug, ES1::Logger* warning, ES1::Logger* error) {
    m_pDebugLog = debug;
    m_pWarningLog = warning;
    m_pErrorLog = error;
}

ES1::Logger* ES1::Application::GetDebugLog() {
    return m_pDebugLog;
}

ES1::Logger* ES1::Application::GetWarningLog() {
    return  m_pWarningLog;
}

ES1::Logger* ES1::Application::GetErrorLog() {
    return  m_pErrorLog;
}

std::string ES1::Application::GetName() {
    return m_strAppName;
}

void ES1::Application::SetDialogWindows(ES1::DialogWindows* pWindows) {
    m_pWindows = pWindows;
}

ES1::DialogWindows* ES1::Application::GetDialog() {
    return m_pWindows;
}

void ES1::Application::SetClipboard(ES1::Clipboard* pClipboard) {
    m_pClipboard = pClipboard;
}

ES1::Clipboard* ES1::Application::GetClipboard() {
    return m_pClipboard;
}