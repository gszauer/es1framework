#include <ES1/Framework/DataStream.h>

ES1::DataStream::DataStream(const char* szType) {
    m_strType = szType;
    m_pRawData = 0;

}

ES1::DataStream::~DataStream() { }

const std::string& ES1::DataStream::GetType() {
    return m_strType;
}

void* ES1::DataStream::GetRawData() {
    return m_pRawData;
}
