#include <ES1/Framework/Matrix.h>
#include <ES1/Framework/Math.h>

// Not used here, but good video: https://www.khanacademy.org/math/linear-algebra/matrix_transformations/determinant_depth/v/linear-algebra-simpler-4x4-determinant
// Good article on determinants: https://people.richland.edu/james/lecture/m116/matrices/determinant.html
// Determinant with test: http://www.sophia.org/tutorials/finding-the-determinant-of-a-4x4-matrix--5
// Matrix rotaiton: http://www.cprogramming.com/tutorial/3d/rotationMatrices.html
// Arbitrary axis rotation: http://www.cprogramming.com/tutorial/3d/rotation.html
// Linear Transforms: https://www.khanacademy.org/math/linear-algebra/matrix_transformations/lin_trans_examples/v/rotation-in-r3-around-the-x-axis
// Pretty good for perspective: // http://forums.4fips.com/viewtopic.php?f=3&t=1059
// LookAt: http://stackoverflow.com/questions/24492274/compatibility-function-for-glulookat
// http://gamedev.stackexchange.com/questions/16719/what-is-the-correct-order-to-multiply-scale-rotation-and-translation-matrices-f
// http://stackoverflow.com/questions/12838375/model-matrix-in-glm

ES1::Matrix::Matrix() :
                    _11(1.0f), _12(0.0f), _13(0.0f), _14(0.0f),
                    _21(0.0f), _22(1.0f), _23(0.0f), _24(0.0f),
                    _31(0.0f), _32(0.0f), _33(1.0f), _34(0.0f),
                    _41(0.0f), _42(0.0f), _43(0.0f), _44(1.0f) { }

ES1::Matrix::Matrix(float* v) {
    for (int i = 0; i < 16; m[i] = v[i], ++i);
}

ES1::Matrix::Matrix(float m11, float m21, float m31, float m41,
                    float m12, float m22, float m32, float m42,
                    float m13, float m23, float m33, float m43,
                    float m14, float m24, float m34, float m44) :
                    _11(m11), _12(m12), _13(m13), _14(m14),
                    _21(m21), _22(m22), _23(m23), _24(m24),
                    _31(m31), _32(m32), _33(m33), _34(m34),
                    _41(m41), _42(m42), _43(m43), _44(m44) { }

ES1::Matrix::Matrix(const Matrix& m) :
                    _11(m._11), _12(m._12), _13(m._13), _14(m._14),
                    _21(m._21), _22(m._22), _23(m._23), _24(m._24),
                    _31(m._31), _32(m._32), _33(m._33), _34(m._34),
                    _41(m._41), _42(m._42), _43(m._43), _44(m._44) { }

ES1::Matrix& ES1::Matrix::operator=(const Matrix& other) {
    for (int i = 0; i < 16; m[i] = other.m[i], ++i);
    return *this;
}

float* ES1::Matrix::operator [](int i) {
    return &(m[i*4]);
}

void ES1::Matrix::GetRow(int i, float* o) {
    o[0] = m[0*4 + i];
    o[1] = m[1*4 + i];
    o[2] = m[2*4 + i];
    o[3] = m[3*4 + i];
}

void ES1::Matrix::GetColumn(int i, float* o) {
    o[0] = m[i*4 + 0];
    o[1] = m[i*4 + 1];
    o[2] = m[i*4 + 2];
    o[3] = m[i*4 + 3];
}

ES1::Matrix ES1::operator*(const ES1::Matrix& v, float f) {
    return ES1::Matrix(v._11 * f, v._21 * f, v._31 * f, v._41 * f,
                       v._12 * f, v._22 * f, v._32 * f, v._42 * f,
                       v._13 * f, v._23 * f, v._33 * f, v._43 * f,
                       v._14 * f, v._24 * f, v._34 * f, v._44 * f);
}

//#include <iostream>
// For every element, dot produc the i-th row and j-th column
// i is row, j is column. Address the matrix appropriateley mat[j][i]
ES1::Matrix ES1::operator*(const ES1::Matrix& m1, const ES1::Matrix& m2) {
    ES1::Matrix result;
    
    ES1::Matrix& _m1 = const_cast<ES1::Matrix&>(m1);
    ES1::Matrix& _m2 = const_cast<ES1::Matrix&>(m2);
    
    /*std::cout << "operator*:\n";
    std::cout << "float row[4] = {0.0f};\n";
    std::cout << "float col[4] = {0.0f};\n";
    float row[4] = {0.0f};
    float col[4] = {0.0f};
    
    for (int i = 0; i < 4; ++i) { // i is ROW
        for (int j = 0; j < 4; ++j) { // j is COLUMN
            _m1.GetRow(i, row);
            _m2.GetColumn(j, col);
            result[j][i] = Dot4fv(row, col);
            
            std::cout << "_m1.GetRow(" << i << ", row);\n";
            std::cout << "_m2.GetColumn(" << j << ", col);\n";
            std::cout << "result[" << j << "][" << i << "] = Dot4fv(row, col);\n\n";
        }
    }
    std::cout << "Done\n";*/
    
    /*result[0][0] = ES1::Dot(_m1.Row(0), _m2.Column(0));
    result[1][0] = ES1::Dot(_m1.Row(0), _m2.Column(1));
    result[2][0] = ES1::Dot(_m1.Row(0), _m2.Column(2));
    result[3][0] = ES1::Dot(_m1.Row(0), _m2.Column(3));
    result[0][1] = ES1::Dot(_m1.Row(1), _m2.Column(0));
    result[1][1] = ES1::Dot(_m1.Row(1), _m2.Column(1));
    result[2][1] = ES1::Dot(_m1.Row(1), _m2.Column(2));
    result[3][1] = ES1::Dot(_m1.Row(1), _m2.Column(3));
    result[0][2] = ES1::Dot(_m1.Row(2), _m2.Column(0));
    result[1][2] = ES1::Dot(_m1.Row(2), _m2.Column(1));
    result[2][2] = ES1::Dot(_m1.Row(2), _m2.Column(2));
    result[3][2] = ES1::Dot(_m1.Row(2), _m2.Column(3));
    result[0][3] = ES1::Dot(_m1.Row(3), _m2.Column(0));
    result[1][3] = ES1::Dot(_m1.Row(3), _m2.Column(1));
    result[2][3] = ES1::Dot(_m1.Row(3), _m2.Column(2));
    result[3][3] = ES1::Dot(_m1.Row(3), _m2.Column(3));*/
    
    float row[4] = {0.0f};
    float col[4] = {0.0f};
    _m1.GetRow(0, row);
    _m2.GetColumn(0, col);
    result[0][0] = Dot4fv(row, col);
    
    _m1.GetRow(0, row);
    _m2.GetColumn(1, col);
    result[1][0] = Dot4fv(row, col);
    
    _m1.GetRow(0, row);
    _m2.GetColumn(2, col);
    result[2][0] = Dot4fv(row, col);
    
    _m1.GetRow(0, row);
    _m2.GetColumn(3, col);
    result[3][0] = Dot4fv(row, col);
    
    _m1.GetRow(1, row);
    _m2.GetColumn(0, col);
    result[0][1] = Dot4fv(row, col);
    
    _m1.GetRow(1, row);
    _m2.GetColumn(1, col);
    result[1][1] = Dot4fv(row, col);
    
    _m1.GetRow(1, row);
    _m2.GetColumn(2, col);
    result[2][1] = Dot4fv(row, col);
    
    _m1.GetRow(1, row);
    _m2.GetColumn(3, col);
    result[3][1] = Dot4fv(row, col);
    
    _m1.GetRow(2, row);
    _m2.GetColumn(0, col);
    result[0][2] = Dot4fv(row, col);
    
    _m1.GetRow(2, row);
    _m2.GetColumn(1, col);
    result[1][2] = Dot4fv(row, col);
    
    _m1.GetRow(2, row);
    _m2.GetColumn(2, col);
    result[2][2] = Dot4fv(row, col);
    
    _m1.GetRow(2, row);
    _m2.GetColumn(3, col);
    result[3][2] = Dot4fv(row, col);
    
    _m1.GetRow(3, row);
    _m2.GetColumn(0, col);
    result[0][3] = Dot4fv(row, col);
    
    _m1.GetRow(3, row);
    _m2.GetColumn(1, col);
    result[1][3] = Dot4fv(row, col);
    
    _m1.GetRow(3, row);
    _m2.GetColumn(2, col);
    result[2][3] = Dot4fv(row, col);
    
    _m1.GetRow(3, row);
    _m2.GetColumn(3, col);
    result[3][3] = Dot4fv(row, col);

    return result;
}

ES1::Matrix& ES1::operator*=(ES1::Matrix& m, float f) {
    for (int i = 0; i < 16; ++i) {
        m.m[i] *= f;
    }
    return m;
}

ES1::Matrix& ES1::operator*=(ES1::Matrix& result, const ES1::Matrix& m2) {
    ES1::Matrix m1 = result;
    
    ES1::Matrix& _m1 = const_cast<ES1::Matrix&>(m1);
    ES1::Matrix& _m2 = const_cast<ES1::Matrix&>(m2);
    
    float row[4] = {0.0f};
    float col[4] = {0.0f};
    _m1.GetRow(0, row);
    _m2.GetColumn(0, col);
    result[0][0] = Dot4fv(row, col);
    
    _m1.GetRow(0, row);
    _m2.GetColumn(1, col);
    result[1][0] = Dot4fv(row, col);
    
    _m1.GetRow(0, row);
    _m2.GetColumn(2, col);
    result[2][0] = Dot4fv(row, col);
    
    _m1.GetRow(0, row);
    _m2.GetColumn(3, col);
    result[3][0] = Dot4fv(row, col);
    
    _m1.GetRow(1, row);
    _m2.GetColumn(0, col);
    result[0][1] = Dot4fv(row, col);
    
    _m1.GetRow(1, row);
    _m2.GetColumn(1, col);
    result[1][1] = Dot4fv(row, col);
    
    _m1.GetRow(1, row);
    _m2.GetColumn(2, col);
    result[2][1] = Dot4fv(row, col);
    
    _m1.GetRow(1, row);
    _m2.GetColumn(3, col);
    result[3][1] = Dot4fv(row, col);
    
    _m1.GetRow(2, row);
    _m2.GetColumn(0, col);
    result[0][2] = Dot4fv(row, col);
    
    _m1.GetRow(2, row);
    _m2.GetColumn(1, col);
    result[1][2] = Dot4fv(row, col);
    
    _m1.GetRow(2, row);
    _m2.GetColumn(2, col);
    result[2][2] = Dot4fv(row, col);
    
    _m1.GetRow(2, row);
    _m2.GetColumn(3, col);
    result[3][2] = Dot4fv(row, col);
    
    _m1.GetRow(3, row);
    _m2.GetColumn(0, col);
    result[0][3] = Dot4fv(row, col);
    
    _m1.GetRow(3, row);
    _m2.GetColumn(1, col);
    result[1][3] = Dot4fv(row, col);
    
    _m1.GetRow(3, row);
    _m2.GetColumn(2, col);
    result[2][3] = Dot4fv(row, col);
    
    _m1.GetRow(3, row);
    _m2.GetColumn(3, col);
    result[3][3] = Dot4fv(row, col);
    
    return result;
}

//#include <iostream>
bool ES1::operator==(const ES1::Matrix& lhs, const ES1::Matrix& rhs) {
    for (int i = 0; i < 16; ++i) {
        if (!ES1::Equals(lhs.m[i], rhs.m[i])) {
            //std::cout << "Not equals: " <<lhs.m[i] << ", " << rhs.m[i] << "\n";
            return false;
        }
    }
    return true;
}

bool ES1::operator!=(const ES1::Matrix& lhs, const ES1::Matrix& rhs) {
    return !operator==(lhs,rhs);
}

ES1::Matrix ES1::Transpose(const ES1::Matrix& m) {
    return ES1::Matrix(m._11, m._12, m._13, m._14,
                       m._21, m._22, m._23, m._24,
                       m._31, m._32, m._33, m._34,
                       m._41, m._42, m._43, m._44);
}

ES1::Vector ES1::MultiplyPoint(const Matrix& m, const ES1::Vector& vector) {
    ES1::Matrix* pm = (ES1::Matrix*)&m;
    
    float result[4] = {0.0f};
    float v[4] = {vector.x, vector.y, vector.z, 1.0f};
    float row[4] = {0.0f};
    
    pm->GetRow(0, row);
    result[0] = Dot4fv(row, v);
    
    pm->GetRow(1, row);
    result[1] = Dot4fv(row, v);
    
    pm->GetRow(2, row);
    result[2] = Dot4fv(row, v);
    
    pm->GetRow(3, row);
    result[3] = Dot4fv(row, v);

    return Vector(result);
}

ES1::Vector ES1::MultiplyVector(const Matrix& m, const ES1::Vector& vector) {
    ES1::Matrix* pm = (ES1::Matrix*)&m;
    float result[4] = {0.0f};
    float v[4] = {vector.x, vector.y, vector.z, 0.0f};
    float row[4] = {0.0f};
    
    pm->GetRow(0, row);
    result[0] = Dot4fv(row, v);
    
    pm->GetRow(1, row);
    result[1] = Dot4fv(row, v);
    
    pm->GetRow(2, row);
    result[2] = Dot4fv(row, v);
    
    pm->GetRow(3, row);
    result[3] = Dot4fv(row, v);
    
    return Vector(result);
}

float ES1::Determinant(const ES1::Matrix& m) {
    /* Find the determinant a 4x4 matrix(M)
     11 12 13 14
     21 22 23 24
     31 32 33 34
     41 42 43 44
     
     Find the cofactor of 11
     11 [] [] []
     [] 22 23 24
     [] 32 33 34
     [] 42 43 44
     
     Find the determinant of the submatrix
     22 23 24
     32 33 34
     42 43 44
     
     det(11) = 22(33 * 44 - 34 * 43) - 23(32 * 44 - 34 * 42) + 24(32 * 43 - 33 * 42)
     cofactor(11) = +det(11)
     
     Find the cofactor of 12
     [] 12 [] []
     21 [] 23 24
     31 [] 33 34
     41 [] 43 44
     
     Find the determinent of the submatrix
     21 23 24
     31 33 34
     41 43 44
     
     det(12) = 21(33 * 44 - 34 * 43) - 23(31 * 44 - 34 * 41) + 24(31 * 43 - 33 * 41)
     cofactor(12) = -det(12)
     
     Find the cofactor of 13
     [] [] 13 []
     21 22 [] 24
     31 32 [] 34
     41 42 [] 44
     
     Find the determinent of the submatrix
     21 22 24
     31 32 34
     41 42 44
     
     det(13) = 21(32 * 44 - 34 * 42) - 22(31 * 44 - 34 * 41) + 24(31 * 42 - 32 * 41)
     cofactor(13) = +det(13)
     
     Find the cofactor of 14
     [] [] [] 14
     21 22 23 []
     31 32 33 []
     41 42 43 []
     
     Find the determinant of the submatrix
     21 22 23
     31 32 33
     41 42 43
     
     det(14) = 21(32 * 43 - 33 * 42) - 22(31 * 43 - 33 * 41) + 23(31 * 42 - 32 * 41)
     cofactor(14) = -det(14) 
     
     determinant(M) = M[1][1] * cofactor(11) + M[1][2] * cofactor(12) + M[1][3] * cofactor(13) + M[1][4] * cofactor(14) */
    
    /* Determinent of 3X3 matrix (M)
     A B C
     D E F
     G H I
     
     Fidn the cofactor of A
     A * *
     * E F
     * H I
     
     Find the determinant of the submatrix
     E F
     H I
     
     det(A) = E * I - F * H
     cofactor(A) = +det(A)
     
     Find the cofactor of B
     * B *
     D * F
     G * I
     
     Find the determinant of the submatrix
     D F
     G I
     
     det(B) = D * I - F * G
     cofactor(B) = -det(B)

     Find the cofactor of
     * * C
     D E *
     G H *
     
     // Find the determinant of the submatrix
     D E
     G H
     
     det(C) =  D * H - E * G
     cofactor(C) = +det(C)
     
     determinent(M) = cofactor(A) + cofactor(B) + cofactor(C)
     
     |M| = A(EI-FH) - B(DI-FG) + C(DH-EG) */
    
    /* Determinent of a 2x2 Matrix (M)
     A B
     C D
     
     Subtract the products of the matrices two diagonals
     |M| = A * D - B * C
     */
    
    /* Cofactor pattern
     + - + -
     - + - +
     + - + -
     - + - + 
     Cofactor = pattern * determinant(submatrix) */
    
    float cofactor_11 =         ((m._22 * ((m._33 * m._44) - (m._34 * m._43))) - (m._23 * ((m._32 * m._44) - (m._34 * m._42))) + (m._24 * ((m._32 * m._43) - (m._33 * m._42))));
    float cofactor_12 = -1.0f * ((m._21 * ((m._33 * m._44) - (m._34 * m._43))) - (m._23 * ((m._31 * m._44) - (m._34 * m._41))) + (m._24 * ((m._31 * m._43) - (m._33 * m._41))));
    float cofactor_13 =         ((m._21 * ((m._32 * m._44) - (m._34 * m._42))) - (m._22 * ((m._31 * m._44) - (m._34 * m._41))) + (m._24 * ((m._31 * m._42) - (m._32 * m._41))));
    float cofactor_14 = -1.0f * ((m._21 * ((m._32 * m._43) - (m._33 * m._42))) - (m._22 * ((m._31 * m._43) - (m._33 * m._41))) + (m._23 * ((m._31 * m._42) - (m._32 * m._41))));
    
    return m._11 * cofactor_11 + m._12 * cofactor_12 + m._13 * cofactor_13 + m._14 * cofactor_14;
}


ES1::Matrix ES1::Minor(const ES1::Matrix& m) {
    ES1::Matrix result;
    
    result._11 = ((m._22 * ((m._33 * m._44) - (m._34 * m._43))) - (m._23 * ((m._32 * m._44) - (m._34 * m._42))) + (m._24 * ((m._32 * m._43) - (m._33 * m._42))));
    result._12 = ((m._21 * ((m._33 * m._44) - (m._34 * m._43))) - (m._23 * ((m._31 * m._44) - (m._34 * m._41))) + (m._24 * ((m._31 * m._43) - (m._33 * m._41))));
    result._13 = ((m._21 * ((m._32 * m._44) - (m._34 * m._42))) - (m._22 * ((m._31 * m._44) - (m._34 * m._41))) + (m._24 * ((m._31 * m._42) - (m._32 * m._41))));
    result._14 = ((m._21 * ((m._32 * m._43) - (m._33 * m._42))) - (m._22 * ((m._31 * m._43) - (m._33 * m._41))) + (m._23 * ((m._31 * m._42) - (m._32 * m._41))));
    
    result._21 = ((m._12 * ((m._33 * m._44) - (m._34 * m._43))) - (m._13 * ((m._32 * m._44) - (m._34 * m._42))) + (m._14 * ((m._32 * m._43) - (m._33 * m._42))));
    result._22 = ((m._11 * ((m._33 * m._44) - (m._34 * m._43))) - (m._13 * ((m._31 * m._44) - (m._34 * m._41))) + (m._14 * ((m._31 * m._43) - (m._33 * m._41))));
    result._23 = ((m._11 * ((m._32 * m._44) - (m._34 * m._42))) - (m._12 * ((m._31 * m._44) - (m._34 * m._41))) + (m._14 * ((m._31 * m._42) - (m._32 * m._41))));
    result._24 = ((m._11 * ((m._32 * m._43) - (m._33 * m._42))) - (m._12 * ((m._31 * m._43) - (m._33 * m._41))) + (m._13 * ((m._31 * m._42) - (m._32 * m._41))));
    
    result._31 = ((m._12 * ((m._23 * m._44) - (m._24 * m._43))) - (m._13 * ((m._22 * m._44) - (m._24 * m._42))) + (m._14 * ((m._22 * m._43) - (m._23 * m._42))));
    result._32 = ((m._11 * ((m._23 * m._44) - (m._24 * m._43))) - (m._13 * ((m._21 * m._44) - (m._24 * m._41))) + (m._14 * ((m._21 * m._43) - (m._23 * m._41))));
    result._33 = ((m._11 * ((m._22 * m._44) - (m._24 * m._42))) - (m._12 * ((m._21 * m._44) - (m._24 * m._41))) + (m._14 * ((m._21 * m._42) - (m._22 * m._41))));
    result._34 = ((m._11 * ((m._22 * m._43) - (m._23 * m._42))) - (m._12 * ((m._21 * m._43) - (m._23 * m._41))) + (m._13 * ((m._21 * m._42) - (m._22 * m._41))));
    
    result._41 = ((m._12 * ((m._23 * m._34) - (m._24 * m._33))) - (m._13 * ((m._22 * m._34) - (m._24 * m._32))) + (m._14 * ((m._22 * m._33) - (m._23 * m._32))));
    result._42 = ((m._11 * ((m._23 * m._34) - (m._24 * m._33))) - (m._13 * ((m._21 * m._34) - (m._24 * m._31))) + (m._14 * ((m._21 * m._33) - (m._23 * m._31))));
    result._43 = ((m._11 * ((m._22 * m._34) - (m._24 * m._32))) - (m._12 * ((m._21 * m._34) - (m._24 * m._31))) + (m._14 * ((m._21 * m._32) - (m._22 * m._31))));
    result._44 = ((m._11 * ((m._22 * m._33) - (m._23 * m._32))) - (m._12 * ((m._21 * m._33) - (m._23 * m._31))) + (m._13 * ((m._21 * m._32) - (m._22 * m._31))));
    
    return result;
}

ES1::Matrix ES1::Cofactor(const ES1::Matrix& m) {
    ES1::Matrix r = Minor(m);
    
    r._11 *=  1.0f; r._12 *= -1.0f; r._13 *=  1.0f; r._14 *= -1.0f;
    r._21 *= -1.0f; r._22 *=  1.0f; r._23 *= -1.0f; r._24 *=  1.0f;
    r._31 *=  1.0f; r._32 *= -1.0f; r._33 *=  1.0f; r._34 *= -1.0f;
    r._41 *= -1.0f; r._42 *=  1.0f; r._43 *= -1.0f; r._44 *=  1.0f;
    
    return r;
}

ES1::Matrix ES1::Adjugate(const ES1::Matrix& m) {
    return Transpose(Cofactor(m));
}

ES1::Matrix ES1::Inverse(const ES1::Matrix& m) {
    float det = Determinant(m);
    if (det == 0.0f) {
        return m;
    }
    return Adjugate(m) * (1.0f/det);
}

// Transform matrix operations
ES1::Matrix ES1::Translate(float x, float y, float z) {
    ES1::Matrix result;
    
    result[3][0] = x;
    result[3][1] = y;
    result[3][2] = z;
    
    return result;
}

ES1::Matrix ES1::Translate(const ES1::Vector& v) {
    ES1::Matrix result;
    
    result[3][0] = v.x;
    result[3][1] = v.y;
    result[3][2] = v.z;
    
    return result;
}

ES1::Matrix ES1::Scale(float uniform) {
    ES1::Matrix result;
    
    result[0][0] = uniform;
    result[1][1] = uniform;
    result[2][2] = uniform;
    
    return result;
}

ES1::Matrix ES1::Scale(float x, float y, float z) {
    ES1::Matrix result;
    
    result[0][0] = x;
    result[1][1] = y;
    result[2][2] = z;
    
    return result;
}

ES1::Matrix ES1::Scale(const ES1::Vector& v) {
    ES1::Matrix result;
    
    result[0][0] = v.x;
    result[1][1] = v.y;
    result[2][2] = v.z;
    
    return result;
}

ES1::Matrix ES1::RotationX(float euler) {
    ES1::Matrix result;
    
    result[1][1] = CosEuler(euler);
    result[1][2] = SinEuler(euler);
    result[2][1] = -SinEuler(euler);
    result[2][2] = CosEuler(euler);
    
    return result;
}

ES1::Matrix ES1::RotationY(float euler) {
    ES1::Matrix result;
    
    result[0][0] = CosEuler(euler);
    result[0][2] = -SinEuler(euler);
    result[2][0] = SinEuler(euler);
    result[2][2] = CosEuler(euler);
    
    return result;
}

ES1::Matrix ES1::RotationZ(float euler) {
    ES1::Matrix result;
    
    result[0][0] = CosEuler(euler);
    result[0][1] = SinEuler(euler);
    result[1][0] = -SinEuler(euler);
    result[1][1] = CosEuler(euler);
    
    return result;
}

ES1::Matrix ES1::RotationX_LeftHanded(float euler) {
    ES1::Matrix result;
    
    result[1][1] = CosEuler(euler);
    result[1][2] = -SinEuler(euler);
    result[2][1] = SinEuler(euler);
    result[2][2] = CosEuler(euler);
    
    return result;
    
}

ES1::Matrix ES1::RotationY_LeftHanded(float euler) {
    ES1::Matrix result;
    
    result[0][0] = CosEuler(euler);
    result[0][2] = SinEuler(euler);
    result[2][0] = -SinEuler(euler);
    result[2][2] = CosEuler(euler);
    
    return result;
}

ES1::Matrix ES1::RotationZ_LeftHanded(float euler) {
    ES1::Matrix result;
    
    result[0][0] = CosEuler(euler);
    result[0][1] = -SinEuler(euler);
    result[1][0] = SinEuler(euler);
    result[1][1] = CosEuler(euler);
    
    return result;
}

ES1::Matrix ES1::Rotation(float eulerAngle, const ES1::Vector& axis) {
    float c = CosEuler(eulerAngle);
    float s = SinEuler(eulerAngle);
    float t = 1.0f - CosEuler(eulerAngle);
    float x2 = axis.x * axis.x;
    float y2 = axis.y * axis.y;
    float z2 = axis.z * axis.z;
    
    ES1::Matrix result;
    
    result[0][0] = t * x2 + c;
    result[1][0] = t * axis.x * axis.y - s * axis.z;
    result[2][0] = t * axis.x * axis.z + s * axis.y;
    
    result[0][1] = t * axis.x * axis.y + s * axis.z;
    result[1][1] = t * y2 + c;
    result[2][1] = t * axis.y * axis.z - s * axis.x;
    
    result[0][2] = t * axis.x * axis.z - s * axis.y;
    result[1][2] = t * axis.y * axis.z + s * axis.x;
    result[2][2] = t * z2 + c;
    
    return result;
}

ES1::Matrix ES1::Rotation_LeftHanded(float eulerAngle, const ES1::Vector& axis) {
    float c = CosEuler(eulerAngle);
    float s = SinEuler(eulerAngle);
    float t = 1.0f - CosEuler(eulerAngle);
    float x2 = axis.x * axis.x;
    float y2 = axis.y * axis.y;
    float z2 = axis.z * axis.z;
    
    ES1::Matrix result;
    
    result[0][0] = t * x2 + c;
    result[1][0] = t * axis.x * axis.y + s * axis.z;
    result[2][0] = t * axis.x * axis.z - s * axis.y;
    
    result[0][1] = t * axis.x * axis.y - s * axis.z;
    result[1][1] = t * y2 + c;
    result[2][1] = t * axis.y * axis.z + s * axis.x;
    
    result[0][2] = t * axis.x * axis.z + s * axis.y;
    result[1][2] = t * axis.y * axis.z - s * axis.x;
    result[2][2] = t * z2 + c;
    
    return result;
}

// from OpenGL spec PDF:                                 can be rewritten as:
// / 2/(r-l)     0        0       -(r+l)/(r-l)  \        / 2/(r-l)     0        0      (r+l)/(l-r)  \
// |   0      2/(t-b)     0       -(t+b)/(t-b)  |   =>   |   0      2/(t-b)     0      (t+b)/(b-t)  |
// |   0         0     -2/(f-n)   -(f+n)/(f-n)  |        |   0         0     2/(n-f)   (f+n)/(n-f)  |
// \   0         0        0             1       /        \   0         0        0           1       /
// invalid for: l=r, b=t, or n=f
ES1::Matrix ES1::Ortho(float left, float right, float bottom, float top, float znear, float zfar) {
    float p_fn = zfar + znear;
    float m_nf = znear - zfar; // ~ -m_fn
    
    float p_rl = right + left;
    float m_rl = right - left;
    float p_tb = top + bottom;
    float m_tb = top - bottom;
    
    float m_lr = -m_rl;
    float m_bt = -m_tb;
    
    return Matrix(
                      2.0f/m_rl,0.0f,       0.0f,       p_rl/m_lr,
                      0.0f,     2.0f/m_tb,  0.0f,       p_tb/m_bt,
                      0.0f,     0.0f,       2.0f/m_nf,  p_fn/m_nf,
                      0.0f,     0.0f,       0.0f,       1.0f
    );
}

ES1::Matrix ES1::Ortho(float left, float right, float bottom, float top) {
    return Ortho(left, right, bottom, top, -1.0f, 1.0f);
}

ES1::Matrix ES1::Ortho() {
    return Ortho(-1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f);
}

ES1::Matrix ES1::Perspective(float fov, float aspectRatio, float znear, float zfar) {
    float ymax = znear * TanRadian(fov * PI / 360.0f);
    float xmax = ymax * aspectRatio;
    
    return ES1::Frustum(-xmax, xmax, -ymax, ymax, znear, zfar);
}

// from OpenGL spec PDF:                                     can be rewritten as:
// /  2n/(r-l)    0        (r+l)/(r-l)       0      \        /  2n/(r-l)    0        (r+l)/(r-l)       0      \
// |     0     2n/(t-b)    (t+b)/(t-b)       0      |   =>   |     0     2n/(t-b)    (t+b)/(t-b)       0      |
// |     0        0       -(f+n)/(f-n)  -2fn/(f-n)  |        |     0        0        (f+n)/(n-f)   2fn/(n-f)  |
// \     0        0            -1            0      /        \     0        0            -1            0      /
// invalid for: n<=0, f<=0, l=r, b=t, or n=f
ES1::Matrix ES1::Frustum(float left, float right, float bottom, float top, float znear, float zfar) {
    float x_2n = znear + znear;
    float x_2nf = 2.0f * znear * zfar;
    
    float p_fn = zfar + znear;
    float m_nf = znear - zfar; // ~ -m_fn
    
    float p_rl = right + left;
    float m_rl = right - left;
    float p_tb = top + bottom;
    float m_tb = top - bottom;
    
    return Matrix(
                      x_2n/m_rl,    0.0f,       p_rl/m_rl,  0.0f,
                      0.0f,         x_2n/m_tb,  p_tb/m_tb,  0.0f,
                      0.0f,         0.0f,       p_fn/m_nf,  x_2nf/m_nf,
                      0.0f,         0.0f,       -1.0f,      0.0f
    );
}

// Copied from GLM
ES1::Matrix ES1::LookAt(const ES1::Vector& eye, const ES1::Vector& center, const ES1::Vector& up) {
    Vector f = Normalize(center - eye);
    Vector s = Normalize(Cross(f, up));
    Vector u = Cross(s, f);
    
    Matrix Result;
    Result[0][0] = s.x;
    Result[1][0] = s.y;
    Result[2][0] = s.z;
    
    Result[0][1] = u.x;
    Result[1][1] = u.y;
    Result[2][1] = u.z;
    
    Result[0][2] =-f.x;
    Result[1][2] =-f.y;
    Result[2][2] =-f.z;
    
    Result[3][0] =-Dot(s, eye);
    Result[3][1] =-Dot(u, eye);
    Result[3][2] = Dot(f, eye);
    
    return Result;
}

ES1::Matrix ES1::LookTowards(const ES1::Vector& eye, const ES1::Vector& forward, const ES1::Vector& up) {
    Vector f = Normalize(forward);
    Vector s = Normalize(Cross(f, up));
    Vector u = Cross(s, f);
    
    Matrix Result;
    Result[0][0] = s.x;
    Result[1][0] = s.y;
    Result[2][0] = s.z;
    Result[0][1] = u.x;
    Result[1][1] = u.y;
    Result[2][1] = u.z;
    Result[0][2] =-f.x;
    Result[1][2] =-f.y;
    Result[2][2] =-f.z;
    Result[3][0] =-Dot(s, eye);
    Result[3][1] =-Dot(u, eye);
    Result[3][2] = Dot(f, eye);
    return Result;
}

ES1::Matrix ES1::Normalize(const ES1::Matrix& mat) {
    ES1::Matrix& m = const_cast<ES1::Matrix&>(mat);
    
    float col1[4] = {0.0f};
    float col2[4] = {0.0f};
    float col3[4] = {0.0f};
    float col4[4] = {0.0f};
    
    m.GetColumn(0, col1);
    m.GetColumn(1, col2);
    m.GetColumn(2, col3);
    m.GetColumn(3, col4);
    
    Normalize4fv(col1);
    Normalize4fv(col2);
    Normalize4fv(col3);
    
    Cross4fv(col1, col2, col3);
    Cross4fv(col2, col3, col1);
    
    Normalize4fv(col1);
    Normalize4fv(col2);
    Normalize4fv(col3);

    return Matrix(col1[0], col2[0], col3[0], col4[0],
                  col1[1], col2[1], col3[1], col4[1],
                  col1[2], col2[2], col3[2], col4[2],
                  col1[3], col2[3], col3[3], col4[3]);
}


ES1::Matrix ES1::LookTowards(const ES1::Vector& position, const ES1::Vector& forward) {
    return LookTowards(position, forward, Vector(0.0f, 1.0f, 0.0f));
}

ES1::Matrix ES1::LookAt(const ES1::Vector& position, const ES1::Vector& target) {
    return LookAt(position, target, Vector(0.0f, 1.0f, 0.0f));
}

ES1::Matrix ES1::TRS(const ES1::Vector& translation, const ES1::Vector& eulerRotation, const ES1::Vector& scale) {
    Matrix translate = ES1::Translate(translation);
    Matrix rotate = RotationY(eulerRotation.y) * RotationX(eulerRotation.x) * RotationZ(eulerRotation.z);
    Matrix scaling = ES1::Scale(scale);
    
    // Scale first, then rotate then translate
    return translate * rotate * scaling;
}

ES1::Matrix ES1::TRS(const ES1::Vector& translation, const ES1::Vector& eulerRotation, float uniformScale) {
    Matrix translate = ES1::Translate(translation);
    Matrix rotate = RotationY(eulerRotation.y) * RotationX(eulerRotation.x) * RotationZ(eulerRotation.z);
    Matrix scaling = ES1::Scale(uniformScale);
    
    // Scale first, then rotate then translate
    return translate * rotate * scaling;
}

ES1::Matrix ES1::TRS(const ES1::Vector& transform, const ES1::Vector& eulerRotation) {
    Matrix translate = ES1::Translate(transform);
    Matrix rotate = RotationY(eulerRotation.y) * RotationX(eulerRotation.x) * RotationZ(eulerRotation.z);
    
    // Scale first, then rotate then translate
    return translate * rotate;
}
