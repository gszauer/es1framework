#include <ES1/Framework/Mouse.h>

ES1::Mouse::Mouse() {
    for (int i = 0; i < 4; ++i) {
        m_pButtonState[i] = false;
        m_pLastButtonState[i] = false;
    }
    m_nPosition[0] = 0;
    m_nPosition[1] = 0;
    m_nScrollDirection = 0;
    
    m_bInWindiw = true;
    m_bLastInWindow = false;
}

ES1::Mouse::~Mouse() { }

void ES1::Mouse::SetPosition(int nX, int nY) {
    m_nPosition[0] = nX;
    m_nPosition[1] = nY;
}

int ES1::Mouse::GetX() {
    return m_nPosition[0];
}

int ES1::Mouse::GetY() {
    return m_nPosition[1];
}

int* ES1::Mouse::GetPosition() {
    return m_nPosition;
}

void ES1::Mouse::SetMouseButton(Mousedef eButton) {
    m_pButtonState[MouseIndex(eButton)] = true;
}

void ES1::Mouse::ClearMouseButton(Mousedef eButton) {
    m_pButtonState[MouseIndex(eButton)] = false;
}

void ES1::Mouse::SetScrollDirection(int direction) {
    m_nScrollDirection = direction;
}

int ES1::Mouse::GetScrollDirection() {
    return m_nScrollDirection;
}

bool ES1::Mouse::ButtonDown(Mousedef eButton) {
    return m_pButtonState[MouseIndex(eButton)];
}

bool ES1::Mouse::ButtonUp(Mousedef eButton) {
    return !m_pButtonState[MouseIndex(eButton)];
}

bool ES1::Mouse::MousePressed(Mousedef eButton) {
  int index = MouseIndex(eButton);
  return (!m_pLastButtonState[index]) && m_pButtonState[index];
}

bool ES1::Mouse::MouseReleased(Mousedef eButton) {
  int index = MouseIndex(eButton);
  return m_pLastButtonState[index] && (!m_pButtonState[index]);
}

void ES1::Mouse::Update() {
    for (int i = 0; i < 4; ++i) {
        m_pLastButtonState[i] = m_pButtonState[i];
    }
    m_nScrollDirection = 0;
    
    m_bLastInWindow = m_bInWindiw;
}

void ES1::Mouse::SetInWidow(bool inWindow) {
    m_bInWindiw = inWindow;
}

bool ES1::Mouse::InWindow() {
    return m_bInWindiw;
}

bool ES1::Mouse::LeftWindow() {
    return !m_bInWindiw && m_bLastInWindow;
}

bool ES1::Mouse::EnteredWindow() {
    return m_bInWindiw && !m_bLastInWindow;
}