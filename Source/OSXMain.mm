#include <ES1/Framework/ES1Framework.h>
#include <ES1/Framework/OpenGL.h>
#include <ES1/Framework/Texture.h>
#include <assert.h>

class EngineTest : public ES1::Application {
protected:
    ES1::Mesh* focusMesh;
    ES1::Mesh* planeMesh;
    ES1::Texture* diffuseTex;
    ES1::DirectionalLight dirLight1;
    ES1::DirectionalLight dirLight2;
    ES1::DirectionalLight dirLight3;
    ES1::PointLight pointLight1;
    ES1::PointLight pointLight2;
    ES1::PointLight pointLight3;
    
    ES1::Mesh* debugSphere;
    float rotationAngle = 0.0f;
public:
    EngineTest(const char* title) : ES1::Application(title) {}
    
    void OnInitialize(ES1::Renderer* renderer, int windowWidth, int windowHeight) {
        renderer->ConfigClearColor(true);
        renderer->SetClearColor(ES1::Background);
        
        renderer->ConfigClearDepth(true);
        renderer->SetClearDepth(1.0f);
        
        renderer->Perspective(60.0f, (float)windowWidth / (float)windowHeight, 0.01f, 1000.0f);
        
        //renderer->LookAt(ES1::Vector(8.0f, 12.0f, 8.0f), ES1::Vector(0.0f, 0.0f, 0.0f));
        //renderer->LookAt(ES1::Vector(2.0f, 3.0f, 2.0f), ES1::Vector(0.0f, 0.0f, 0.0f));
        renderer->LookAt(ES1::Vector(4.0f, 6.0f, 4.0f), ES1::Vector(0.0f, 0.0f, 0.0f));
        //renderer->LookAt(ES1::Vector(8.0f, 2.0f, 8.0f), ES1::Vector(0.0f, 0.0f, 0.0f));

        
        //focusMesh = ES1::Mesh::MakeCube();
        focusMesh = ES1::Mesh::MakeSphere(true, true);
        planeMesh = ES1::Mesh::MakePlane();
        //renderer->ConfigSmoothShading(false);
        diffuseTex = ES1::Texture::Load("/Users/funplus/Pictures/uv_checker.png");
        
        dirLight1.lightColor = ES1::Red;
        dirLight1.lightDirection = ES1::Normalize(ES1::Vector(1, 0, 0));
        
        dirLight2.lightColor = ES1::Blue;
        dirLight2.lightDirection = ES1::Normalize(ES1::Vector(0, 0, 1));
        
        dirLight3.lightColor = ES1::Green;
        dirLight3.lightDirection = ES1::Normalize(ES1::Vector(0, 1, 0));
        
        debugSphere = ES1::Mesh::MakeSphere(false, false);
        
        pointLight1.lightColor = ES1::Red;
        pointLight1.lightRadius = 2.0f;
        pointLight1.lightPosition = ES1::Vector(/*-1.0f, -0.75f, -1.0f*/0.0f, 0.5f, 0.0f);
        
        pointLight2.lightColor = ES1::Blue;
        pointLight2.lightRadius = 2.0f;
        pointLight2.lightPosition = ES1::Vector(0.0f, 0.0f, 2.0f);

        pointLight3.lightColor = ES1::Green;
        pointLight3.lightRadius = 3.0f;
        pointLight3.lightPosition = ES1::Vector(0.0f, 3.0f, 0.0f);
    }
    
    void OnShutdown() {
        delete focusMesh;
        delete diffuseTex;
        delete planeMesh;
        
        delete debugSphere;
    }
    
    void OnUpdate(float deltaTime) {
        /*rotationAngle += 90.0f * deltaTime;
        while (rotationAngle > 360.0f) {
            rotationAngle -= 360.0f;
        }*/
    }
    
    void OnRender(ES1::Renderer* renderer) {
//        glDisable(GL_LIGHTING);
        glPushMatrix();
        glRotated(rotationAngle, 1.0f, 0.0f, 0.0f);
        
        renderer->Clear();
        ES1::Matrix identity;
        //float zero[] = {0.0f, 0.0f, 0.0f};
        
        /*renderer->PushLight(&dirLight3);
        renderer->PushLight(&dirLight1);
        renderer->PushLight(&dirLight2);
        
        renderer->Line(zero, (dirLight1.lightDirection * 2.0f).xyz, dirLight1.lightColor, identity);
        renderer->Line(zero, (dirLight2.lightDirection * 2.0f).xyz, dirLight2.lightColor, identity);
        renderer->Line(zero, (dirLight3.lightDirection * 2.0f).xyz, dirLight3.lightColor, identity);*/
        
        //renderer->PushLight(&dirLight1);
        
        renderer->PushLight(&pointLight1);
        //renderer->PushLight(&pointLight2);
        //renderer->PushLight(&pointLight3);
        
        ES1::Material debugMat;
        debugMat.lit = false;
        debugMat.wireframe = true;
        
        ES1::Matrix debugTrans = ES1::Translate(pointLight1.lightPosition) * ES1::Scale(pointLight1.lightRadius);
        debugMat.SetColor(ES1::Red);
        renderer->Render(*debugSphere, debugMat, debugTrans);
        
        /*debugTrans = ES1::Translate(pointLight2.lightPosition) * ES1::Scale(pointLight2.lightRadius);
        debugMat.SetColor(ES1::Blue);
        renderer->Render(*debugSphere, debugMat, debugTrans);
        
        debugTrans = ES1::Translate(pointLight3.lightPosition) * ES1::Scale(pointLight3.lightRadius);
        debugMat.SetColor(ES1::Green);
        renderer->Render(*debugSphere, debugMat, debugTrans);*/

        ES1::Material mat;
        mat.lit = true;
        //mat.wireframe = true;
        //mat.SetColor(ES1::White);
        //mat.SetHighlight(ES1::White, 1.0f);
        mat.diffuseTexture = diffuseTex;
        
        ES1::Matrix rix = ES1::Translate(-1.5f, 0.0f, -1.5f) * ES1::Scale(5.0f, 5.0f, 5.0f);
        //renderer->Render(*focusMesh, mat, rix);
        
        //mat.wireframe = true;
        //mat.lit = false;
        rix = ES1::Translate(0.0f, 0.0f, 0.0f) * ES1::Scale(5.0f, 1.0f, 5.0f);
        renderer->Render(*planeMesh, mat, rix);
        
        glDisable(GL_LIGHTING);
        glBegin(GL_LINES);
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex3fv(pointLight1.lightPosition.xyz);
        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glEnd();
        glEnable(GL_LIGHTING);
        
        //glEnable(GL_LINE);
        glPopMatrix();
        //GL_ASSERT_NO_ERROR();
    }
};

static EngineTest debugApp("Test App");

int main(int argc, const char** argv) {
    ES1::Application* pApplicaiton = ES1::Application::GetInstance();
    
    ES1::StdLogger debugLogger, warningLogger, errorLogger;
    pApplicaiton->SetLoggers(&debugLogger, &warningLogger, &errorLogger);
    
    ES1::Platform* pPlatform = new ES1::Platform(pApplicaiton);
    int result = pPlatform->MessagePump(argc, argv);
    delete pPlatform;
    
    return result;
}