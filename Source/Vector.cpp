#include <ES1/Framework/Vector.h>
#include <ES1/Framework/Math.h>

/* Notes
 Cross product of a 4D vector is not defined!
    http://stackoverflow.com/questions/23305159/vector4-crossproduct
 Useful link on definitions of vector properties
 4 dimensional vectors take w into length consideration
    https://www.khanacademy.org/math/linear-algebra/vectors_and_spaces/dot_cross_products/v/vector-dot-product-and-vector-length
*/

ES1::Vector::Vector() :
    x(0.0f), y(0.0f), z(0.0f) { }

ES1::Vector::Vector(float _x, float _y, float _z) :
    x(_x), y(_y), z(_z) { }

ES1::Vector::Vector(float* v) :
    x(v[0]), y(v[1]), z(v[2]) { }

ES1::Vector::Vector(const ES1::Vector& v) :
    x(v.x), y(v.y), z(v.z) { }

ES1::Vector& ES1::Vector::operator=(const ES1::Vector& v) {
    x = v.x; y = v.y; z = v.z;
    return *this;
}

float ES1::Vector::operator [](int i) {
    return xyz[i];
}

float ES1::Dot4fv(float* v1, float* v2) {
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2] + v1[3] * v2[3];
}

void ES1::Normalize4fv(float* v) {
    float dot = Dot4fv(v, v);
    if (dot == 0.0f) {
        return;
    }
    dot = Sqrt(dot);
    v[0] /= dot;
    v[1] /= dot;
    v[2] /= dot;
    v[3] /= dot;
}

void ES1::Cross4fv(float* result, float* v1, float* v2) {
    float temp[4] = {v1[0], v1[1], v1[2], v1[3]};
    if (result == v1) {
        v1 = temp;
    }
    
    result[0] = v1[1] * v2[2] - v1[2] * v2[1];
    result[1] = v1[2] * v2[0] - v1[0] * v2[2];
    result[2] = v1[0] * v2[1] - v1[1] * v2[0];
    result[3] = 0.0f;
}

float ES1::Dot3fv(float* v1, float* v2) {
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

void ES1::Normalize3fv(float* v) {
    float dot = Dot3fv(v, v);
    if (dot == 0.0f) {
        return;
    }
    dot = Sqrt(dot);
    v[0] /= dot;
    v[1] /= dot;
    v[2] /= dot;
}

void ES1::Cross3fv(float* result, float* v1, float* v2) {
    float temp[3] = {v1[0], v1[1], v1[2]};
    if (result == v1) {
        v1 = temp;
    }
    
    result[0] = v1[1] * v2[2] - v1[2] * v2[1];
    result[1] = v1[2] * v2[0] - v1[0] * v2[2];
    result[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

ES1::Vector ES1::Cross(const ES1::Vector& v1, const ES1::Vector& v2) {
    ES1::Vector result;
    
    result.x = v1.y * v2.z - v1.z * v2.y;
    result.y = v1.z * v2.x - v1.x * v2.z;
    result.z = v1.x * v2.y - v1.y * v2.x;
    
    return result;
}

float ES1::Dot(const ES1::Vector& v1, const ES1::Vector& v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

float ES1::Distance(const ES1::Vector& v1, const ES1::Vector& v2) {
    return Length(v1 - v2);
}

float ES1::DistanceSq(const ES1::Vector& v1, const ES1::Vector& v2) {
    return LengthSq(v1 - v2);
}

float ES1::Length(const ES1::Vector& v) {
    float dot = Dot(v, v);
    if (dot == 0.0f) {
        return 0.0f;
    }
    return Sqrt(dot);
}

float ES1::LengthSq(const ES1::Vector& v) {
    return Dot(v, v);
}

ES1::Vector ES1::Normalize(const ES1::Vector& v) {
    float dot = Dot(v, v);
    if (dot == 0.0f) {
        return ES1::Vector();
    }
    dot = Sqrt(dot);
    return ES1::Vector(v.x / dot, v.y / dot, v.z / dot);
}

ES1::Vector ES1::Lerp(const ES1::Vector& v1, const ES1::Vector& v2, float delta) {
    return v1 + (v2 - v1) * delta;
}

bool ES1::operator==(const ES1::Vector& lhs, const ES1::Vector& rhs) {
    return Equals(lhs.x, rhs.x) && Equals(lhs.y, rhs.y) && Equals(lhs.z, rhs.z);
}

bool ES1::operator< (const ES1::Vector& lhs, const ES1::Vector& rhs) {
    return Length(lhs) < Length(rhs);
}

bool ES1::operator!=(const ES1::Vector& lhs, const ES1::Vector& rhs) {
    return !operator==(lhs,rhs);
}

bool ES1::operator> (const ES1::Vector& lhs, const ES1::Vector& rhs) {
    return  operator< (rhs,lhs);
}

bool ES1::operator<=(const ES1::Vector& lhs, const ES1::Vector& rhs) {
    return !operator> (lhs,rhs);
}

bool ES1::operator>=(const ES1::Vector& lhs, const ES1::Vector& rhs) {
    return !operator< (lhs,rhs);
}

ES1::Vector ES1::operator*(const ES1::Vector& v, float f) {
    return ES1::Vector(v.x * f, v.y * f, v.z * f);
}

ES1::Vector ES1::operator*(const ES1::Vector& v1, const ES1::Vector& v2) {
    return ES1::Vector(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

ES1::Vector ES1::operator+(const ES1::Vector& v1, const ES1::Vector& v2) {
    return ES1::Vector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

ES1::Vector ES1::operator-(const ES1::Vector& v1, const ES1::Vector& v2) {
    return ES1::Vector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

ES1::Vector ES1::operator-(const ES1::Vector& v) {
    return ES1::Vector(-v.x, -v.y, -v.z);
}

ES1::Vector& ES1::operator*=(ES1::Vector& v, float f) {
    v.x *= f;
    v.y *= f;
    v.z *= f;
    
    return v;
}

ES1::Vector& ES1::operator*=(ES1::Vector& v, const ES1::Vector& v2) {
    v.x = v.x * v2.x;
    v.y = v.y * v2.y;
    v.z = v.z * v2.z;
    
    return v;
}

ES1::Vector& ES1::operator+=(ES1::Vector& v, const ES1::Vector& v2) {
    v.x = v.x + v2.x;
    v.y = v.y + v2.y;
    v.z = v.z + v2.z;
    
    return v;
}
ES1::Vector& ES1::operator-=(ES1::Vector& v, const ES1::Vector& v2) {
    v.x = v.x - v2.x;
    v.y = v.y - v2.y;
    v.z = v.z - v2.z;
    
    return v;
}

ES1::Vector ES1::operator*(float f, const ES1::Vector& v) {
    return v * f;
}