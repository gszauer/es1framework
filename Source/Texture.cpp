#include <ES1/Framework/Texture.h>
#include <ES1/Framework/OpenGL.h>

#define _ES1_STB_IMAGE_IMPLEMENTATION_
#include <ES1/Framework/stb_image.h>

ES1::Texture::Texture() {
    
}

ES1::Texture::~Texture() {
    
}

ES1::Texture* ES1::Texture::Load(const char* path) {
    int bitdepth = 0;
    int width, height;
    
    unsigned char *data_raw = stbi_load(path, &width, &height, &bitdepth, 4);
    if (data_raw == 0) {
        return 0;
    }
    
    unsigned int handle;
    glGenTextures(1, &handle);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, handle);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)data_raw);
    stbi_image_free(data_raw);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    ES1::Texture* result = new ES1::Texture();
    
    result->m_nGLHandle = handle;
    result->m_nWidth = width;
    result->m_nHeight = height;
    result->m_nBitDepth = bitdepth;
    result->m_bLinearFilter = true;
    
    return result;
}

unsigned int ES1::Texture::GetGLHandle() {
    return m_nGLHandle;
}

int ES1::Texture::GetWidth() {
    return m_nWidth;
}

int ES1::Texture::GetHeight() {
    return m_nHeight;
}

bool ES1::Texture::IsUsingLinearFilter() {
    return m_bLinearFilter;
}

bool ES1::Texture::IsUsingNearestFilter() {
    return !m_bLinearFilter;
}

void ES1::Texture::UseLinearFilter() {
    if (!m_bLinearFilter) {
        glBindTexture(GL_TEXTURE_2D, m_nGLHandle);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void ES1::Texture::UseNearestFilter() {
    if (m_bLinearFilter) {
        glBindTexture(GL_TEXTURE_2D, m_nGLHandle);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void ES1::Texture::Unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
}

void ES1::Texture::Bind() {
    glBindTexture(GL_TEXTURE_2D, m_nGLHandle);
}