#include <ES1/Framework/FileLogger.h>
#include <iostream>

ES1::FileLogger::FileLogger() {
    pTarget = 0;
}

ES1::FileLogger::~FileLogger() {
    if (pTarget != 0) {
        fclose (pTarget);
        pTarget = 0;
    }
}

void ES1::FileLogger::SetFile(const char* szFilePath) {
    if (pTarget != 0) {
        fclose (pTarget);
        pTarget = 0;
    }
    
    pTarget = fopen (szFilePath , "w");
    
    if (pTarget == 0) {
        std::cout << "Error, could not open: " << szFilePath << " for writing\n";
    }
}

void ES1::FileLogger::Log(const char* szString, ...) {
    if (pTarget != 0) {
        va_list args;
        va_start (args, szString);
        vfprintf(pTarget, szString, args);
        va_end (args);
    }
    else {
        std::cout << "Error, logger target not set\n";
    }
}