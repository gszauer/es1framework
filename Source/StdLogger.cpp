#include <ES1/Framework/StdLogger.h>
#include <iostream>

ES1::StdLogger::StdLogger() { }

ES1::StdLogger::~StdLogger() { }

void ES1::StdLogger::Log(const char* szFormat, ...) {
    va_list args;
    va_start(args, szFormat);
    vprintf(szFormat, args);
    va_end(args);
}