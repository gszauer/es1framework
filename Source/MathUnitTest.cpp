#include "MathUnitTest.h"
#include <iostream>
#include <OpenGL/gl.h>
#include <assert.h>
#include "GLVisualizer.h"

using namespace ES1;
using namespace UnitTests;

#ifndef TEST_GL_VIEW
int main(int argc, const char * argv[])  {
#else
int main2(int argc, const char * argv[])  {
#endif
    std::cout.precision(9);
    
    MathUnits();
    VectorUnits();
    MatrixUnits();
    QuaternionUnits();
    
    std::cout << "Unit tests finished\n";
    
    return 0;
}

float deltaAccum = 0.0f;
void UnitTests::DoGLRender() {
    // Set legacy matrices
    Matrix projection = Perspective(60.0f, 800.0f / 600.0f, 0.03f, 1000.0f);
    /* Static camera
    Matrix view = LookAt(Vector(-5, 5, 5), Vector(), Vector(0,1,0)); */
    // Orbital camera
    deltaAccum += (1.0f / 30.0f) * 30.0f;
    Matrix view = Translate(0.0f, 1.0f, -5.0f);
    view = RotationY(deltaAccum) * view;
    Vector viewPoint = MultiplyPoint(view, Vector());
    view = LookAt(viewPoint, Vector(), Vector(0,1,0));
    
    //Matrix model = RotationX(90.0f);
    //Matrix model = Translate(2.0f, 0.0f, 0.0f);
    //Matrix model = Scale(0.5f);
    //Matrix model = Translate(2.0f, 0.0f, 0.0f) * RotationX(90.0f) * Scale(0.5f);
    
    //Matrix model = TRS(Vector(2.0f, 0.0f, 0.0f), Vector(90.0f, 0.0f, 0.0f), Vector(0.5f, 0.5f, 0.5f));
    Matrix model = TRS(Vector(2.0f, 0.0f, 0.0f), Vector(90.0f, 0.0f, 0.0f), 0.5f);
    //Matrix model = TRS(Vector(2.0f, 0.0f, 0.0f), Vector(90.0f, 0.0f, 0.0f));
    
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(projection.m);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf((view * model).m);
    
    GLDrawPyramid();
    
    
    glLoadMatrixf((view).m);
    glBegin(GL_LINES);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(1.0f, 0.0f, 0.0f);
    
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 1.0f, 0.0f);
    
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glEnd();
}
    
void UnitTests::VectorUnits() {
    std::cout << "\tRunning Vector unit tests\n";
    Vector v;
    if (v.x != 0 || v.y != 0 || v.z != 0) {
        std::cout << "Default constructor broke\n";
    }
    Vector v2(10, 20, 30);
    if (v2.x != 10 || v2.y != 20 || v2.z != 30) {
        std::cout << "float3 constructor broken\n";
    }
    Vector v3(v2);
    if (v3.x != 10 || v3.y != 20 || v3.z != 30) {
        std::cout << "copy constructor broken\n";
    }
    float val[] = {10, 20, 30};
    Vector v4(val);
    if (v4.x != 10 || v4.y != 20 || v4.z != 30) {
        std::cout << "float fv constructor broken\n";
    }
    v = v2;
    if (v.x != 10 || v.y != 20 || v.z != 30) {
        std::cout << "operator=  broken\n";
    }
    if (v[0] != 10 || v[1] != 20 || v[2] != 30) {
        std::cout << "operator[]  broken\n";
    }
    if (v != v2) {
        std::cout << "!= broken\n";
    }
    v.x = 100;
    if (v == v2) {
        std::cout << "== broken";
    }
    
    Vector right(1.0f, 0.0f, 0.0f);
    Vector up(0.0f, 1.0f, 0.0f);
    Vector forward(0.0f, 0.0f, 1.0f);
    if (Cross(right, up) != forward) {
        std::cout << "Cross is broken\n";
    }
    
    if (Length(right) != 1.0f) {
        std::cout << "Length is broken";
    }
    v = Vector(40, 50, 60);
    if (Dot(v, v2) != 3200) {
        std::cout << "Dot product is broken\n";
    }
    if (!Equals(Distance(v, v2), 51.961524227)) {
        std::cout << "Distance is broken\n";
    }
    if (!Equals(DistanceSq(v, v2), 51.961524227 * 51.961524227)) {
        std::cout << "DistanceSQ is broken\n";
    }
    if (!Equals(Length(v), 87.7496414)) {
        std::cout << "Length is broken: " << Length(v) << "\n";
    }
    if (!Equals(LengthSq(v), 7700)) {
        std::cout << "LengthSQ is broken: " << LengthSq(v) << "\n";
    }
    v = Normalize(v);
    if (Length(v) != 1.0f) {
        std::cout << "Normalize is broken\n";
    }
    v = Vector();
    v2 = Vector(1,1,1);
    v3 = Lerp(v, v2, 0.5f);
    v4 = Vector(0.5f, 0.5f, 0.5f);
    if (v3 != v4) {
        std::cout << "Lerp is broken\n";
    }
    
    if (v2 < v) {
        std::cout << "L broken\n";
    }
    if (!(v3 <= v4)) {
        std::cout << "Lt broken\n";
    }
    if (v > v2) {
        std::cout << "G broken\n";
    }
    if (!(v3 <= v4)) {
        std::cout << "Gt is broken\n";
    }

}
    
void UnitTests::QuaternionUnits() {
    std::cout << "\tRunning Quaternion unit tests\n";
    
    Quaternion quat;
    Quaternion expected = Quaternion(1.0f, 0.0f, 0.0f, 0.0f);
    if (quat != expected) {
        std::cout << "Constructor, or Constructor4f is broken\n";
    }
    
    quat = Quaternion({ 1.0f, 0.0f, 0.0f, 0.0f});
    if (quat != expected) {
        std::cout << "Constructor4fv, or assignment operator is broken\n";
    }
    
    quat = Quaternion(Vector(10.0f, 20.0f, 30.0f));
    expected = Normalize(Quaternion(0.951549053, 0.127678588, 0.144877553, 0.239297241));
    if (quat != expected) {
        std::cout << "Euler constructor or normalize is broken\n";
        PrintQuaternion("quat: ", quat);
        PrintQuaternion("expected: ", expected);
    }
    
    if (!Equals(quat[0], 0.951549053f) || !Equals(quat[1], 0.127678588f) || !Equals(quat[2],  0.144877553f) || !Equals(quat[3], 0.239297241f)) {
        std::cout << "Subscript operator is broken!\n";
    }
    
    if (Magnitude(quat) != 1.0f) {
        std::cout << "Quat is not normalized\n";
    }
    
    Matrix xRot = RotationX(30.0f);
    quat = EulerAngles(30.0f, 0.0f, 0.0f);
    if (AsMatrix(quat) != xRot) {
        std::cout << "Quat EulerAngles, or AsMatrix is broken\n";
        PrintMatrix("Quat: ", AsMatrix(quat));
        PrintMatrix("XRot: ", xRot);
    }
    quat = Quaternion(Vector(30.0f, 0.0f, 0.0f));
    if (AsMatrix(quat) != xRot) {
        std::cout << "Quat Constructor EulerAngles, or AsMatrix is broken\n";
        PrintMatrix("Quat: ", AsMatrix(quat));
        PrintMatrix("XRot: ", xRot);
    }
    
    Matrix yRot = RotationY(30.0f);
    quat = EulerAngles(Vector(0.0f, 30.0f, 0.0f));
    if (AsMatrix(quat) != yRot) {
        std::cout << "EulerAngles (Vector) is broken\n";
    }
    
    Vector asEuler = ToEulerAngles(quat);
    float yDiff = 30.0f - asEuler.y;
    if (yDiff > 0.1f) {
        std::cout << "To Euler Angles is broken\n";
        PrintVector("asEuler: ", asEuler);
    }
    
    Matrix zRot = RotationZ(20.0f);
    quat = AngleAxis(20.0f, Vector(0.0f, 0.0f, 1.0f));
    if (AsMatrix(quat) != zRot) {
        std::cout << "Angle Axis rotation is broken\n";
    }
    
    float angle = 0.0f;
    Vector axis;
    ToAngleAxis(quat, &angle, &axis);
    if (20.0f - angle > 0.1f) {
        std::cout << "Angle axis (angle) is broken: " << angle << "\n";
    }
    if (axis != Vector(0.0f, 0.0f, 1.0f)) {
        std::cout << "Axis is broken\n";
        PrintVector("Axis: ", axis);
    }
    
    quat = Quaternion(Vector(53,0,0));
    Quaternion quat2 = AngleAxis(53, Vector(1.0f, 0.0f, 0.0f));
    if (quat != quat2) {
        std::cout << "Angle Axis and Eulers don't give same result\n";
    }
    xRot = RotationX(53);
    if (AsQuaternion(xRot) != quat) {
        std::cout << "As quaternion seems to be broken\n";
        PrintQuaternion("quat: ", quat);
        PrintQuaternion("rot: ", AsQuaternion(xRot));
    }
    
    
    quat = AngleAxis(90.0f, Vector(0.0f, 0.0f, 1.0f));
    Matrix mat = AsMatrix(quat);
    if (mat != RotationZ(90)) {
        std::cout << "Rotation not as expected\n";
    }
    
    if (Magnitude(quat) != 1.0f) {
        std::cout << "Quat magnitude not as expected\n";
    }
    
    quat.w = 700;
    quat2 = Clone(quat);
    if (quat != quat2) {
        std::cout << "Quaternion clone is broken\n";
    }
    
    if (Magnitude(quat) == 1.0f) {
        std::cout << "Magnitude is broken\n";
    }
    quat = Normalize(quat);
    if (!Equals(Magnitude(quat), 1.0f)) {
        std::cout << "Quat magnitude not as expected: " << Magnitude(quat) << "\n";
    }
    
    quat2 = Inverse(quat);
    
    if (quat2 == quat) {
        std::cout << "Inverse is broken\n";
    }
    
    quat2 = Inverse(quat2);
    if (quat2 != quat) {
        std::cout << "Still broken\n";
    }
    
    quat2.w = 5.0f;
    
    if (!Equals(Dot(quat2, quat2), Magnitude(quat2) * Magnitude(quat2))) {
        std::cout << "dot: " <<Dot(quat2, quat2) << "\n";
        std::cout << "mag: " <<Magnitude(quat2) << "\n";
        std::cout << "mag sq: " << (Magnitude(quat2)*Magnitude(quat2)) << "\n";
    }
    
    quat = Quaternion(10.0f, 0.0f, 0.0f);
    mat = RotationX(10.0f);
    Vector vec1 = MultiplyVector(mat, Vector(1.0f, 1.0f, 1.0f));
    Vector vec2 = quat * Vector(1.0f, 1.0f, 1.0f);
    if (vec1 != vec2) {
        std::cout << "Multiply by vector is broken\n";
    }
    
    
    Quaternion quat1 = AngleAxis(30.0f, Vector(0.0f, 1.0f, 0.0f));
    quat2 = AngleAxis(60.0f, Vector(1.0f, 0.0f, 0.0f));
    quat = quat1 * quat2;
    expected.w = 0.836518f;
    expected.x = 0.482961f;
    expected.y = 0.224143f;
    expected.z = -0.129408f;
    if (quat != expected) {
        std::cout << "Quaternion multiply is broken!\n";
    }
    Vector euler = ToEulerAngles(quat);
    if (60.0f - euler.x > 0.1f) {
        std::cout << "Quaternion ul (x) is broken!\n";
    }
    if (30.0f - euler.y > 0.1f) {
        std::cout << "Quaternion ul (y) is broken!\n";
    }
    
    quat = AngleAxis(30.0f, Vector(0.0f, 1.0f, 0.0f));
    quat *= AngleAxis(60.0f, Vector(1.0f, 0.0f, 0.0f));
    if (quat != expected) {
        std::cout << "Quaternion multiply is broken!\n";
    }
    euler = ToEulerAngles(quat);
    if (60.0f - euler.x > 0.1f) {
        std::cout << "Quaternion ul (x) is broken!\n";
    }
    if (30.0f - euler.y > 0.1f) {
        std::cout << "Quaternion ul (y) is broken!\n";
    }
    
    quat2 = quat;
    quat2 = quat2 * 2.0f;
    quat *= 2.0f;
    
    expected.w = 1.67304f;
    expected.x = 0.965922f;
    expected.y = 0.448286f;
    expected.z = -0.258816f;
    
    if (quat != expected || quat2 != expected) {
        std::cout << "Quat * Float seems to be broken\n";
    }
    
    quat1 = AngleAxis(30.0f, Vector(0.0f, 1.0f, 0.0f));
    quat2 = AngleAxis(60.0f, Vector(1.0f, 0.0f, 0.0f));
    quat = Slerp(quat1, quat2, 33.0f);
    
    expected.w = 0.956368f;
    expected.x = 0.258406f;
    expected.y = 0.13633f;
    expected.z = 0.0f;
    
    if (quat != expected) {
        std::cout << "Lerp is broken\n";
    }
    
    
    Vector v1(0.0f, 1.0f, 0.0f);
    Vector v2(1.0f, 0.0f, 0.0f);
    Quaternion q = FromTo(v1, v2);
    v1 = q * v1;
    if (v1 != v2) {
        std::cout << "From to is broke?\n";
    }
}
    
void UnitTests::MatrixUnits() {
    std::cout << "\tRunning Matrix unit tests\n";
    Matrix trans = Translate(10, 20, 30);
    float e1[] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        10, 20, 30, 1
    };
    for (int i = 0; i < 16; ++i) {
        if (!Equals(trans.m[i], e1[i])) {
            std::cout << "Translation is broken\n";
        }
    }
    
    float column3[4] = {0.0f};
    float row3[4] = {0.0f};
    
    trans.GetColumn(3, column3);
    trans.GetRow(3, row3);
    
    if (column3[0] != 10 || column3[1] != 20 || column3[2] != 30 || column3[3] != 1) {
        std::cout << "Wrong translation (or column getter)\n";
    }
    if (row3[0] != row3[1] || row3[1] != row3[2] || row3[2] != 0.0f || row3[3] != 1) {
        std::cout << "Wrong translation (or row getter)\n";
    }
    if (!Equals(trans[3][1], 20.0f)) {
        std::cout << "Wrong translation (or bracket getter)\n";
    }
    
    Matrix def;
    float e2[] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
    for (int i = 0; i < 16; ++i) {
        if (!Equals(def.m[i], e2[i])) {
            std::cout << "Default constructor is broken\n";
        }
    }
    
    def = Matrix(1, 5,  9, 13,
                 2, 6, 10, 14,
                 3, 7, 11, 15,
                 4, 8, 12, 16);
    float e3[] = {
        1, 2, 3, 4,
        5, 6, 7, 8,
        9, 10, 11, 12,
        13, 14, 15, 16
    };
    for (int i = 0; i < 16; ++i) {
        if (!Equals(def.m[i], e3[i])) {
            std::cout << "multi number constructor is broken\n";
        }
    }
    Matrix def2(e3);
    if (def2 != def) {
        std::cout << "float* constructor is broken\n";
    }
    
    Matrix m(trans);
    if (m != trans) {
        std::cout << "copy constructor is broken\n";
    }
    
    def = trans;
    if (def != m) {
        std::cout << "assignment operator is broken\n";
    }
    
    def = Scale(3.0f, 2.0f, 4.0f);
    float e4[] = {
        3, 0, 0, 0,
        0, 2, 0, 0,
        0, 0, 4, 0,
        0, 0, 0, 1
    };
    for (int i = 0; i < 16; ++i) {
        if (!Equals(def.m[i], e4[i])) {
            std::cout << "scale is broken\n";
        }
    }
    
    def = def * 2;
    float e5[] = {
        6, 0, 0, 0,
        0, 4, 0, 0,
        0, 0, 8, 0,
        0, 0, 0, 2
    };
    for (int i = 0; i < 16; ++i) {
        if (!Equals(def.m[i], e5[i])) {
            std::cout << "scalar multiply is broken\n";
        }
    }
    
    def = Translate(10, 20, 30) * RotationX(35.0f);
    float e6[4][4] = {  {1, 0, 0, 0},
                        {0, 0.81915f, 0.57357f, 0},
                        {0, -0.57357f, 0.81915f, 0},
                        {10, 20, 30, 1 }};
    bool issue = false;
    for (int r = 0; r<4; ++r) {
        for (int c = 0; c<4; ++c) {
            if (!Equals(def[c][r], e6[c][r])) {
                std::cout << "matrix multiply[" << c << "][" << r << "] is broken: " << def[c][r] << " / " << e6[c][r] << "\n";
                issue = true;
            }

        }
    }
    if (issue) {
        PrintMatrix("Multiply: ", def);
    }
    
    def = Translate(20, 30, 40);
    Vector v4(1.0f, 1.0f, 1.0f);
    Vector v = MultiplyPoint(def, v4);
    if (v[0] != 21 || v[1] != 31 ||  v[2] != 41) {
        std::cout << "Matrix vector multiply is broken\n";
        PrintVector(v);
    }
    
    if (def != def) {
        std::cout << "Not equal broken\n";
    }
    
    if (!(def != Translate(5, 5, 5))) {
        std::cout << "Not equal broken\n";
    }
    
    if (def == Scale(2, 2, 2)) {
        std::cout << "== broken\n";
    }
    
    def =Translate(10, 20, 30);
    def *=RotationX(35.0f);
    issue = false;
    for (int r = 0; r<4; ++r) {
        for (int c = 0; c<4; ++c) {
            if (!Equals(def[c][r], e6[c][r])) {
                std::cout << "matrix multiply[" << c << "][" << r << "] assign is broken: " << def[c][r] << " / " << e6[c][r] << "\n";
                issue = true;
            }
            
        }
    }
    if (issue) {
        PrintMatrix("Multiply assign: ", def);
    }
    
    def = Scale(3.0f, 2.0f, 4.0f);
    def *= 2;
    for (int i = 0; i < 16; ++i) {
        if (!Equals(def.m[i], e5[i])) {
            std::cout << "scalar multiply assignment is broken\n";
        }
    }
    
    def = Translate(10, 20, 30);
    def = Transpose(def);
    float e7[4][4] = {  {1, 0, 0, 10},
                        {0, 1, 0, 20},
                        {0, 0, 1, 30},
                        {0, 0, 0, 1 }};

    def2 = Matrix((float*)e7);
    if (def != def2) {
        std::cout << "Transpose is broken?\n";
    }
    
    def = Translate(10, 20, 30);
    float e8[4][4] = {  {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {10, 20, 30, 1 }};
    issue = false;
    for (int r = 0; r<4; ++r) {
        for (int c = 0; c<4; ++c) {
            if (!Equals(def[c][r], e8[c][r])) {
                std::cout << "matrix translate[" << c << "][" << r << "] is broken: " << def[c][r] << " / " << e6[c][r] << "\n";
                issue = true;
            }
            
        }
    }
    if (issue) {
        PrintMatrix("Translate broken: ", def);
    }
    
    def = Scale(10);
    def2 = Scale(Vector(10, 10, 10));
    if (def != def2) {
        std::cout << "Scale done broke\n";
    }
    if (def[0][0] != 10 || def[1][1] != 10 || def[2][2] != 10) {
        std::cout << "Scale definateley broke\n";
    }
    
    Matrix det(2,  5,  3, 5,
               4,  6,  6, 3,
               11, 3,  2, -2,
               4,  -7, 9, 3);
    if (!Equals(Determinant(det), 2960)) {
        std::cout << "Determinant is broken\n";
    }
    
    det = Matrix(3, 0, 2, -1,
                 1, 2, 0, -2,
                 4, 0, 6, -3,
                 5, 0, 2, 0);
    if (!Equals(Determinant(det), 20)) {
        std::cout << "Determinant is broken\n";
    }
    
    Matrix adj = Adjugate(det);
    if (adj != Matrix(12, 0, -4, 0,
                      -50, 10 ,10, 20,
                      -30, 0, 10, 10,
                      -44, 0, 8, 20)) {
        std::cout << "Adjugate is broken\n";
    }
    Matrix inv = Inverse(det);
    Matrix res = det * inv;
    if (res != Matrix()) {
        std::cout << "Inverse is broken\n";
    }
    
    
    Matrix rotX = RotationX(30.0f);
    Matrix expected = Matrix(1, 0, 0, 0,
                             0, 0.866027, -0.499998, 0,
                             0, 0.499998, 0.866027, 0,
                             0, 0, 0, 1);
    if (rotX != expected) {
        std::cout << "X rotation not what we expected\n";
    }
    Matrix rotXLh = RotationX_LeftHanded(30.0f);
    if (Transpose(rotX) != rotXLh) {
        std::cout << "X rotation (left handed) is not what we expected\n";
    }
    
    Matrix rotY = RotationY(-90.0f);
    expected = Matrix(8.42015e-06, 0, -1, 0,
                      0, 1, 0, 0,
                      1, 0, 8.42015e-06, 0,
                      0, 0, 0, 1);
    if (rotY != expected) {
        std::cout << "Rotation Y not what expected\n";
    }
    Matrix rotYLh = RotationY_LeftHanded(-90.0f);
    if (Transpose(rotY) != rotYLh) {
        std::cout << "Rotation Y (left handed) not what expected\n";
    }
    
    Matrix rotZ = RotationZ(798.0f);
    expected = Matrix(0.207984, -0.978132, 0, 0,
                      0.978132, 0.207984, 0, 0,
                      0, 0, 1, 0,
                      0, 0, 0, 1);
    if (rotZ != expected) {
        std::cout << "Rot Z not what we expected\n";
    }
    Matrix rotZLh = RotationZ_LeftHanded(798.0f);
    if (Transpose(rotZ) != rotZLh) {
        std::cout << "Rotation Z (left handed) not what we expected\n";
    }
    
    Matrix axisRotation = Rotation(15.0f, Normalize(Vector(15, 30, 45)));
    expected = Matrix(0.96836, -0.202648, 0.145645, 0,
                      0.212383, 0.975662, -0.0545689, 0,
                      -0.131042, 0.083775, 0.987831, 0,
                      0, 0, 0, 1);
    if (expected != axisRotation) {
        std::cout << "Axis rotation not what we expected\n";
    }
    Matrix axisLg = Rotation_LeftHanded(15.0f, Normalize(Vector(15, 30, 45)));
    if (Transpose(axisRotation) != axisLg) {
        std::cout << "Axis rotation (left handed) not what we expected\n";
    }
    
    Matrix simpleMul = Translate(10, 20, 30) * Scale(10, 20, 30);
    expected = Matrix(10, 0, 0, 10,
                      0, 20, 0, 20,
                      0, 0, 30, 30,
                      0, 0, 0, 1);
    if (simpleMul != expected) {
        std::cout << "Simple multiplication is broken\n";
    }

    Matrix complexMul =  Translate(10, 20, 30) * (RotationY(-90.0f) * RotationZ(798.0f)) * Scale(1, 2, 1);
    expected = Matrix(1.75126e-06, -1.6472e-05, -1, 10,
                      0.978132, 0.415968, 0, 20,
                      0.207984, -1.95626, 8.42015e-06, 30,
                      0, 0, 0, 1);
    if (complexMul != expected) {
        std::cout << "More complex multiplication is broken\n";
    }
    
    expected = Matrix(-1, 0, 0, 0,
                      0, 1, -0, 0,
                      0, 0, -0, 1,
                      0, 0, -1, 0);
    Matrix test = Frustum(-1, 1, 1, -1, -1, 1);
    if (test != expected) {
        std::cout << "Frustum (-1, 1, 1, -1, -1, 1); looks broken\n";
    }
    
    expected = Matrix(-3.33333, 0, 0.333333, 0,
                      0, 1.42857, 0.142857, 0,
                      0, 0, -0.0909091, 54.5454559,
                      0, 0, -1, 0);
    test = Frustum(-10, 20, 30, -40, -50, 60);
    if (test != expected) {
        std::cout << "Frustum (-10, 20, 30, -40, -50, 60); looks broken\n";
        PrintMatrix(test);
    }
    
    expected = Matrix(1, 0, 0, -0,
                  0, 1, 0, -0,
                  0, 0, -1, -0,
                  0, 0, 0, 1);
    test = Ortho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
    if (test != expected) {
        std::cout << "Ortho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f); looks broken\n";
    }
    
    expected = Matrix(1.29903829, 0, 0, 0,
                      0, 1.73205113, 0, 0,
                      0, 0, -1, -0.002,
                      0, 0, -1, 0);
    test = Perspective(60.0f, 800.0f / 600.0f, 0.001f, 1000.0f);
    if (test != expected) {
        std::cout << "Perspective @60 degrees is broken\n";
    }
    
    Vector vec3 = Vector(1.0f, 2.0f, 3.0f);
    Vector vec4 = Vector(1.0f, 2.0f, 3.0f);
    Matrix mat = RotationZ(30.0f) * Translate(10.0f, 20.0f, 30.0f);
    vec3 = MultiplyVector(mat, vec3);
    vec4 = MultiplyVector(mat, vec4);
    Vector vecExpected  = Vector(-0.133968, 2.23205, 3);
    if (vec3 != vecExpected) {
        std::cout << "Multiply Vector is not good\n";
    }
    if (vec3 != vec4) {
        std::cout << "Multiply vector is not the same as * opertor!\n";
    }
    
    vec3 = Vector(20.0f, 33.3f, 19.25f);
    vec4 = Vector(20.0f, 33.3f, 19.25f);
    mat = Translate(33.0f, 45.0f, 66.0f) * RotationY(57.0f);
    vec3 = MultiplyPoint(mat, vec3);
    vec4 = MultiplyPoint(mat, vec4);
    vecExpected = Vector(60.0372238, 78.3000031, 59.7110329);
    if (vec3 != vecExpected)  {
        std::cout << "Multoply point is broken\n";
    }
    if (vec3 != vec4) {
        std::cout << "vec3: " << vec3.x << ", " << vec3.y << ", " << vec3.z << "\n";
        std::cout << "vec4: " << vec4.z << ", " << vec4.y << ", " << vec4.z << "\n";
        std::cout << "Multiply point is not the same as the * operator\n";
    }
    
    Vector v1(10, 20, 30);
    Vector v2(10, 20, 30);
    mat = Translate(10, 20, 30);
    v1 = MultiplyVector(mat, v1);
    v2 = MultiplyPoint(mat, v2);
    if (v1 == v2) {
        std::cout << "MultiplyVector should not be the same as MultiplyPoint\n";
    }
    
    Vector v1_4(10, 20, 30);
    Vector v2_4(10, 20, 30);
    mat = Translate(10, 20, 30);
    v1_4 = MultiplyVector(mat, v1_4);
    v2_4 = MultiplyPoint(mat, v2_4);
    if (v1_4 == v2_4) {
        std::cout << "Multiplying two vector 4's this way shoudl yield different results";
    }
    if (v1_4 != v1) {
        std::cout << "error, multiplication stuff (v1)\n";
    }
    if (v2_4 != v2) {
        std::cout << "error, multiplication stuff (v2)\n";
    }
    
    expected = Matrix(-0.447214, 0, -0.894427, -0,
                      -0.596285, 0.745356, 0.298142, -5.96046e-08,
                      0.666667, 0.666667, -0.333333, -7.5,
                      0, 0, 0, 1);
    mat = LookAt(Vector(5.0f, 5.0f, -2.5f), Vector(), Vector(0.0f, 1.0f, 0.0f));
    if (mat != expected) {
        std::cout << "Look at not what was expected\n";
    }
    
    expected = Matrix(0.908308923, 0, -0.418300152, -6.07132816,
                      0, 1, 0, -0,
                      0.418300122, -0, 0.908308864, -10.7228241,
                      0, 0, 0, 1);
    mat = LookAt(Vector(10.0f, 0.0f, 7.2f), Vector(3.0f, 0.0f, -8.0f));
    if (expected != mat) {
        std::cout << "Look at with no up not what was expected\n";
    }
    
    mat = LookTowards(Vector(5.0f, 5.0f, -2.5f), Normalize(Vector(5.0f, 5.0f, -2.5f) * -1.0f), Vector(0.0f, 1.0f, 0.0f));
    expected = Matrix(-0.447214, 0, -0.894427, -0,
                      -0.596285, 0.745356, 0.298142, -5.96046e-08,
                      0.666667, 0.666667, -0.333333, -7.5,
                      0, 0, 0, 1);
    if (expected != mat) {
        std::cout << "Look towards is broken\n";
    }
    

    mat = LookAt(Vector(-2.0f, -7.0f, 6.0f), Vector(), Vector(0.0f, 1.0f, 0.0f));
    expected = Matrix(0.948683321, -0, 0.316227764, 1.1920929e-07,
                      -0.234640539, 0.670401514, 0.703921676, -4.76837158e-07,
                      -0.21199958, -0.741998553, 0.635998726, -9.43398094,
                      0, 0, 0, 1);
    if (mat != expected) {
        std::cout << "Broken\n";
    }
    
    mat = Normalize(mat);
    expected = Matrix(0.948683381, -1.49011594e-08, 0.316227764, 1.1920929e-07,
                      -0.234640539, 0.670401514, 0.703921676, -4.76837158e-07,
                      -0.211999565, -0.741998613, 0.635998726, -9.43398094,
                      0, 0, 0, 1);
    if (mat != expected) {
        std::cout << "Matrix Normalize is Broken\n";
    }
    
}

void UnitTests::MathUnits() {
    std::cout << "\tRunning mathlib unit tests\n";
    if (Equals(1.0f, 2.0f)) {
        std::cout << "1 does not equal 2\n";
    }
    if (Equals(1.0f, 1.1f)) {
        std::cout << "1 does not equal 1.1\n";
    }
    if (Equals(1.0f, 1.01f)) {
        std::cout << "1 does not equal 1.01\n";
    }
    if (Equals(1.0f, 1.001f)) {
        std::cout << "1 does not equal 1.001\n";
    }
    if (Equals(1.0f, 1.0001f)) {
        std::cout << "1 does not equal 1.0001\n";
    }
    if (Equals(1.0f, 1.00001f)) {
        std::cout << "1 does not equal 1.00001\n";
    }

    float lerpResult = Lerp(0.0f, 1.0f, 0.5f);
    if(lerpResult != 0.5f) {
        std::cout << "expected lerp result: 0.5f\n";
    }
    lerpResult = Lerp(0.0f, 2.0f, 0.5f);
    if(lerpResult != 1.0f) {
        std::cout << "expected lerp result: 1.0f\n";
    }
    
    float clampResult = Clamp(-5.0f, 0.0f, 2.0f);
    if (clampResult != 0.0f) {
        std::cout << "clamp result expected at 0.0f\n";
    }
    
    clampResult = Clamp(1.0f, 2.0f, 10.0f);
    if (clampResult != 2.0f) {
        std::cout << "clamp result expected at 2.0f\n";
    }
    
    clampResult = Clamp(5.0f, 0.0f, 10.0f);
    if (clampResult != 5.0f) {
        std::cout << "clamp result expected at 5.0f\n";
    }
    
    clampResult = Clamp01(-20.0f);
    if (clampResult != 0.0f) {
        std::cout << "clamp01 expeted at 0\n";
    }
    
    clampResult = Clamp01(33.3f);
    if (clampResult != 1.0f) {
        std::cout << "clamp01 expeted at 1\n";
    }
    
    clampResult = Clamp01(0.3f);
    if (clampResult != 0.3f) {
        std::cout << "clamp01 expeted at 0.3\n";
    }
    
    float absNum = Abs(10.0f);
    if (absNum != 10.0f) {
        std::cout << "abs expected at: 10.0\n";
    }

    absNum = Abs(-11.1f);
    if (absNum != 11.1f) {
        std::cout << "abs expected at: 11.1, result: " << absNum << "\n";
    }
    
    absNum = Abs(0.0f);
    if (absNum != 0.0f) {
        std::cout << "abs expected at: 0\n";
    }
    
    absNum = Abs(-0.0f);
    if (absNum != 0.0f) {
        std::cout << "abs expected at: 0\n";
    }

    float sinRes = SinRadian(35.0f);
    if (!Equals(-0.42818266949f, sinRes)) {
        std::cout << "Sin of 35 radians expected at: -0.42818266949f\n";
    }
    
    sinRes = SinRadian(0.32f);
    if (!Equals(0.31456656061, sinRes)) {
        std::cout << "Sin of 0.32 radians expected at: 0.31456656061f\n";
    }

    sinRes = SinEuler(35.0f);
    if (!Equals(0.57357643635f, sinRes)) {
        std::cout << "Sin of 35 degrees expected at: 0.57357643635, got: " << sinRes << "\n";
    }
    
    sinRes = SinEuler(0.32f);
    if (!Equals(0.00558502457f, sinRes)) {
        std::cout << "Sin of 0.32 degrees expected at: 0.00558502457f\n";
    }
    
    float cosRes = CosEuler(-42.0f);
    if (!Equals(0.74314482547f, cosRes)) {
        std::cout << "cos of -42 degrees expected at: 0.74314482547\n";
    }
    
    cosRes = CosEuler(0.26f);
    if (!Equals(0.99998970394, cosRes)) {
        std::cout << "cos of 0.26 degrees expected at: 0.99998970394\n";
    }
    
    cosRes = CosRadian(-42.0f);
    if (!Equals(-0.39998531498, cosRes)) {
        std::cout << "cos of -42 radians expected at: -0.39998531498\n";
    }
    
    cosRes = CosRadian(0.26f);
    if (!Equals(0.96638997813, cosRes)) {
        std::cout << "cos of 0.26 radians expected at: 0.96638997813\n";
    }
    
    float tanRes = TanEuler(30.0f);
    if (!Equals(tanRes, 0.57735026919f)) {
        std::cout << "tan of 30 degrees expected at: 0.57735026919f\n";
    }
    
    tanRes = TanEuler(-7.9f);
    if (!Equals(tanRes, -0.13876146683f)) {
        std::cout << "tan of -7.9 degrees expected at: -0.13876146683\n";
    }
    
    tanRes = TanRadian(30.0f);
    if (!Equals(tanRes, -6.40533119665f)) {
        std::cout << "tan of 30 radians expected at: -6.40533119665, got: " << tanRes << "\n";
    }
    
    tanRes = TanRadian(-7.9f);
    if (!Equals(tanRes, 21.71506690979)) {
        std::cout << "tan of -7.9 radians expected at: 21.71506690979, got: " << tanRes << "\n";
    }
    
    float powRes = Pow(10, 2);
    if (!Equals(powRes, 100)) {
        std::cout << "10 to the power of 2 should be 100!\n";
    }
    
    powRes = Pow(5, -7);
    if (!Equals(powRes, 0.0000128f)) {
        std::cout << "5^-7 expected at: 0.0000128 \n";
    }
    
    float sqrtRes = Sqrt(16);
    if (!Equals(sqrtRes, 4)) {
        std::cout << "Square root of 16 expected at 4\n";
    }
    
    sqrtRes = Sqrt(17);
    if (!Equals(sqrtRes, 4.12310562562f)) {
        std::cout << "Square root of 17 expected at 4.12310562562f\n";
    }
    
    float asinRes = AsinEuler(17);
    if (!Equals(asinRes, 0.301241441f)) {
        std::cout << "Asin of 17 degrees expected at 0.301241441 radians\n";
    }
    
    float acosRes = AcosEuler(17);
    if (!Equals(acosRes, 1.26955489f)) {
        std::cout << "Acos of 17 degrees expected at 1.26955489 radians\n";
    }
    
    float atanRes = AtanEuler(17);
    if (!Equals(atanRes, 0.288432018f)) {
        std::cout << "Atan of 17 degrees expected at 0.288432018 radians\n";
    }
    
    float atan2Res = Atan2Euler(17, 3);
    if (!Equals(atan2Res, 1.39612412452698f)) {
        std::cout << "Atan2 of 17, 3 degrees expected at 1.39612412452698f radians\n";
    }
    
    asinRes = AsinRadian(0.29670438170433);
    if (!Equals(asinRes, 0.301241441f)) {
        std::cout << "Asin of 0.29670438170433 radians expected at 0.301241441 radians\n";
    }
    
    acosRes = AcosRadian(0.29670438170433);
    if (!Equals(acosRes, 1.26955489f)) {
        std::cout << "Acos of 0.29670438170433 radians expected at 1.26955489 radians\n";
    }
    
    atanRes = AtanRadian(0.29670438170433);
    if (!Equals(atanRes, 0.288432018f)) {
        std::cout << "Atan of 0.29670438170433 radians expected at 0.288432018 radians\n";
    }
    
    atan2Res = Atan2Euler(0.29670438170433, 0.0523595958948135);
    if (!Equals(atan2Res, 1.39612412452698f)) {
        std::cout << "Atan2 of 0.29670438170433, 0.0523595958948135 radians expected at 1.39612412452698f radians\n";
    }
}

void UnitTests::PrintQuaternion(const Quaternion& q) {
    std::cout << q.w << ", " << q.x << ", " << q.y << ", " << q.z << "\n";
}

void UnitTests::PrintQuaternion(const char* c, const Quaternion& q) {
    std::cout << c << ": " << q.w << ", " << q.x << ", " << q.y << ", " << q.z << "\n";
}

void UnitTests::PrintMatrix(const ES1::Matrix& mat) {
    ES1::Matrix& m = const_cast<ES1::Matrix&>(mat);
    std::cout << m[0][0] << ", " << m[1][0] << ", " << m[2][0] << ", " << m[3][0] << "\n";
    std::cout << m[0][1] << ", " << m[1][1] << ", " << m[2][1] << ", " << m[3][1] << "\n";
    std::cout << m[0][2] << ", " << m[1][2] << ", " << m[2][2] << ", " << m[3][2] << "\n";
    std::cout << m[0][3] << ", " << m[1][3] << ", " << m[2][3] << ", " << m[3][3] << "\n";
}

void UnitTests::PrintMatrix(const char* c, const ES1::Matrix& m) {
    std::cout << c << "\n";
    PrintMatrix(m);
}

void UnitTests::PrintVector(const ES1::Vector& v) {
    std::cout << v.x << ", " << v.y << ", " << v.z << "\n";
}

void UnitTests::PrintVector(const char* c, const ES1::Vector& v) {
    std::cout << c << ": " << v.x << ", " << v.y << ", " << v.z << "\n";
}