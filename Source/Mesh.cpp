#include <ES1/Framework/Mesh.h>
#include <ES1/Framework/Math.h>
#include <vector>

ES1::Mesh::Mesh() {
    m_pSoup = 0;
    m_pVertices = 0;
    m_pNormals = 0;
    m_pTexCoords = 0;
    m_pColors = 0;
    m_pIndices = 0;
    
    m_vecMin = vec(0.0f, 0.0f, 0.0f);
    m_vecMax = vec(0.0f, 0.0f, 0.0f);
    m_nVertexCount = 0;
    m_nIndexCount = 0;
}

ES1::Mesh::~Mesh() {
    if (m_pSoup != 0) {
        delete[] m_pSoup;
    }
    if (m_pIndices != 0) {
        delete[] m_pIndices;
    }
}

float* ES1::Mesh::GetSoup() {
    return m_pSoup;
}

float* ES1::Mesh::GetVertices() {
    return m_pVertices;
}

float* ES1::Mesh::GetNormals() {
    return m_pNormals;
}

float* ES1::Mesh::GetTexCoords() {
    return m_pTexCoords;
}

float* ES1::Mesh::GetColors() {
    return m_pColors;
}

unsigned int* ES1::Mesh::GetIndices() {
    return m_pIndices;
}

bool ES1::Mesh::IsValid() {
    return m_pSoup != 0 && m_nVertexCount > 0;
}

bool ES1::Mesh::HasVertices() {
    return m_pVertices != 0 && m_nVertexCount > 0;
}

bool ES1::Mesh::HasNormals() {
    return m_pNormals != 0 && m_nVertexCount > 0;
}

bool ES1::Mesh::HasTexCoords() {
    return m_pTexCoords != 0 && m_nVertexCount > 0;
}

bool ES1::Mesh::HasColors() {
    return m_pColors != 0 && m_nVertexCount > 0;
}

bool ES1::Mesh::HasIndices() {
    return m_pIndices != 0 && m_nIndexCount > 0;
}

int ES1::Mesh::GetIndexCount() {
    return m_nIndexCount;
}

int ES1::Mesh::GetVertexCount() {
    return m_nVertexCount;
}

ES1::vec ES1::Mesh::GetMin() {
    return m_vecMin;
}

ES1::vec ES1::Mesh::GetMax() {
    return m_vecMax;
}

float ES1::Mesh::GetRadius() {
    return m_nRadius;
}

// http://gamedev.stackexchange.com/questions/16585/how-do-you-programmatically-generate-a-sphere
// http://www.opengl.org.ru/docs/pg/0208.html
// http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
ES1::Mesh* ES1::Mesh::MakeSphere(bool useNormals, bool useTexCoords) {
    std::vector<float> vertices;
    std::vector<float> normals;
    std::vector<float> texCoords;
    std::vector<unsigned int> indices;
    
    float radius = 1.0f;
    int rings = 20;// Slices
    int sectors = 20; // Stacks
    
    float const R = 1.0f / (float)(rings - 1);
    float const S = 1.0f / (float)(sectors - 1);
    int r, s;
    
    vertices.resize(rings * sectors * 3);
    if (useNormals) {
        normals.resize(rings * sectors * 3);
    }
    if (useTexCoords) {
        texCoords.resize(rings * sectors * 2);
    }
    
    std::vector<float>::iterator v = vertices.begin();
    std::vector<float>::iterator n = normals.begin();
    std::vector<float>::iterator t = texCoords.begin();
    
    float pot = PI * 0.5f;
    
    for(r = 0; r < rings; r++) {
        for(s = 0; s < sectors; s++) {
            float y = SinRadian(-pot + PI * ((float)r) * R);
            float x = CosRadian(2.0f * PI * ((float)s) * S) * SinRadian(PI * ((float)r) * R);
            float z = SinRadian(2.0f * PI * ((float)s) * S) * SinRadian(PI * ((float)r) * R);
            
            if (useTexCoords) { 
                *t++ = ((float)s) * S;
                *t++ = ((float)r) * R;
            }
            
            ES1::Vector norm = ES1::Normalize(ES1::Vector(x, y, z));
            
            *v++ = norm.x * radius;
            *v++ = norm.y * radius;
            *v++ = norm.z * radius;
            
            if (useNormals) {
                *n++ = norm.x;
                *n++ = norm.y;
                *n++ = norm.z;
            }
        }
    }
    
    indices.resize(rings * sectors * 6);
    std::vector<unsigned int>::iterator i = indices.begin();
    
    for(r = 0; r < rings - 1; r++) {
        for(s = 0; s < sectors - 1; s++) {
            //0: *i++ = r * sectors + s;
            //1: *i++ = r * sectors + (s+1);
            //2: *i++ = (r+1) * sectors + (s+1);
            //3: *i++ = (r+1) * sectors + s;

            *i++ = (r+1) * sectors + s;
            *i++ = (r+1) * sectors + (s+1);
            *i++ = r * sectors + (s+1);
            
            *i++ = r * sectors + (s+1);
            *i++ = r * sectors + s;
            *i++ = (r+1) * sectors + s;
        }
    }
    
    float* pVerts = 0;
    int cVerts = (int)vertices.size();
    if (cVerts > 0) {
        pVerts = &vertices[0];
    }
    
    float* pNorms = 0;
    int cNorms = (int)normals.size();
    if (cNorms > 0) {
        pNorms = &normals[0];
    }
    
    float* pTex = 0;
    int cTex = (int)texCoords.size();
    if (cTex > 0) {
        pTex = &texCoords[0];
    }
    
    float* pColors = 0;
    int cColors = 0;
    
    unsigned int* pIndices = 0;
    int cIndices = (int)indices.size();
    if(cIndices > 0) {
        pIndices = &indices[0];
    }
    
    return Construct(pVerts, cVerts, pNorms, cNorms, pTex, cTex, pColors, cColors, pIndices, cIndices);
}

ES1::Mesh* ES1::Mesh::MakePlane(bool useNormals, bool useTexCoords, bool useColors) {
    std::vector<float> vertices;
    std::vector<float> normals;
    std::vector<float> texCoords;
    std::vector<float> colors;
    
    int divisions = 500;
    
    for (int x = 0; x < divisions; ++x) {
        for (int z = 0; z < divisions; ++z) {
            float divisionStep = 2.0f / ((float)divisions);
            
            float xMin = -1.0f + ((float)x) * divisionStep;
            float xMax = xMin + divisionStep;
            
            float zMin = -1.0f + ((float)z) * divisionStep;
            float zMax = zMin + divisionStep;
            
            float uMin = (xMin + 1.0f) * 0.5f;
            float uMax = (xMax+ 1.0f) * 0.5f;
            
            float vMin = (zMin + 1.0f) * 0.5f;
            float vMax = (zMax + 1.0f) * 0.5f;
            
            for (int j = 0; j < 6; ++j) {
                if (useNormals) {
                    normals.push_back(0.0f);
                    normals.push_back(1.0f);
                    normals.push_back(0.0f);
                }
                if (useColors) {
                    colors.push_back(1.0f);
                    colors.push_back(1.0f);
                    colors.push_back(1.0f);
                }
            }
         
            vertices.push_back(xMax);
            vertices.push_back(0.0f);
            vertices.push_back(zMin);
            
            if (useTexCoords) {
                texCoords.push_back(uMax);
                texCoords.push_back(vMin);
            }
            
            vertices.push_back(xMin);
            vertices.push_back(0.0f);
            vertices.push_back(zMin);
            
            if (useTexCoords) {
                texCoords.push_back(uMin);
                texCoords.push_back(vMin);
            }
            
            vertices.push_back(xMin);
            vertices.push_back(0.0f);
            vertices.push_back(zMax);
            
            if (useTexCoords) {
                texCoords.push_back(uMin);
                texCoords.push_back(vMax);
            }
            
            vertices.push_back(xMax);
            vertices.push_back(0.0f);
            vertices.push_back(zMin);
            
            if (useTexCoords) {
                texCoords.push_back(uMax);
                texCoords.push_back(vMin);
            }
            
            vertices.push_back(xMin);
            vertices.push_back(0.0f);
            vertices.push_back(zMax);
            
            if (useTexCoords) {
                texCoords.push_back(uMin);
                texCoords.push_back(vMax);
            }
            
            vertices.push_back(xMax);
            vertices.push_back(0.0f);
            vertices.push_back(zMax);
            
            if (useTexCoords) {
                texCoords.push_back(uMax);
                texCoords.push_back(vMax);
            }
        }
    }
        
    float* pVerts = 0;
    int cVerts = (int)vertices.size();
    if (cVerts > 0) {
        pVerts = &vertices[0];
    }
    
    float* pNorms = 0;
    int cNorms = (int)normals.size();
    if (cNorms > 0) {
        pNorms = &normals[0];
    }
    
    float* pTex = 0;
    int cTex = (int)texCoords.size();
    if (cTex > 0) {
        pTex = &texCoords[0];
    }
    
    float* pColors = 0;
    int cColors = (int)colors.size();
    if (cColors > 0) {
        pColors = &colors[0];
    }
    
    unsigned int* pIndices = 0;
    int cIndices = 0;
    
    return Construct(pVerts, cVerts, pNorms, cNorms, pTex, cTex, pColors, cColors, pIndices, cIndices);
}

ES1::Mesh* ES1::Mesh::MakeCube(bool useNormals, bool useUvs, bool useColors) {
    //-x, -y, -z     +-----+    x, -y, -z
    //             / |    /|
    //-x, -y,  z  +--+---+ |    x, -y,  z
    //-x,  y, -z  |  +---|-+    x,  y, -z
    //            | /    | /
    //-x,  y,  z  +------+       x,  y,  z
    
    std::vector<float> vertices;
    std::vector<float> normals;
    std::vector<float> texCoords;
    std::vector<float> colors;
    
    // bottom face
    for (int i = 0; i < 6; ++i) {
        if (useNormals) {
            normals.push_back(0.0f);
            normals.push_back(-1.0f);
            normals.push_back(0.0f);
        }
        if (useColors) {
            colors.push_back(0.0f);
            colors.push_back(1.0f);
            colors.push_back(0.0f);
        }
    }
    // TODO: UVs
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    
    //top face
    for (int i = 0; i < 6; ++i) {
        if (useNormals) {
            normals.push_back(0.0f);
            normals.push_back(1.0f);
            normals.push_back(0.0f);
        }
        if (useColors) {
            colors.push_back(0.0f);
            colors.push_back(1.0f);
            colors.push_back(0.0f);
        }
    }
    // TODO: UVs
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    
    //right face
    for (int i = 0; i < 6; ++i) {
        if (useNormals) {
            normals.push_back(1.0f);
            normals.push_back(0.0f);
            normals.push_back(0.0f);
        }
        if (useColors) {
            colors.push_back(1.0f);
            colors.push_back(0.0f);
            colors.push_back(0.0f);
        }
    }
    // TODO: UVs
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    
    //front face
    for (int i = 0; i < 6; ++i) {
        if (useNormals) {
            normals.push_back(0.0f);
            normals.push_back(0.0f);
            normals.push_back(1.0f);
        }
        if (useColors) {
            colors.push_back(0.0f);
            colors.push_back(0.0f);
            colors.push_back(1.0f);
        }
    }
    // TODO: UVs
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    
    //left face
    for (int i = 0; i < 6; ++i) {
        if (useNormals) {
            normals.push_back(-1.0f);
            normals.push_back(0.0f);
            normals.push_back(0.0f);
        }
        if (useColors) {
            colors.push_back(1.0f);
            colors.push_back(0.0f);
            colors.push_back(0.0f);
        }
    }
    // TODO: UVs
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    
    //back face
    for (int i = 0; i < 6; ++i) {
        if (useNormals) {
            normals.push_back(0.0f);
            normals.push_back(0.0f);
            normals.push_back(-1.0f);
        }
        if (useColors) {
            colors.push_back(0.0f);
            colors.push_back(0.0f);
            colors.push_back(1.0f);
        }
    }
    // TODO: UVs
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    vertices.push_back(-1.0f);
    
    vertices.push_back(-1.0f);
    vertices.push_back(1.0f);
    vertices.push_back(-1.0f);
    
    float* pVerts = 0;
    int cVerts = (int)vertices.size();
    if (cVerts > 0) {
        pVerts = &vertices[0];
    }
    
    float* pNorms = 0;
    int cNorms = (int)normals.size();
    if (cNorms > 0) {
        pNorms = &normals[0];
    }
    
    float* pTex = 0;
    int cTex = (int)texCoords.size();
    if (cTex > 0) {
        pTex = &texCoords[0];
    }
    
    float* pColors = 0;
    int cColors = (int)colors.size();
    if (cColors > 0) {
        pColors = &colors[0];
    }
    
    unsigned int* pIndices = 0;
    int cIndices = 0;
    
    return Construct(pVerts, cVerts, pNorms, cNorms, pTex, cTex, pColors, cColors, pIndices, cIndices);
}

ES1::Mesh* ES1::Mesh::Construct(float* vertices, int numVertices, float* normals, int numNormals, float* texCoords, int numTexCoords, float* colors, int numColors, unsigned int* indices, int numIndices) {
    
    Mesh* result = new Mesh();
    
    result->m_pSoup = 0;
    result->m_pVertices = 0;
    result->m_pNormals = 0;
    result->m_pTexCoords = 0;
    result->m_pColors = 0;
    result->m_pIndices = 0;
    
    result->m_vecMin = vec(0.0f, 0.0f, 0.0f);
    result->m_vecMax = vec(0.0f, 0.0f, 0.0f);
    result->m_nVertexCount = 0;
    result->m_nIndexCount = 0;
    result->m_nRadius = 0.0f;
    
    int vertexCount = numVertices / 3;
    int normalCount = numNormals / 3;
    int colorCount =  numColors / 3;
    int texCount = numTexCoords / 2;
    int indexCount = numIndices;
    
    if (vertexCount == 0) {
        // TODO: Log error?
        return result;
    }
    
    if (normalCount != 0 && normalCount != vertexCount) {
        // TODO: Log error?
        return result;
    }
    
    if (colorCount != 0 && colorCount != vertexCount) {
        // TODO: Log error??
        return result;
    }
    
    if (texCount != 0 && texCount != vertexCount) {
        // TODO: Log error??
        return result;
    }
    
    int size = vertexCount * 3 + normalCount * 3 + colorCount * 3 + texCount * 2;
    result->m_pSoup = new float[size];
    
    int offset = 0;
    memcpy(&result->m_pSoup[offset], &vertices[0], sizeof(float) * (vertexCount * 3));
    result->m_pVertices = &result->m_pSoup[offset];
    offset += vertexCount * 3;
    
    result->m_nVertexCount = vertexCount;
    
    if (normalCount != 0) {
        memcpy(&result->m_pSoup[offset], &normals[0], sizeof(float) * (normalCount * 3));
        result->m_pNormals = &result->m_pSoup[offset];
        offset += normalCount * 3;
    }
    else {
        result->m_pNormals = 0;
    }
    
    if (texCount != 0) {
        memcpy(&result->m_pSoup[offset], &texCoords[0], sizeof(float) * (texCount * 2));
        result->m_pTexCoords = &result->m_pSoup[offset];
        offset += texCount * 2;
    }
    else {
        result->m_pTexCoords = 0;
    }
    
    if (colorCount != 0) {
        memcpy(&result->m_pSoup[offset], &colors[0], sizeof(float) * (colorCount * 3));
        result->m_pColors = &result->m_pSoup[offset];
        offset += colorCount * 3;
    }
    else {
        result->m_pColors = 0;
    }
    
    if (indexCount == 0) {
        result->m_pIndices = 0;
        result->m_nIndexCount = 0;
    }
    else {
        result->m_pIndices = new unsigned int[indexCount];
        memcpy(&result->m_pIndices[0], &indices[0], sizeof(unsigned int) * indexCount);
        result->m_nIndexCount = indexCount;
    }
    
    result->m_vecMin.x = result->m_vecMax.x = vertices[0];
    result->m_vecMin.y = result->m_vecMax.y = vertices[1];
    result->m_vecMin.z = result->m_vecMax.z = vertices[2];
    
    for (int i = 0, size = vertexCount * 3; i < size; i += 3) {
        if (vertices[i + 0] < result->m_vecMin.x) {
            result->m_vecMin.x = vertices[i + 0];
        }
        if (vertices[i + 1] < result->m_vecMin.y) {
            result->m_vecMin.y = vertices[i + 1];
        }
        if (vertices[i + 2] < result->m_vecMin.z) {
            result->m_vecMin.z = vertices[i + 2];
        }
        
        if (vertices[i + 0] > result->m_vecMax.x) {
            result->m_vecMax.x = vertices[i + 0];
        }
        if (vertices[i + 1] > result->m_vecMax.y) {
            result->m_vecMax.y = vertices[i + 1];
        }
        if (vertices[i + 2] > result->m_vecMax.z) {
            result->m_vecMax.z = vertices[i + 2];
        }
    }
    
    result->m_nRadius = Max(Length(result->m_vecMax), Length(result->m_vecMin));    
    
    return result;
}