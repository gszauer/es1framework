#include <ES1/Framework/Math.h>
#include <cmath>

float ES1::CosRadian(float rad) {
    return cosf(rad);
}

float ES1::SinRadian(float rad) {
    return sinf(rad);
}

float ES1::TanRadian(float rad) {
    return tanf(rad);
}

float ES1::AsinRadian(float rad) {
    return asinf(rad);
}

float ES1::AcosRadian(float rad) {
    return acosf(rad);
}

float ES1::AtanRadian(float rad) {
    return atanf(rad);
}

float ES1::Atan2Radian(float radX, float radY) {
    return atan2f(radX, radY);
}

float ES1::Lerp(float v1, float v2, float delta) {
    return v1 + (v2 - v1) * delta;
}

float ES1::Min(float x, float y) {
    if (x <= y) {
        return x;
    }
    return y;
}

float ES1::Max(float x, float y) {
    if (x >= y) {
        return x;
    }
    return y;
}

float ES1::Clamp(float input, float min, float max) {
    if (input < min)
        return min;
    if (input > max)
        return max;
    return input;
}

float ES1::Clamp01(float input) {
    if (input < 0.0f)
        return 0.0f;
    if (input > 1.0f)
        return 1.0f;
    return input;
}

bool ES1::Equals(float f1, float f2) {
    float diff = ES1::Abs(f1 - f2);
    return diff < EPSILON;
}

float ES1::Sqrt(float number) {
    return sqrtf(number);
}

float ES1::Pow(float base, float exponent) {
    return powf(base, exponent);
}

float ES1::CosEuler(float euler) {
    return cosf(euler * DEG2RAD);
}

float ES1::SinEuler(float euler) {
    return sinf(euler * DEG2RAD);
}

float ES1::Abs(float value) {
    if (value < 0.0f) {
        return -1.0f * value;
    }
    return value;
}

float ES1::TanEuler(float euler) {
    return tanf(euler * DEG2RAD);
}

float ES1::AsinEuler(float euler) {
    return asinf(euler * DEG2RAD);
}

float ES1::AcosEuler(float euler) {
    return acosf(euler * DEG2RAD);
}

float ES1::AtanEuler(float euler) {
    return atanf(euler * DEG2RAD);
}

float ES1::Atan2Euler(float eulerX, float eulerY) {
    return atan2f(eulerX * DEG2RAD, eulerY * DEG2RAD);
}