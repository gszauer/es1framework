#include <ES1/Framework/Platform.h>
#include <ES1/Framework/Application.h>
#include <ES1/Framework/Logger.h>
#include <ES1/Framework/DialogWindows.h>
#include <ES1/Framework/FileSystem.h>
#include <ES1/Framework/Clipboard.h>
#include <ES1/Framework/Mouse.h>
#include <ES1/Framework/Keyboard.h>
#include <ES1/Framework/Renderer.h>

std::string ES1::Platform::GetWorkingDir() {
    return m_strWorkingDir;
}
std::string ES1::Platform::GetAssetsDir() {
    return m_strAssetsDir;
}

std::string ES1::Platform::GetArgv(int index) {
    return m_vArgs[index];
}

ES1::Application* ES1::Platform::GetApplication() {
    return m_pApplication;
}

int ES1::Platform::GetArgc() {
    return (int)m_vArgs.size();
}

ES1::Platform::Platform(Application* pApplication) {
    m_pApplication = pApplication;
    m_dLastTime = 0.0;
    
    std::string assetsDir = GetExecutableDir() + "/Assets";
    SetAssetsDir(assetsDir.c_str());
    std::string workingDir = GetPersistentDir();
    SetWorkingDir(workingDir.c_str());
    
    m_pWindows = new DialogWindows(*this);
    m_pClipboard = new Clipboard(*this);
    m_pFileSystem = new FileSystem(*this, GetWorkingDir().c_str(), GetAssetsDir().c_str(), GetPersistentDir().c_str());
    
    m_pMouse = new Mouse();
    m_pKeyboard = new Keyboard();
    
    m_pApplication->SetKeyboard(m_pKeyboard);
    m_pApplication->SetMouse(m_pMouse);
    m_pApplication->SetDialogWindows(m_pWindows);
    m_pApplication->SetFileSystem(m_pFileSystem);
    m_pApplication->SetClipboard(m_pClipboard);
}

ES1::Platform::~Platform() {
    delete m_pKeyboard;
    delete m_pMouse;
    delete m_pWindows;
    delete m_pFileSystem;
    delete m_pClipboard;
}

void ES1::Platform::Initialize(int w, int h) {
    m_bAlive = true;
    m_pRenderer = new Renderer();
    m_dLastTime = GetMilliseconds();
    m_pApplication->OnInitialize(m_pRenderer, w, h);
}

void ES1::Platform::Shutdown() {
    m_bAlive = false;
    
    m_pApplication->OnShutdown();
    
    if (m_pRenderer != 0) {
        delete m_pRenderer;
        m_pRenderer = 0;
    }
}

void ES1::Platform::Resize(int w, int h) {
    m_pApplication->OnResize(w, h);
}

void ES1::Platform::SetMousePosition(int x, int y) {
    m_pMouse->SetPosition(x, y);
}

void ES1::Platform::SetScrollWheel(float delta) {
    int dir = 0;
    if (delta > MOUSE_SCROLL_DELTA) {
        dir = 1;
    }
    else if (delta < -MOUSE_SCROLL_DELTA) {
        dir = -1;
    }
    m_pMouse->SetScrollDirection(dir);
}

void ES1::Platform::OnMouseDown(ES1::Mousedef button) {
    m_pMouse->SetMouseButton(button);
}

void ES1::Platform::OnMouseUp(ES1::Mousedef button) {
    m_pMouse->ClearMouseButton(button);
}

void ES1::Platform::MouseEnteredWindow() {
    m_pMouse->SetInWidow(true);
}

void ES1::Platform::MouseLeftWindow() {
    m_pMouse->SetInWidow(false);
}

void ES1::Platform::OnKeyDown(Keydef eKey) {
    m_pKeyboard->SetKeyPressed(eKey);
}

void ES1::Platform::OnKeyUp(Keydef eKey) {
    m_pKeyboard->SetKeyReleased(eKey);
}

void ES1::Platform::SetWorkingDir(const char* szDir) {
    if (DirectoryExists(szDir)) {
        m_strWorkingDir = szDir;
    }
    else if (CreateDirectory(szDir)){
        m_pApplication->GetWarningLog()->Log("Created working directory at: %s\n", szDir);
        m_strWorkingDir = GetExecutableDir();
    }
    else {
        m_pApplication->GetErrorLog()->Log("Working directory not found at: %s\nFalling back to: %s\n", szDir, GetExecutableDir().c_str());
        m_strWorkingDir = GetExecutableDir();
    }
}

void ES1::Platform::SetAssetsDir(const char* szDir) {
    if (DirectoryExists(szDir)) {
        m_strAssetsDir = szDir;
    }
    else {
        m_pApplication->GetErrorLog()->Log("Assets directory not found at: %s\nFalling back to: %s\n", szDir, GetExecutableDir().c_str());
        m_strAssetsDir = GetExecutableDir();
    }
}

void ES1::Platform::Frame() {
    double currentTime = GetMilliseconds();
    double deltaTime = double(currentTime - m_dLastTime) * 0.001;
    m_dLastTime = currentTime;
    
    m_pApplication->OnUpdate(deltaTime);
    m_pApplication->OnRender(m_pRenderer);

    m_pMouse->Update();
    m_pKeyboard->Update();
}