#include <ES1/Framework/Light.h>

// Blending / Multipass
// http://stackoverflow.com/questions/11839965/how-do-you-add-light-with-multiple-passes-in-opengl
// https://www.opengl.org/discussion_boards/showthread.php/172033-Multi-pass-rendering-problem
// https://web.archive.org/web/20130321073308/http://nehe.gamedev.net/tutorial/blending/16001
// http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-10-transparency/

// Lights
// https://web.archive.org/web/20130304134441/http://nehe.gamedev.net/tutorial/texture_filters%2C_lighting_%26_keyboard_control/15002/
// http://www.just.edu.jo/~yaser/courses/cs480/Tutorials/OpenGl%20-%20Light%20Part%20II.htm

ES1::PointLight::PointLight() {
    lightPosition = Vector(0.0f, 0.0f, 0.0f);
    lightRadius = 1.0f;
    lightColor = ES1::White;
    // falloff??
}

ES1::PointLight::PointLight(const ES1::PointLight& o) {
    lightPosition = o.lightPosition;
    lightColor = o.lightColor;
    lightRadius = o.lightRadius;
}

ES1::PointLight& ES1::PointLight::operator=(const ES1::PointLight& o) {
    if (&o == this) {
        return *this;
    }
    
    lightPosition = o.lightPosition;
    lightColor = o.lightColor;
    lightRadius = o.lightRadius;
    
    return *this;
}
    
ES1::DirectionalLight::DirectionalLight() {
    lightColor = ES1::White;
    lightDirection = Normalize(Vector(1.0f, 1.0f, 1.0f));
}

ES1::DirectionalLight::DirectionalLight(const ES1::DirectionalLight& o) {
    lightColor = o.lightColor;
    lightDirection = o.lightDirection;
}

ES1::DirectionalLight& ES1::DirectionalLight::operator=(const ES1::DirectionalLight& o) {
    if (&o == this) {
        return *this;
    }
    
    lightColor = o.lightColor;
    lightDirection = o.lightDirection;
    
    return *this;
}

ES1::DirectionalLight::~DirectionalLight() { }

ES1::PointLight::~PointLight() { }