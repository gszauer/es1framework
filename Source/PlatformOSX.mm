// gcc Cocoa.m -o OSXWindow -framework Cocoa -framework Quartz -framework OpenGL

#include <ES1/Framework/Platform.h>
#include <ES1/Framework/Application.h>
#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/OpenGL.h>
#include <OpenGL/gl.h>
#include <iostream>
#include <sys/time.h>
#include <unistd.h>
#include <algorithm>

ES1::Platform* g_pPlatform = 0;

ES1::Keydef OSX_GetKey(unsigned short code, unsigned long modifierFlags);

@class View;
static CVReturn GlobalDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);

@interface View : NSOpenGLView <NSWindowDelegate> {
    @public
    CVDisplayLinkRef displayLink;
    bool running;
    NSRect windowRect;
    NSRecursiveLock* appLock;
}
@end

@implementation View
// Initialize
- (id) initWithFrame: (NSRect) frame {
    running = true;
    
    // No multisampling
    int samples = 0;
    
    // Keep multisampling attributes at the start of the attribute lists since code below assumes they are array elements 0 through 4.
    NSOpenGLPixelFormatAttribute windowedAttrs[] =
    {
        NSOpenGLPFAMultisample,
        NSOpenGLPFASampleBuffers, static_cast<NSOpenGLPixelFormatAttribute>(samples ? 1 : 0),
        NSOpenGLPFASamples, static_cast<NSOpenGLPixelFormatAttribute>(samples),
        NSOpenGLPFAAccelerated,
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFAColorSize, 32,
        NSOpenGLPFADepthSize, 24,
        NSOpenGLPFAAlphaSize, 8,
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersionLegacy,
        0
    };
    
    // Try to choose a supported pixel format
    NSOpenGLPixelFormat* pf = [[NSOpenGLPixelFormat alloc] initWithAttributes:windowedAttrs];
    
    if (!pf) {
        bool valid = false;
        while (!pf && samples > 0) {
            samples /= 2;
            windowedAttrs[2] = samples ? 1 : 0;
            windowedAttrs[4] = samples;
            pf = [[NSOpenGLPixelFormat alloc] initWithAttributes:windowedAttrs];
            if (pf) {
                valid = true;
                break;
            }
        }
        
        if (!valid) {
            std::cout << "OpenGL pixel format not supported.\n";
            return nil;
        }
    }
    
    self = [super initWithFrame:frame pixelFormat:pf];
    appLock = [[NSRecursiveLock alloc] init];
    
    return self;
}

- (void) prepareOpenGL {
    [super prepareOpenGL];
    
    [[self window] setLevel: NSNormalWindowLevel];
    [[self window] makeKeyAndOrderFront: self];
    
    // Make all the OpenGL calls to setup rendering and build the necessary rendering objects
    [[self openGLContext] makeCurrentContext];
    // Synchronize buffer swaps with vertical refresh rate
    GLint swapInt = 1; // Vsynch on!
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // Create a display link capable of being used with all active displays
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    
    // Set the renderer output callback function
    CVDisplayLinkSetOutputCallback(displayLink, &GlobalDisplayLinkCallback, (__bridge void *)(self));
    
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    
    GLint dim[2] = {static_cast<GLint>(windowRect.size.width), static_cast<GLint>(windowRect.size.height)};
    CGLSetParameter(cglContext, kCGLCPSurfaceBackingSize, dim);
    CGLEnable(cglContext, kCGLCESurfaceBackingSize);
    
    [appLock lock];
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    if (g_pPlatform != 0) {
        g_pPlatform->Initialize(dim[0], dim[1]);
        g_pPlatform->Resize(dim[0], dim[1]);
    }
    std::cout << "Executable dir:   " << g_pPlatform->GetExecutableDir() << "\n";
    std::cout << "Working dir:      " << g_pPlatform->GetWorkingDir() << "\n";
    std::cout << "Assets dir:       " << g_pPlatform->GetAssetsDir() << "\n";
    std::cout << "Persistent dir:   " << g_pPlatform->GetPersistentDir() << "\n";
    std::cout << "Argv[0]:          " << g_pPlatform->GetArgv(0) << "\n";
    std::cout << "GL version:       " << glGetString(GL_VERSION) << "\n";
    std::cout << "GLSL version:     " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    [appLock unlock];
    
    // Activate the display link
    CVDisplayLinkStart(displayLink);
}

// Tell the window to accept input events
- (BOOL)acceptsFirstResponder {
    return YES;
}

- (void)mouseMoved:(NSEvent*) event {
    [appLock lock];
    NSPoint point = [self convertPoint:[event locationInWindow] fromView:nil];
    if (g_pPlatform != 0) {
        g_pPlatform->SetMousePosition(point.x, point.y);
    }
    [appLock unlock];
}

- (void) mouseDragged: (NSEvent*) event {
    [appLock lock];
    NSPoint point = [self convertPoint:[event locationInWindow] fromView:nil];
    if (g_pPlatform != 0) {
        g_pPlatform->SetMousePosition(point.x, point.y);
    }
    [appLock unlock];
}

- (void)scrollWheel: (NSEvent*) event  {
    [appLock lock];
    NSPoint point = [self convertPoint:[event locationInWindow] fromView:nil];
    if (g_pPlatform != 0) {
        g_pPlatform->SetMousePosition(point.x, point.y);
        g_pPlatform->SetScrollWheel([event deltaY]);
    }
    [appLock unlock];
}

- (void) mouseDown: (NSEvent*) event {
    [appLock lock];
    NSPoint point = [self convertPoint:[event locationInWindow] fromView:nil];
    if (g_pPlatform != 0) {
        g_pPlatform->SetMousePosition(point.x, point.y);
        g_pPlatform->OnMouseDown(ES1::Mousedef::MOUSE_LEFT);
    }
    [appLock unlock];
}

- (void) mouseUp: (NSEvent*) event {
    [appLock lock];
    NSPoint point = [self convertPoint:[event locationInWindow] fromView:nil];
    if (g_pPlatform != 0) {
        g_pPlatform->SetMousePosition(point.x, point.y);
        g_pPlatform->OnMouseUp(ES1::Mousedef::MOUSE_LEFT);
    }
    [appLock unlock];
}

- (void) rightMouseDown: (NSEvent*) event {
    [appLock lock];
    NSPoint point = [self convertPoint:[event locationInWindow] fromView:nil];
    if (g_pPlatform != 0) {
        g_pPlatform->SetMousePosition(point.x, point.y);
        g_pPlatform->OnMouseDown(ES1::Mousedef::MOUSE_RIGHT);
    }
    [appLock unlock];
}

- (void) rightMouseUp: (NSEvent*) event {
    [appLock lock];
    NSPoint point = [self convertPoint:[event locationInWindow] fromView:nil];
    if (g_pPlatform != 0) {
        g_pPlatform->SetMousePosition(point.x, point.y);
        g_pPlatform->OnMouseUp(ES1::Mousedef::MOUSE_RIGHT);
    }
    [appLock unlock];
}

- (void)otherMouseDown: (NSEvent*) event {
    [appLock lock];
    NSPoint point = [self convertPoint:[event locationInWindow] fromView:nil];
    if (g_pPlatform != 0) {
        g_pPlatform->SetMousePosition(point.x, point.y);
        g_pPlatform->OnMouseDown(ES1::Mousedef::MOUSE_MIDDLE);
    }
    [appLock unlock];
}

- (void)otherMouseUp: (NSEvent*) event {
    [appLock lock];
    NSPoint point = [self convertPoint:[event locationInWindow] fromView:nil];
    if (g_pPlatform != 0) {
        g_pPlatform->SetMousePosition(point.x, point.y);
        g_pPlatform->OnMouseUp(ES1::Mousedef::MOUSE_MIDDLE);
    }
    [appLock unlock];
}

- (void) mouseEntered: (NSEvent*)event {
    [appLock lock];
    if (g_pPlatform != 0) {
        g_pPlatform->MouseEnteredWindow();
    }
    [appLock unlock];
}

- (void) mouseExited: (NSEvent*)event {
    [appLock lock];
    if (g_pPlatform != 0) {
        g_pPlatform->MouseLeftWindow();
    }
    [appLock unlock];
}

- (void) keyDown: (NSEvent*) event {
    [appLock lock];
    if ([event isARepeat] == NO) {
        if (g_pPlatform != 0) {
            g_pPlatform->OnKeyDown(OSX_GetKey([event keyCode], [event modifierFlags]));
        }
    }
    [appLock unlock];
}

- (void) keyUp: (NSEvent*) event {
    [appLock lock];
    if (g_pPlatform != 0) {
        g_pPlatform->OnKeyDown(OSX_GetKey([event keyCode], [event modifierFlags]));
    }
    [appLock unlock];
}

// Update
- (CVReturn) getFrameForTime:(const CVTimeStamp*)outputTime {
    if (!running) {
        return kCVReturnSuccess;
    }
    [appLock lock];
    
    [[self openGLContext] makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    if (g_pPlatform != 0) {
        g_pPlatform->Frame();
    }
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    if (false) { // Update loop returns false
        [NSApp terminate:self];
    }
    
    [appLock unlock];
    
    return kCVReturnSuccess;
}

// Resize
- (void)windowDidResize:(NSNotification*)notification {
    if (!running) {
        return;
    }
    NSSize size = [ [ _window contentView ] frame ].size;
    [appLock lock];
    [[self openGLContext] makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    
    windowRect.size.width = size.width;
    windowRect.size.height = size.height;
    if (g_pPlatform != 0) {
        g_pPlatform->Resize(size.width, size.height);
    }

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    [appLock unlock];
}

- (void)resumeDisplayRenderer  {
    [appLock lock];
    CVDisplayLinkStop(displayLink);
    [appLock unlock];
}

- (void)haltDisplayRenderer  {
    [appLock lock];
    CVDisplayLinkStop(displayLink);
    [appLock unlock];
}

// Terminate window when the red X is pressed
-(void)windowWillClose:(NSNotification *)notification {
    if (running) {
        running = false;
        
        [appLock lock];
        
        if (g_pPlatform != 0) {
            g_pPlatform->Shutdown();
        }
        
        CVDisplayLinkStop(displayLink);
        CVDisplayLinkRelease(displayLink);
        
        [appLock unlock];
    }
    
    [NSApp terminate:self];
}

// Cleanup
- (void) dealloc {
}
@end

static CVReturn GlobalDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* now, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* displayLinkContext) {
    CVReturn result = [(__bridge View*)displayLinkContext getFrameForTime:outputTime];
    return result;
}

int ES1::Platform::MessagePump(int argc, const char** argv)  {
    g_pPlatform = this;
    
    // Create a shared app instance.
    if (NSApplicationLoad() == NO) {
        std::cout << "Application not loaded.\n";
        return -1;
    }
    [NSApplication sharedApplication];
    
    for (int i = 0; i < argc; ++i) {
        m_vArgs.push_back(argv[i]);
    }
    
    // Style flags
    NSUInteger windowStyle = NSTitledWindowMask  | NSClosableWindowMask | NSResizableWindowMask | NSMiniaturizableWindowMask;
    
    // Window bounds (x, y, width, height)
    NSRect screenRect = [[NSScreen mainScreen] frame];
    NSRect viewRect = NSMakeRect(0, 0, 800, 600);
    NSRect windowRect = NSMakeRect(NSMidX(screenRect) - NSMidX(viewRect),
                                   NSMidY(screenRect) - NSMidY(viewRect),
                                   viewRect.size.width,
                                   viewRect.size.height);
    
    NSWindow * window = [[NSWindow alloc] initWithContentRect:windowRect
                                                    styleMask:windowStyle
                                                      backing:NSBackingStoreBuffered
                                                        defer:NO];
    
    // Since Snow Leopard, programs without application bundles and Info.plist files don't get a menubar
    // and can't be brought to the front unless the presentation option is changed
    [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
    
    // NSString name of the app
    id appName = [NSString stringWithUTF8String: g_pPlatform->GetApplication()->GetName().c_str()]; // [[NSProcessInfo processInfo] processName];
    
    // Next, we need to create the menu bar. You don't need to give the first item in the menubar a name
    // (it will get the application's name automatically)
    id menubar = [NSMenu new];
    id appMenuItem = [NSMenuItem new];
    //[appMenuItem setTitle: appName]; Can't seem to set the main menu item here. Oh well.
    
    [menubar addItem:appMenuItem];
    [NSApp setMainMenu:menubar];
    
    // Then we add the quit item to the menu. Fortunately the action is simple since terminate: is 
    // already implemented in NSApplication and the NSApplication is always in the responder chain.
    id appMenu = [NSMenu new];
    id quitTitle = [@"Quit " stringByAppendingString:appName];
    id quitMenuItem = [[NSMenuItem alloc] initWithTitle:quitTitle
                                                  action:@selector(terminate:) keyEquivalent:@"q"];
    [appMenu addItem:quitMenuItem];
    [appMenuItem setSubmenu:appMenu];
    
    // Create app delegate to handle system events
    View* view = [[View alloc] initWithFrame:windowRect];
    view->windowRect = windowRect;
    [window setAcceptsMouseMovedEvents:YES];
    [window setContentView:view];
    [window setDelegate:view];
    
    // Set app title
    [window setTitle:appName];
    
    // Add fullscreen button
    [window setCollectionBehavior: NSWindowCollectionBehaviorFullScreenPrimary];
    
    // Show window and run event loop 
    [window orderFrontRegardless]; 
    [NSApp run]; 
    
    return (0); 
}

ES1::Keydef OSX_GetKey(unsigned short code, unsigned long modifierFlags) {
    bool shiftDown = (modifierFlags & NSShiftKeyMask);
    unsigned int caps = (shiftDown ? 1 : 0) ^ ((modifierFlags & NSAlphaShiftKeyMask) ? 1 : 0);
    
    
    switch(code)
    {
        case 0x69:
            return ES1::Keydef::KEY_PRINT;
        case 0x35:
            return ES1::Keydef::KEY_ESCAPE;
        case 0x33:
            return ES1::Keydef::KEY_BACKSPACE;
        case 0x30:
            return ES1::Keydef::KEY_TAB;
        case 0x24:
            return ES1::Keydef::KEY_RETURN;
        case 0x72:
            return ES1::Keydef::KEY_INSERT;
        case 0x73:
            return ES1::Keydef::KEY_HOME;
        case 0x74:
            return ES1::Keydef::KEY_PG_UP;
        case 0x79:
            return ES1::Keydef::KEY_PG_DOWN;
        case 0x75:
            return ES1::Keydef::KEY_DELETE;
        case 0x77:
            return ES1::Keydef::KEY_END;
        case 0x7B:
            return ES1::Keydef::KEY_LEFT_ARROW;
        case 0x7C:
            return ES1::Keydef::KEY_RIGHT_ARROW;
        case 0x7E:
            return ES1::Keydef::KEY_UP_ARROW;
        case 0x7D:
            return ES1::Keydef::KEY_DOWN_ARROW;
        case 0x47:
            return ES1::Keydef::KEY_NUM_LOCK;
        case 0x45:
            return ES1::Keydef::KEY_KP_PLUS;
        case 0x4E:
            return ES1::Keydef::KEY_KP_MINUS;
        case 0x43:
            return ES1::Keydef::KEY_KP_MULTIPLY;
        case 0x4B:
            return ES1::Keydef::KEY_KP_DIVIDE;
        case 0x59:
            return ES1::Keydef::KEY_KP_HOME;
        case 0x5B:
            return ES1::Keydef::KEY_KP_UP;
        case 0x5C:
            return ES1::Keydef::KEY_KP_PG_UP;
        case 0x56:
            return ES1::Keydef::KEY_KP_LEFT;
        case 0x57:
            return ES1::Keydef::KEY_KP_FIVE;
        case 0x58:
            return ES1::Keydef::KEY_KP_RIGHT;
        case 0x53:
            return ES1::Keydef::KEY_KP_END;
        case 0x54:
            return ES1::Keydef::KEY_KP_DOWN;
        case 0x55:
            return ES1::Keydef::KEY_KP_PG_DOWN;
        case 0x52:
            return ES1::Keydef::KEY_KP_INSERT;
        case 0x41:
            return ES1::Keydef::KEY_KP_DELETE;
        case 0x7A:
            return ES1::Keydef::KEY_F1;
        case 0x78:
            return ES1::Keydef::KEY_F2;
        case 0x63:
            return ES1::Keydef::KEY_F3;
        case 0x76:
            return ES1::Keydef::KEY_F4;
        case 0x60:
            return ES1::Keydef::KEY_F5;
        case 0x61:
            return ES1::Keydef::KEY_F6;
        case 0x62:
            return ES1::Keydef::KEY_F7;
        case 0x64:
            return ES1::Keydef::KEY_F8;
        case 0x65:
            return ES1::Keydef::KEY_F9;
        case 0x6D:
            return ES1::Keydef::KEY_F10;
        // MACOS reserved:
            // return ES1::Keydef::KEY_F11;
            // return ES1::Keydef::KEY_F12;
            // return ES1::Keydef::KEY_PAUSE;
            // return ES1::Keydef::KEY_SCROLL_LOCK;
        case 0x31:
            return ES1::Keydef::KEY_SPACE;
        case 0x1D:
            return shiftDown ? ES1::Keydef::KEY_RIGHT_PARENTHESIS : ES1::Keydef::KEY_ZERO;
        case 0x12:
            return shiftDown ? ES1::Keydef::KEY_EXCLAM : ES1::Keydef::KEY_ONE;
        case 0x13:
            return shiftDown ? ES1::Keydef::KEY_AT : ES1::Keydef::KEY_TWO;
        case 0x14:
            return shiftDown ? ES1::Keydef::KEY_NUMBER : ES1::Keydef::KEY_THREE;
        case 0x15:
            return shiftDown ? ES1::Keydef::KEY_DOLLAR : ES1::Keydef::KEY_FOUR;
        case 0x17:
            return shiftDown ? ES1::Keydef::KEY_PERCENT : ES1::Keydef::KEY_FIVE;
        case 0x16:
            return shiftDown ? ES1::Keydef::KEY_CIRCUMFLEX : ES1::Keydef::KEY_SIX;
        case 0x1A:
            return shiftDown ? ES1::Keydef::KEY_AMPERSAND : ES1::Keydef::KEY_SEVEN;
        case 0x1C:
            return shiftDown ? ES1::Keydef::KEY_ASTERISK : ES1::Keydef::KEY_EIGHT;
        case 0x19:
            return shiftDown ? ES1::Keydef::KEY_LEFT_PARENTHESIS : ES1::Keydef::KEY_NINE;
        case 0x18:
            return shiftDown ? ES1::Keydef::KEY_EQUAL : ES1::Keydef::KEY_PLUS;
        case 0x2B:
            return shiftDown ? ES1::Keydef::KEY_LESS_THAN : ES1::Keydef::KEY_COMMA;
        case 0x1B:
            return shiftDown ? ES1::Keydef::KEY_UNDERSCORE : ES1::Keydef::KEY_MINUS;
        case 0x2F:
            return shiftDown ? ES1::Keydef::KEY_GREATER_THAN : ES1::Keydef::KEY_PERIOD;
        case 0x29:
            return shiftDown ? ES1::Keydef::KEY_COLON : ES1::Keydef::KEY_SEMICOLON;
        case 0x2C:
            return shiftDown ? ES1::Keydef::KEY_QUESTION : ES1::Keydef::KEY_SLASH;
        case 0x32:
            return shiftDown ? ES1::Keydef::KEY_GRAVE : ES1::Keydef::KEY_TILDE;
        case 0x21:
            return shiftDown ? ES1::Keydef::KEY_LEFT_BRACE : ES1::Keydef::KEY_LEFT_BRACKET;
        case 0x2A:
            return shiftDown ? ES1::Keydef::KEY_BAR : ES1::Keydef::KEY_BACK_SLASH;
        case 0x1E:
            return shiftDown ? ES1::Keydef::KEY_RIGHT_BRACE : ES1::Keydef::KEY_RIGHT_BRACKET;
        case 0x27:
            return shiftDown ? ES1::Keydef::KEY_QUOTE : ES1::Keydef::KEY_APOSTROPHE;
        case 0x00:
            return caps ? ES1::Keydef::KEY_CAPITAL_A : ES1::Keydef::KEY_A;
        case 0x0B:
            return caps ? ES1::Keydef::KEY_CAPITAL_B : ES1::Keydef::KEY_B;
        case 0x08:
            return caps ? ES1::Keydef::KEY_CAPITAL_C : ES1::Keydef::KEY_C;
        case 0x02:
            return caps ? ES1::Keydef::KEY_CAPITAL_D : ES1::Keydef::KEY_D;
        case 0x0E:
            return caps ? ES1::Keydef::KEY_CAPITAL_E : ES1::Keydef::KEY_E;
        case 0x03:
            return caps ? ES1::Keydef::KEY_CAPITAL_F : ES1::Keydef::KEY_F;
        case 0x05:
            return caps ? ES1::Keydef::KEY_CAPITAL_G : ES1::Keydef::KEY_G;
        case 0x04:
            return caps ? ES1::Keydef::KEY_CAPITAL_H : ES1::Keydef::KEY_H;
        case 0x22:
            return caps ? ES1::Keydef::KEY_CAPITAL_I : ES1::Keydef::KEY_I;
        case 0x26:
            return caps ? ES1::Keydef::KEY_CAPITAL_J : ES1::Keydef::KEY_J;
        case 0x28:
            return caps ? ES1::Keydef::KEY_CAPITAL_K : ES1::Keydef::KEY_K;
        case 0x25:
            return caps ? ES1::Keydef::KEY_CAPITAL_L : ES1::Keydef::KEY_L;
        case 0x2E:
            return caps ? ES1::Keydef::KEY_CAPITAL_M : ES1::Keydef::KEY_M;
        case 0x2D:
            return caps ? ES1::Keydef::KEY_CAPITAL_N : ES1::Keydef::KEY_N;
        case 0x1F:
            return caps ? ES1::Keydef::KEY_CAPITAL_O : ES1::Keydef::KEY_O;
        case 0x23:
            return caps ? ES1::Keydef::KEY_CAPITAL_P : ES1::Keydef::KEY_P;
        case 0x0C:
            return caps ? ES1::Keydef::KEY_CAPITAL_Q : ES1::Keydef::KEY_Q;
        case 0x0F:
            return caps ? ES1::Keydef::KEY_CAPITAL_R : ES1::Keydef::KEY_R;
        case 0x01:
            return caps ? ES1::Keydef::KEY_CAPITAL_S : ES1::Keydef::KEY_S;
        case 0x11:
            return caps ? ES1::Keydef::KEY_CAPITAL_T : ES1::Keydef::KEY_T;
        case 0x20:
            return caps ? ES1::Keydef::KEY_CAPITAL_U : ES1::Keydef::KEY_U;
        case 0x09:
            return caps ? ES1::Keydef::KEY_CAPITAL_V : ES1::Keydef::KEY_V;
        case 0x0D:
            return caps ? ES1::Keydef::KEY_CAPITAL_W : ES1::Keydef::KEY_W;
        case 0x07:
            return caps ? ES1::Keydef::KEY_CAPITAL_X : ES1::Keydef::KEY_X;
        case 0x10:
            return caps ? ES1::Keydef::KEY_CAPITAL_Y : ES1::Keydef::KEY_Y;
        case 0x06:
            return caps ? ES1::Keydef::KEY_CAPITAL_Z : ES1::Keydef::KEY_Z;
        default:
            return ES1::Keydef::KEY_NONE;
    }
    
    return ES1::Keydef::KEY_A;
}

std::string ES1::Platform::GetPersistentDir() {
    NSError *error;
    NSURL *appSupportDir = [[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:&error];
        
    return std::string([[appSupportDir path] UTF8String]) + "/" + m_pApplication->GetName();
}

std::string ES1::Platform::GetExecutableDir() {
    NSString *appParentDirectory = [[NSBundle mainBundle] bundlePath];
    return std::string([appParentDirectory UTF8String]);
}

double ES1::Platform::GetMilliseconds() {
    static timeval s_tTimeVal;
    gettimeofday(&s_tTimeVal, NULL);
    double time = s_tTimeVal.tv_sec * 1000.0; // sec to ms
    time += s_tTimeVal.tv_usec / 1000.0; // us to ms
    return time;
}

bool ES1::Platform::IsFile(const char* szFilePath) {
    BOOL isDir = NO;
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithUTF8String:szFilePath]  isDirectory:&isDir];
    return fileExists == YES && isDir == NO;
}

bool ES1::Platform::IsDirectory(const char* szFilePath) {
    BOOL isDir = NO;
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithUTF8String:szFilePath]  isDirectory:&isDir];
    return fileExists == YES && isDir == YES;
}

bool ES1::Platform::DirectoryExists(const char* szDirectoryPath) {
    BOOL isDir = NO;
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithUTF8String:szDirectoryPath]  isDirectory:&isDir];
    return fileExists == YES && isDir == YES;
}

bool ES1::Platform::FileExists(const char* szFilePath) {
    BOOL isDir = NO;
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithUTF8String:szFilePath]  isDirectory:&isDir];
    return fileExists == YES && isDir == NO;
}

bool ES1::Platform::CreateDirectory(const char* szDirectoryPath) {
    BOOL isDir;
    NSString *directory = [NSString stringWithUTF8String:szDirectoryPath];
    NSFileManager *fileManager= [NSFileManager defaultManager];
    
    if(![fileManager fileExistsAtPath:directory isDirectory:&isDir]) {
        return [fileManager createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    
    return false;
}

std::vector<std::string> ES1::Platform::ListDirectory(const char* szDirectoryPath) {
    NSString *docPath = [NSString stringWithUTF8String:szDirectoryPath];
    NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:docPath];
    
    std::vector<std::string> result;
    NSString *filename;
    while ((filename = [dirEnum nextObject])) {
        result.push_back(std::string([filename UTF8String]));
    }
    
    return result;
}

void ES1::Platform::ShowDialog(const char* szTitle, const char* szMessage) {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText: [NSString stringWithUTF8String:szTitle]];
    [alert setInformativeText: [NSString stringWithUTF8String:szMessage]];
    [alert addButtonWithTitle:@"Ok"];
    [alert runModal];
}

// http://stackoverflow.com/questions/5686958/set-string-to-pasteboard-copy-paste-in-cocoa-application
void ES1::Platform::SetClipboardText(const char* szText) {
    [[NSPasteboard generalPasteboard] declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:nil];
    [[NSPasteboard generalPasteboard] setString:[NSString stringWithUTF8String:szText] forType:NSStringPboardType];
}

std::string ES1::Platform::GetClipboardText() {
    return std::string([[[NSPasteboard generalPasteboard]  stringForType:NSPasteboardTypeString] UTF8String]);
}

// http://stackoverflow.com/questions/7466717/open-file-dialog
// https://gist.github.com/jeremy-w/3777700
// http://stackoverflow.com/questions/11815784/nsopenpanel-get-filename-in-objective-c
FILE* ES1::Platform::OpenFileDialog(const char* szMode, const std::vector<std::string>& filter) {
    id fileTypes = [NSMutableArray new];
    std::for_each(filter.begin(), filter.end(), ^(std::string str) {
        id nsstr = [NSString stringWithUTF8String:str.c_str()];
        [fileTypes addObject:nsstr];
    });
    
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    [openDlg setCanChooseFiles: YES];
    [openDlg setCanChooseDirectories: NO];
    [openDlg setAllowedFileTypes: fileTypes];
    
    if ( [openDlg runModal] == NSOKButton ) {
        for( NSURL* URL in [openDlg URLs] ) {
            FILE *fp = fopen([[URL path] UTF8String], szMode);
            if (fp != 0) {
                return fp;
            }
        }
    }
    
    return 0;
}

std::string ES1::Platform::SelectFolderDialog() {
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    [openDlg setCanChooseFiles: NO];
    [openDlg setCanChooseDirectories: YES];
    
    if ( [openDlg runModal] == NSOKButton ) {
        for( NSURL* URL in [openDlg URLs] ) {
            return std::string([[URL path] UTF8String]);
        }
    }
    
    return "";
}

FILE* ES1::Platform::SaveFileDialog(const char* szMode, const std::vector<std::string>& filter) {
    id fileTypes = [NSMutableArray new];
    std::for_each(filter.begin(), filter.end(), ^(std::string str) {
        id nsstr = [NSString stringWithUTF8String:str.c_str()];
        [fileTypes addObject:nsstr];
    });
    
    NSSavePanel* saveDlg = [NSSavePanel savePanel];
    [saveDlg setAllowedFileTypes: fileTypes];
    
    if ( [saveDlg runModal] == NSOKButton ) {
        return fopen([[[saveDlg URL] path] UTF8String], szMode);
    }
    
    return 0;
}