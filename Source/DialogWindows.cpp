#include <ES1/Framework/Platform.h>
#include <ES1/Framework/DialogWindows.h>

ES1::DialogWindows::DialogWindows(ES1::Platform& platform) {
    m_pPlatform = &platform;
}

ES1::DialogWindows::~DialogWindows() {
    
}

FILE* ES1::DialogWindows::OpenFile(const char* szMode, const std::vector<std::string>& filter) {
    return m_pPlatform->OpenFileDialog(szMode, filter);
}

FILE* ES1::DialogWindows::SaveFile(const char* szMode, const std::vector<std::string>& filter) {
    return m_pPlatform->SaveFileDialog(szMode, filter);
}

std::string ES1::DialogWindows::SelectFolder() {
    return m_pPlatform->SelectFolderDialog();
}

void ES1::DialogWindows::Alert(const char* szTitle, const char* szMessage) {
    return m_pPlatform->ShowDialog(szTitle, szMessage);
}