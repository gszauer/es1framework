#include <ES1/Framework/MouseDefs.h>

int ES1::MouseIndex(ES1::Mousedef m) {
    switch (m) {
        case ES1::Mousedef::MOUSE_NONE : return 0;
        case ES1::Mousedef::MOUSE_LEFT: return 1;
        case ES1::Mousedef::MOUSE_MIDDLE: return 2;
        case ES1::Mousedef::MOUSE_RIGHT: return 3;
    }
}