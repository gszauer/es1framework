#include <ES1/Framework/Quaternion.h>
#include <ES1/Framework/Math.h>

// Not sure where these came from. At some point i was just playing with the math
// https://gist.github.com/gszauer/6017246
// Quaternion * Vector: http://gamedev.stackexchange.com/questions/28395/rotating-vector3-by-a-quaternion
// Quaternion to matrix: http://www.flipcode.com/documents/matrfaq.html#Q54
// From to rotation: http://stackoverflow.com/questions/1171849/finding-quaternion-representing-the-rotation-from-one-vector-to-another
// http://tfc.duke.free.fr/coding/md5-specs-en.html
// http://forum.unity3d.com/threads/trivia-q-the-w-in-a-quaternion.2039/

ES1::Quaternion::Quaternion() : w(1.0f), x(0.0f), y(0.0f), z(0.0f) { }

ES1::Quaternion ES1::Clone(const ES1::Quaternion& quat) {
    Quaternion result;
    result.w = quat.w;
    result.x = quat.x;
    result.y = quat.y;
    result.z = quat.z;
    return result;
}

void ES1::Quaternion::Normalize() {
    float mag = (w * w + x * x + y * y + z * z);
    if (mag != 0.0f) {
        mag = Sqrt(mag);
        w = w / mag;
        x = x / mag;
        y = y / mag;
        z = z / mag;
    }
    else {
        w = 1.0f;
        x = y = z = 0.0f;
    }
}

// http://gamedev.stackexchange.com/questions/54915/what-is-the-rationale-behind-this-method-of-computing-the-w-component-from-md5-m
void ES1::Quaternion::ComputeW() {
        float t = 1.0f - ( x * x ) - ( y * y ) - ( z * z );
        if ( t < 0.0f ) {
            w = 0.0f;
        }
        else {
            w = -Sqrt(t);
        }
        
    // This function already assumes that the quaternion is of unit length.
    // We compute the W accordingly
}

ES1::Quaternion::Quaternion(float _w, float _x, float _y, float _z) : w(_w), x(_x), y(_y), z(_z) {
    Normalize();
}

ES1::Quaternion::Quaternion(float* v) : w(v[0]), x(v[1]), y(v[2]), z(v[3])  {
    Normalize();
}

ES1::Quaternion::Quaternion(const ES1::Quaternion& v) : w(v.w), x(v.x), y(v.y), z(v.z) {
    Normalize();
}

ES1::Quaternion::Quaternion(float eulerX, float eulerY, float eulerZ) {
    Quaternion internal =   AngleAxis(eulerY, Vector(0.0f, 1.0f, 0.0f))  *
                            AngleAxis(eulerX, Vector(1.0f, 0.0f, 0.0f))  *
                            AngleAxis(eulerZ, Vector(0.0f, 0.0f, 1.0f));
    w = internal.w;
    x = internal.x;
    y = internal.y;
    z = internal.z;
    
    Normalize();
}

ES1::Quaternion::Quaternion(const ES1::Vector& eulers) {
    Quaternion internal =   AngleAxis(eulers.y, Vector(0.0f, 1.0f, 0.0f))  *
                            AngleAxis(eulers.x, Vector(1.0f, 0.0f, 0.0f))  *
                            AngleAxis(eulers.z, Vector(0.0f, 0.0f, 1.0f));
    w = internal.w;
    x = internal.x;
    y = internal.y;
    z = internal.z;
    
    Normalize();
}

ES1::Quaternion& ES1::Quaternion::operator=(const ES1::Quaternion& v) {
    w = v.w;
    x = v.x;
    y = v.y;
    z = v.z;
    
    return *this;
}

float ES1::Quaternion::operator [](int i) {
    return wxyz[i];
}

ES1::Quaternion ES1::EulerAngles(float x, float y, float z) {
    // This is also valid!
    /*return  AngleAxis(y, Vector(0.0f, 1.0f, 0.0f))  *
            AngleAxis(x, Vector(1.0f, 0.0f, 0.0f))  *
            AngleAxis(z, Vector(0.0f, 0.0f, 1.0f));*/
    
    float cX (CosEuler (x / 2.0f));
    float sX (SinEuler (x / 2.0f));
    
    float cY (CosEuler (y / 2.0f));
    float sY (SinEuler (y / 2.0f));
    
    float cZ (CosEuler (z / 2.0f));
    float sZ (SinEuler (z / 2.0f));
    
    ES1::Quaternion qX (cX, sX, 0.0F, 0.0F);
    ES1::Quaternion qY (cY, 0.0F, sY, 0.0F);
    ES1::Quaternion qZ (cZ, 0.0F, 0.0F, sZ);
    
    return Normalize((qY * qX) * qZ);
}

ES1::Quaternion ES1::EulerAngles(const ES1::Vector& v) {
    return EulerAngles(v.x, v.y, v.z);
}

ES1::Vector ES1::ToEulerAngles(const ES1::Quaternion& q) {
    quat q1 = Normalize(q);
    
    double heading;     // Y
    double attitude;    // Z
    double bank;        // X
    
    double test = q1.x*q1.y + q1.z*q1.w;
    
    if (test > 0.499) { // singularity at north pole
        heading = 2.0 * Atan2Radian(q1.x, q1.w);
        attitude = PI / 2.0;
        bank = 0.0;
        return Vector(0.0f, 0.0f, 0.0f);
    }
    if (test < -0.499) { // singularity at south pole
        heading = -2.0 * Atan2Radian(q1.x,q1.w);
        attitude = -PI / 2.0;
        bank = 0;
        return Vector(0.0f, 0.0f, 0.0f);
    }
    
    double sqx = q1.x*q1.x;
    double sqy = q1.y*q1.y;
    double sqz = q1.z*q1.z;
    heading = Atan2Radian(2.0 * q1.y * q1.w - 2.0 * q1.x * q1.z , 1.0 - 2.0 * sqy - 2.0 * sqz);
    attitude = AsinRadian(2.0 * test);
    bank = Atan2Radian(2.0 * q1.x * q1.w - 2.0 * q1.y * q1.z, 1.0 - 2.0 * sqx - 2.0 * sqz);
    
    return Vector(bank * RAD2DEG, heading * RAD2DEG, attitude * RAD2DEG);
}

ES1::Quaternion ES1::Normalize(const ES1::Quaternion& q) {
    Quaternion result;
    float mag = (q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z);
    if (mag != 0.0f) {
        mag = Sqrt(mag);
        result.w = q.w / mag;
        result.x = q.x / mag;
        result.y = q.y / mag;
        result.z = q.z / mag;
    }
    
    return result;
}

ES1::Quaternion ES1::Inverse(const ES1::Quaternion& q) {
    Quaternion result;
    result.w = q.w;
    result.x = -q.x;
    result.y = -q.y;
    result.z = -q.z;
    return result;
}

ES1::Quaternion ES1::AngleAxis(float angle, const ES1::Vector& axis) {
    angle = angle * DEG2RAD;
    
    Vector nAxis = Normalize(axis);
    
    double s = SinRadian(angle * 0.5f);
    quat q;
    
    q.x = nAxis.x * s;
    q.y = nAxis.y * s;
    q.z = nAxis.z * s;
    q.w = CosRadian(angle * 0.5f);
    
    return Normalize(q);
}

void ES1::ToAngleAxis(const ES1::Quaternion& q, float* outAngle, ES1::Vector* outAxis) {
    Quaternion q1 = q;
    
    *outAngle = (2.0f * AcosRadian(q1.w)) * RAD2DEG;
    
    double s = Sqrt(1.0f-q1.w*q1.w);
    if (s < EPSILON) {
        outAxis->x = q1.x;
        outAxis->y = q1.y;
        outAxis->z = q1.z;
    } else {
        outAxis->x = q1.x / s;
        outAxis->y = q1.y / s;
        outAxis->z = q1.z / s;
    }
}

ES1::Quaternion ES1::operator*(const ES1::Quaternion& left, const ES1::Quaternion& right) {
    quat q;
    
    q.w = left.w * right.w - left.x * right.x - left.y * right.y - left.z * right.z;
    q.x = left.w * right.x + left.x * right.w + left.y * right.z - left.z * right.y;
    q.y = left.w * right.y - left.x * right.z + left.y * right.w + left.z * right.x;
    q.z = left.w * right.z + left.x * right.y - left.y * right.x + left.z * right.w;
    
    return q;
}

ES1::Quaternion& ES1::operator*=(ES1::Quaternion& left, const ES1::Quaternion& right) {
    quat q;
    
    q.w = left.w * right.w - left.x * right.x - left.y * right.y - left.z * right.z;
    q.x = left.w * right.x + left.x * right.w + left.y * right.z - left.z * right.y;
    q.y = left.w * right.y - left.x * right.z + left.y * right.w + left.z * right.x;
    q.z = left.w * right.z + left.x * right.y - left.y * right.x + left.z * right.w;
    
    left = q;
    
    return left;
}

ES1::Quaternion ES1::operator*(const ES1::Quaternion& q, float f) {
    Quaternion res = Clone(q);
    res.w = q.w * f;
    res.x = q.x * f;
    res.y = q.y * f;
    res.z = q.z * f;
    return res;
}


ES1::Quaternion& ES1::operator*=(ES1::Quaternion& left, float f) {
    left.w *= f;
    left.x *= f;
    left.y *= f;
    left.z *= f;
    return left;
}

ES1::Vector ES1::operator*(const ES1::Quaternion& q, const ES1::Vector& v) {
    // Extract the vector part of the quaternion
    Vector u(q.x, q.y, q.z);
    
    // Extract the scalar part of the quaternion
    float s = q.w;
    
    // Do the math
    return 2.0f * ES1::Dot(u, v) * u + (s*s - ES1::Dot(u, u)) * v+ 2.0f * s * ES1::Cross(u, v);
}

bool ES1::operator==(const ES1::Quaternion& lhs, const ES1::Quaternion& rhs) {
    return Equals(lhs.w, rhs.w) && Equals(lhs.x, rhs.x) && Equals(lhs.y, rhs.y) && Equals(lhs.z, rhs.z);
}

bool ES1::operator!=(const ES1::Quaternion& lhs, const ES1::Quaternion& rhs) {
    return !operator==(lhs,rhs);
}

/*ES1::Matrix ES1::ToMatrix(const ES1::Quaternion& quat) {
    float xx = quat.x * quat.x;
    float xy = quat.x * quat.y;
    float xz = quat.x * quat.z;
    float xw = quat.x * quat.w;
    float yy = quat.y * quat.y;
    float yz = quat.y * quat.z;
    float yw = quat.y * quat.w;
    float zz = quat.z * quat.z;
    float zw = quat.z * quat.w;
    
    return Matrix(1.0f - 2.0f * ( yy + zz ),
                  2.0f * ( xy - zw ),
                  2.0f * ( xz + yw ),
                  0.0f,
                  2.0f * ( xy + zw ),
                  1.0f - 2.0f * ( xx + zz ),
                  2.0f * ( yz - xw ),
                  0.0f,
                  2.0f * ( xz - yw ),
                  2.0f * ( yz + xw ),
                  1.0f - 2.0f * ( xx + yy ),
                  0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
}*/

float ES1::Dot(const ES1::Quaternion& q1, const ES1::Quaternion& q2) {
    return (q1.x*q2.x + q1.y*q2.y + q1.z*q2.z + q1.w*q2.w);
}

ES1::Quaternion ES1::Lerp(const ES1::Quaternion& q1, const ES1::Quaternion& q2, float t) {
    ES1::Quaternion tmpQuat;
    // if (dot < 0), q1 and q2 are more than 360 deg apart.
    // The problem is that quaternions are 720deg of freedom.
    // so we - all components when lerping
    if (Dot (q1, q2) < 0.0f) {
        tmpQuat.x = q1.x + t * (-q2.x - q1.x);
        tmpQuat.y = q1.y + t * (-q2.y - q1.y);
        tmpQuat.z = q1.z + t * (-q2.z - q1.z);
        tmpQuat.w = q1.w + t * (-q2.w - q1.w);
    }
    else {
        tmpQuat.x = q1.x + t * (q2.x - q1.x);
        tmpQuat.y = q1.y + t * (q2.y - q1.y);
        tmpQuat.z = q1.z + t * (q2.z - q1.z);
        tmpQuat.w = q1.w + t * (q2.w - q1.w);
    }
    
    return Normalize (tmpQuat);
}

ES1::Quaternion ES1::Slerp(const ES1::Quaternion& q1, const ES1::Quaternion& q2, float t) {
    float dot = Dot( q1, q2 );
    
    // dot = cos(theta)
    // if (dot < 0), q1 and q2 are more than 90 degrees apart,
    // so we can invert one to reduce spinning
    Quaternion tmpQuat;
    if (dot < 0.0f ) {
        dot = -dot;
        tmpQuat.x = -q2.x;
        tmpQuat.y = -q2.y;
        tmpQuat.z = -q2.z;
        tmpQuat.w = -q2.w;
    }
    else {
        tmpQuat = q2;
    }
    
    if (dot < 0.95f ) {
        float angle = AcosRadian(dot);
        float sinadiv, sinat, sinaomt;
        sinadiv = 1.0f/SinRadian(angle);
        sinat   = SinRadian(angle*t);
        sinaomt = SinRadian(angle*(1.0f-t));
        
        tmpQuat.x = (q1.x*sinaomt+tmpQuat.x*sinat)*sinadiv;
        tmpQuat.y = (q1.y*sinaomt+tmpQuat.y*sinat)*sinadiv;
        tmpQuat.z = (q1.z*sinaomt+tmpQuat.z*sinat)*sinadiv;
        tmpQuat.w = (q1.w*sinaomt+tmpQuat.w*sinat)*sinadiv;
        return tmpQuat;
        
    }
    else {
        return Lerp(q1,tmpQuat,t);
    }
}

float ES1::Magnitude(const ES1::Quaternion& q) {
    float sqrMag = Dot(q, q);
    if (sqrMag != 0.0f) {
        return Sqrt(sqrMag);
    }
    return 0;
}

float ES1::SqrMagnitude(const ES1::Quaternion& q) {
    return Dot(q, q);
}

ES1::Quaternion ES1::FromTo(const ES1::Vector& v1, const ES1::Vector& v2) {
    Quaternion q;
    Vector a = Cross(v1, v2);
    float v1Len = Length(v1);
    float v2Len = Length(v2);
    float dot = Dot(v1, v2);
    float w = (v1Len * v1Len) * (v2Len * v2Len) + dot;
    if (w == 0.0f) {
        return Quaternion();
    }
    w = Sqrt(w);
    return Quaternion(w, a.x, a.y, a.z);
}

ES1::Matrix ES1::AsMatrix(const ES1::Quaternion& q) {
    float x = q.x * 2.0f;
    float y = q.y * 2.0f;
    float z = q.z * 2.0f;
    float xx = q.x * x;
    float yy = q.y * y;
    float zz = q.z * z;
    float xy = q.x * y;
    float xz = q.x * z;
    float yz = q.y * z;
    float wx = q.w * x;
    float wy = q.w * y;
    float wz = q.w * z;
    
    Matrix result;
    
    // Calculate 3x3 matrix from orthonormal basis
    result.m[0] = 1.0f - (yy + zz);
    result.m[1] = xy + wz;
    result.m[2] = xz - wy;
    
    result.m[4] = xy - wz;
    result.m[5] = 1.0f - (xx + zz);
    result.m[6] = yz + wx;
    
    result.m[8]  = xz + wy;
    result.m[9]  = yz - wx;
    result.m[10] = 1.0f - (xx + yy);
    
    // 0  1  2  3    0 4 8  12
    // 4  5  6  7    1 5 9  13
    // 8  9  10 11   2 6 10 14
    // 12 13 14 15   3 7 11 15
    
    return result;
}

ES1::Quaternion ES1::AsQuaternion(const ES1::Matrix& mat) {
    // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
    // article "Quaternionf Calculus and Fast Animation".
    
    ES1::Matrix& m = const_cast<ES1::Matrix&>(mat);
    ES1::Quaternion q;
    
    float fTrace = m[0][0] + m[1][1] + m[2][2];
    float fRoot;
    
    if ( fTrace > 0.0f )
    {
        // |w| > 1/2, may as well choose w > 1/2
        fRoot = Sqrt (fTrace + 1.0f);  // 2w
        q.w = 0.5f * fRoot;
        fRoot = 0.5f / fRoot;  // 1/(4w)
        q.x = (m[1][2] - m[2][1]) * fRoot;
        q.y = (m[2][0] - m[0][2]) * fRoot;
        q.z = (m[0][1] - m[1][0]) * fRoot;
    }
    else {
        // |w| <= 1/2
        int s_iNext[3] = { 1, 2, 0 };
        int i = 0;
        if ( m[1][1] > m[0][0] ) {
            i = 1;
        }
        if ( m[2][2] > m[i][i] ) {
            i = 2;
        }
        int j = s_iNext[i];
        int k = s_iNext[j];
        
        fRoot = Sqrt (m[i][i] - m[j][j] - m[k][k] + 1.0f);
        float* apkQuat[3] = { &q.x, &q.y, &q.z };
        if (fRoot == 0) {
            return Quaternion();
        }
        *apkQuat[i] = 0.5f * fRoot;
        fRoot = 0.5f / fRoot;
        q.w = (m[k][j] - m[j][k]) * fRoot;
        *apkQuat[j] = (m[i][j] + m[j][i]) * fRoot;
        *apkQuat[k] = (m[i][k] + m[k][i]) * fRoot;
    }
    
    /*Original (transposed) implementation
    ES1::Matrix& m = const_cast<ES1::Matrix&>(mat);
    ES1::Quaternion q;
    
    float fTrace = m[0][0] + m[1][1] + m[2][2];
    float fRoot;
    
    if ( fTrace > 0.0f )
    {
        // |w| > 1/2, may as well choose w > 1/2
        fRoot = Sqrt (fTrace + 1.0f);  // 2w
        q.w = 0.5f * fRoot;
        fRoot = 0.5f / fRoot;  // 1/(4w)
        q.x = (m[2][1] - m[1][2]) * fRoot;
        q.y = (m[0][2] - m[2][0]) * fRoot;
        q.z = (m[1][0] - m[0][1]) * fRoot;
    }
    else {
        // |w| <= 1/2
        int s_iNext[3] = { 1, 2, 0 };
        int i = 0;
        if ( m[1][1] > m[0][0] ) {
            i = 1;
        }
        if ( m[2][2] > m[i][i] ) {
            i = 2;
        }
        int j = s_iNext[i];
        int k = s_iNext[j];
        
        fRoot = Sqrt (m[i][i] - m[j][j] - m[k][k] + 1.0f);
        float* apkQuat[3] = { &q.x, &q.y, &q.z };
        if (fRoot == 0) {
            return Quaternion();
        }
        *apkQuat[i] = 0.5f * fRoot;
        fRoot = 0.5f / fRoot;
        q.w = (m[k][j] - m[j][k]) * fRoot;
        *apkQuat[j] = (m[j][i] + m[i][j]) * fRoot;
        *apkQuat[k] = (m[k][i] + m[i][k]) * fRoot;
    }
     */
    return Normalize(q);
}