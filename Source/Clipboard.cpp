#include <ES1/Framework/Platform.h>
#include <ES1/Framework/Clipboard.h>

ES1::Clipboard::Clipboard(ES1::Platform& platform) {
    m_pPlatform = &platform;
}

ES1::Clipboard::~Clipboard() {
    
}

std::string ES1::Clipboard::GetText() {
    return m_pPlatform->GetClipboardText();
}

void ES1::Clipboard::SetText(const std::string& text) {
    m_pPlatform->SetClipboardText(text.c_str());
}