#include <ES1/Framework/Material.h>
#include <ES1/Framework/Texture.h>

// http://gamedev.stackexchange.com/questions/11741/my-ambient-lighting-is-not-working-correctly
// Color = Global.ambient*Material.ambient + LightFactor[0] + LightFactor[1] ... + Material.emissive

// Multi-texturing:
// https://www.google.com/search?q=glActiveTexture+multitexture&oq=glActiveTexture+multitexture&aqs=chrome..69i57.3718j0j4&sourceid=chrome&es_sm=91&ie=UTF-8
// keywords: glActiveTexture multi texture

// Light map:
// http://www.eng.utah.edu/~cs5610/lectures/Lightmaps%20Multitexture%202010.pdf

// Lighting
// http://www.just.edu.jo/~yaser/courses/cs480/Tutorials/OpenGl%20-%20Chapter%208%20%20Light%20&%20Material.htm

// Bump map:
// http://nehe.gamedev.net/tutorial/bump-mapping,_multi-texturing_&_extensions/16009/
ES1::Material::Material() {
    ambient = Magenta;
    diffuse = Magenta;
    specular = Color(0.0f, 0.0f, 0.0f);
    shininess = 0.0f;
    
    diffuseTexture = 0;
    normalTexture = 0;
    
    lit = false;
    castShadow = false;
    recieveShadow = false;
    renderOcclusion = false;
    flatShaded = false;
    wireframe = false;
}

ES1::Material::~Material() {
    
}

void ES1::Material::SetColor(ES1::Color color) {
    diffuse = color;
    ambient = color;
}

void ES1::Material::SetHighlight(Color color, float intensity) {
    if (intensity < 0.0f) {
        intensity = 0.0f;
    }
    if (intensity > 1.0f) {
        intensity = 1.0f;
    }
    specular = color;
    shininess  = intensity;
}


void ES1::Material::ParseMtl(const char* path) {
    // TODO
}